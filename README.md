La génèse
=======
Cette matière primitive, informe et grossière est née suite à la visualisation de la vidéo de "Mickaël Launay" intitulée "[La face cachée des tables de multiplication - Micmaths](https://www.youtube.com/watch?v=-X49VQgi86E)" puis de la vidéo "[Les fractales - Micmaths](https://www.youtube.com/watch?v=iFA3g_4myFw)". Cette matière a été façonnée au clavier bit après bit, octet après octet en égrainant les secondes, les heures, les jours. Puisse-t-elle être utile à tous les petits êtres de lumière en devenir.

Oui mais pourquoi infliger cela au monde, il ne t'a rien fait ?
=======
Les mathématiques ne sont pas une science de torture pour punir les femmes et les hommes. Cet outil est là pour essayer de montrer la beauté des mathématiques en mettant en exergue leur pureté et que leur simplicité fait naître la complexité et l'émerveillement. Pour les incrédules, l'image d'une fratale dans votre oeil vous fera vaciller. Vive le chou romanesco!!!

Dernière version stable
=======
- Windows :
    - [Swing](http://jerome7.laurent.free.fr/jdrawer/jdrawer-swing-1.4.0.exe)
    - [JavaFX](http://jerome7.laurent.free.fr/jdrawer/jdrawer-javafx-1.4.0.exe)
- Tous systèmes d'exploitation :
    - [Swing](http://jerome7.laurent.free.fr/jdrawer/jdrawer-swing-1.4.0-jar-with-dependencies.jar)
    - [JavaFX](http://jerome7.laurent.free.fr/jdrawer/jdrawer-javafx-1.4.0-jar-with-dependencies.jar)

Voir la bête
=======
- Swing :
    - java -jar jdrawer-swing-1.4.0-jar-with-dependencies.jar
    - jdrawer-swing-1.4.0.exe
- JavaFX :
    - java -jar jdrawer-swing-1.4.0-jar-with-dependencies.jar
    - jdrawer-javafx-1.4.0.exe

Captures d'écran
=======

Interface
======
![Interface](java/jdrawer/src/site/resources/images/screenshots/javafx_interface.png)

Modulo
======
![Modulo](java/jdrawer/src/site/resources/images/screenshots/modulo.png)

Tapis de Sierpiński
======
![Triangle](java/jdrawer/src/site/resources/images/screenshots/sierpinski_triangle.png)
![Carré](java/jdrawer/src/site/resources/images/screenshots/sierpinski_square.png)

Flocon de Von Koch
======
![Triangle](java/jdrawer/src/site/resources/images/screenshots/von_koch_triangle.png)
![Carré](java/jdrawer/src/site/resources/images/screenshots/von_koch_square.png)

Forme 2D
======
![Segment](java/jdrawer/src/site/resources/images/screenshots/2d_segment.png)
![Triangle](java/jdrawer/src/site/resources/images/screenshots/2d_triangle.png)
![Carré](java/jdrawer/src/site/resources/images/screenshots/2d_square.png)
![Composition](java/jdrawer/src/site/resources/images/screenshots/2d_composition.png)

Forme 3D
======
![Cube](java/jdrawer/src/site/resources/images/screenshots/3d_cube.png)
![Cube mailles](java/jdrawer/src/site/resources/images/screenshots/3d_link_cube.png)
![Cube illuminé](java/jdrawer/src/site/resources/images/screenshots/3d_illuminated_cube.png)
![Cube ensoleillé](java/jdrawer/src/site/resources/images/screenshots/3d_sunny_cube.png)
