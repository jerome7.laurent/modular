**1.1.0**
 - Ajout du "[Tapis de Sierpiński](https://fr.wikipedia.org/wiki/Tapis_de_Sierpi%C5%84ski, https://fr.wikipedia.org/wiki/Triangle_de_Sierpi%C5%84ski)" avec les erreurs de calculs, s'il vous plaît.

**1.2.0**
 - Ajout des formes 2D
 - Ajout du carré pour la 3D
 - Ajout de la sauvegarde du dessin pour JavaFX

**1.3.0**
 - Ajout du choix d'une couleur pour chaque module de dessin
 - Ajout des formes pentagone, hexagone, heptagone, octogone, ennéagone, décagone, hendécagone, dodécagone dans le module de forme 2D
