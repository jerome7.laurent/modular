/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.computer;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.computer.AbstractCloneDrawComputer;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.CircleDrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.LineDrawShape;
import org.jerome.laurent.jdrawer.api.shape.MacroDrawShape;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.TextDrawShape;
import org.jerome.laurent.jdrawer.api.shape.VectorDraw;

/**
 * @author Jérôme LAURENT
 *
 */
public class ModuloDrawComputer extends AbstractCloneDrawComputer {

	private static final Logger LOGGER = Logger.getLogger(ModuloDrawComputer.class.getName());

	public static final String PAUSE_PROPERTY_ID = "pause";
	public static final String TABLE_START_PROPERTY_ID = "table.start";
	public static final String TABLE_END_PROPERTY_ID = "table.end";
	public static final String TABLE_STEP_PROPERTY_ID = "table.step";
	public static final String MODULO_START_PROPERTY_ID = "modulo.start";
	public static final String MODULO_END_PROPERTY_ID = "modulo.end";
	public static final String MODULO_STEP_PROPERTY_ID = "modulo.step";
	public static final String CIRCLE_RADIUS_PROPERTY_ID = "circle.radius";
	public static final String DISPLAY_NUMBER_PROPERTY_ID = "display.number";

	private long tempo;
	private long pause;
	private double tableStart;
	private double tableEnd;
	private double tableStep;
	private int moduloStart;
	private int moduloEnd;
	private int moduloStep;
	private int circleRadius;
	private boolean isNumberDisplayed;

	private double currentTable;
	private int currentModulo;

	private int currentComputeIndex;
	private PointDraw[] circlePoints;

	public ModuloDrawComputer() {
		this.tempo = 0L;
		this.pause = 50L;
		this.tableStart = 2.0;
		this.tableEnd = 10.0;
		this.tableStep = 0.1;
		this.moduloStart = 300;
		this.moduloEnd = 300;
		this.moduloStep = 10;
		this.circleRadius = 300;

		this.currentComputeIndex = -1;
		this.currentTable = this.tableStart;
		this.currentModulo = this.moduloStart;
		this.isNumberDisplayed = false;

		this.getConfiguration().addNumberProperty(TEMPO_PROPERTY_ID, this.tempo, 0L, null, 100L);
		this.getConfiguration().addNumberProperty(PAUSE_PROPERTY_ID, this.pause, 1L, null, 100L);
		this.getConfiguration().addNumberProperty(TABLE_START_PROPERTY_ID, this.tableStart, 1.0, null, 0.1);
		this.getConfiguration().addNumberProperty(TABLE_END_PROPERTY_ID, this.tableEnd, 1.0, null, 0.1);
		this.getConfiguration().addNumberProperty(TABLE_STEP_PROPERTY_ID, this.tableStep, 1.0, null, 0.1);
		this.getConfiguration().addNumberProperty(MODULO_START_PROPERTY_ID, this.moduloStart, 1, null, 1);
		this.getConfiguration().addNumberProperty(MODULO_END_PROPERTY_ID, this.moduloEnd, 1, null, 1);
		this.getConfiguration().addNumberProperty(MODULO_STEP_PROPERTY_ID, this.moduloStep, 1, null, 1);
		this.getConfiguration().addNumberProperty(CIRCLE_RADIUS_PROPERTY_ID, this.circleRadius, 10, null, 10);
		this.getConfiguration().addBooleanProperty(DISPLAY_NUMBER_PROPERTY_ID, this.isNumberDisplayed);
	}

	@Override
	public boolean hasNext() {

		if (this.currentTable >= (this.tableEnd - this.tableStep) && this.currentComputeIndex >= this.moduloEnd) {
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info("End of compute.");
			}
			return false;
		}

		if (this.currentComputeIndex == this.currentModulo && this.currentModulo < this.moduloEnd) {
			this.currentModulo += this.moduloStep;
			this.currentComputeIndex = -2;
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info("Starting modulo: " + this.currentModulo);
			}
		}

		if (this.currentComputeIndex == this.moduloEnd && this.currentTable < (this.tableEnd - this.tableStep)) {
			this.currentTable += this.tableStep;
			this.currentModulo = this.moduloStart;
			this.currentComputeIndex = -2;
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info("Starting table: " + this.currentTable);
			}
		}

		return true;
	}

	@Override
	protected DrawShape computeNext() {
		final DrawShape computedShape;

		if (this.currentComputeIndex == -1) {
			computedShape = this.computeCircle();
		} else if (this.currentComputeIndex == -2) {
			computedShape = DrawShapes.CLEAR_DRAW_SHAPE;
		} else {
			computedShape = this.computeNextLine();
		}

		this.currentComputeIndex++;
		return computedShape;
	}

	protected DrawShape computeCircle() {
		final MacroDrawShape shapes;
		final CircleDrawShape computedShape = new CircleDrawShape(new PointDraw(), this.circleRadius);

		if (this.isNumberDisplayed) {
			shapes = new MacroDrawShape();
			shapes.getChildren().add(computedShape);
		} else {
			shapes = null;
		}


		this.circlePoints = new PointDraw[this.currentModulo];
		final double peace = 2.0 * Math.PI / this.currentModulo;
		for (int i = 0; i < this.circlePoints.length; i++) {
			this.circlePoints[i] = new PointDraw(
					Math.toIntExact((long) (this.circleRadius * Math.sin(i * peace))),
					Math.toIntExact((long) (this.circleRadius * Math.cos(i * peace)))
					);
			if (this.isNumberDisplayed) {
				final int x = Math.toIntExact((long) ((this.circleRadius + 15) * Math.sin(i * peace)));
				final int y = Math.toIntExact((long) ((this.circleRadius + 15) * Math.cos(i * peace)));
				final TextDrawShape childShape = new TextDrawShape(new PointDraw(x, y), Integer.toString(i));
				DrawShapes.translate(new VectorDraw(-7, 6), childShape.getPoint1());
				shapes.getChildren().add(childShape);
			}
		}

		if (this.isNumberDisplayed) {
			return shapes;
		}

		return computedShape;
	}

	protected LineDrawShape computeNextLine() {
		final int startValue = this.currentComputeIndex % this.currentModulo;
		final int endValue = (int) ((this.currentTable * this.currentComputeIndex) % this.currentModulo);
		final LineDrawShape computedShape = new LineDrawShape(this.circlePoints[startValue], this.circlePoints[endValue]);
		if (this.currentComputeIndex == (this.currentModulo-1)) {
			computedShape.setTempo(this.pause);
		} else {
			computedShape.setTempo(this.tempo);
		}

		return computedShape;
	}

	@Override
	public void reset() {
		this.currentModulo = this.moduloStart;
		this.currentTable = this.tableStart;
		this.currentComputeIndex = -1;
	}

	@Override
	public List<DrawError> applyConfiguration() {
		this.tempo = this.getPropertyValue(Long.class, TEMPO_PROPERTY_ID);
		this.pause = this.getPropertyValue(Long.class, PAUSE_PROPERTY_ID);
		this.tableStart = this.getPropertyValue(Double.class, TABLE_START_PROPERTY_ID);
		this.tableEnd = this.getPropertyValue(Double.class, TABLE_END_PROPERTY_ID);
		this.tableStep = this.getPropertyValue(Double.class, TABLE_STEP_PROPERTY_ID);
		this.moduloStart = this.getPropertyValue(Integer.class, MODULO_START_PROPERTY_ID);
		this.moduloEnd = this.getPropertyValue(Integer.class, MODULO_END_PROPERTY_ID);
		this.moduloStep = this.getPropertyValue(Integer.class, MODULO_STEP_PROPERTY_ID);
		this.circleRadius = this.getPropertyValue(Integer.class, CIRCLE_RADIUS_PROPERTY_ID);
		this.isNumberDisplayed = this.getPropertyValue(Boolean.class, DISPLAY_NUMBER_PROPERTY_ID);

		return Collections.emptyList();
	}

}