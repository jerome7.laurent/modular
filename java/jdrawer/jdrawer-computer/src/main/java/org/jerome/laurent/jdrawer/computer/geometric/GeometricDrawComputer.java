/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.computer.geometric;

import java.util.List;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.computer.AbstractCloneDrawComputer;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.MacroDrawShape;

/**
 * @author Jérôme LAURENT
 *
 */
public abstract class GeometricDrawComputer extends AbstractCloneDrawComputer {

	public static final String SIDE_PROPERTY_ID = "side";
	public static final String MAX_NUMBER_PROPERTY_ID = "max.number";
	public static final String SHAPE_PROPERTY_ID = "shape";

	protected static final long DEFAULT_TEMPO = 100L;
	protected static final int DEFAULT_SIDE = 400;
	protected static final int DEFAULT_MAX_NUMBER = 200;

	protected MacroDrawShape shape;
	protected int number;
	protected long tempo = DEFAULT_TEMPO;
	protected int side = DEFAULT_SIDE;
	protected int maxNumber = DEFAULT_MAX_NUMBER;

	protected EnumDrawBean currentShape;
	protected EnumDrawBean[] shapes;

	public GeometricDrawComputer() {
		super();

		this.shapes = this.buildShapes();
		this.currentShape = this.getDefaultShape();

		this.getConfiguration().addNumberProperty(TEMPO_PROPERTY_ID, this.tempo, 0L, null, 100L);
		this.getConfiguration().addEnumProperty(SHAPE_PROPERTY_ID, this.shapes);
		this.getConfiguration().addNumberProperty(SIDE_PROPERTY_ID, this.side, 1, 1000, 50);
		this.getConfiguration().addNumberProperty(MAX_NUMBER_PROPERTY_ID, this.maxNumber, 1, null, 50);
		EnumDrawBean.setSelected(this.shapes, this.currentShape.getEnumeration());

		this.init();
	}

	@Override
	public boolean hasNext() {
		return this.number < this.maxNumber;
	}

	@Override
	public List<DrawError> applyConfiguration() {

		final List<DrawError> errors = super.applyConfiguration();

		this.tempo = this.getPropertyValue(Long.class, TEMPO_PROPERTY_ID);
		this.shape.setTempo(this.tempo);
		this.side = this.getPropertyValue(Integer.class, SIDE_PROPERTY_ID);
		this.maxNumber = this.getPropertyValue(Integer.class, MAX_NUMBER_PROPERTY_ID);
		this.shapes = this.getPropertyValue(EnumDrawBean[].class, SHAPE_PROPERTY_ID);
		this.currentShape = EnumDrawBean.getSelected(this.shapes);

		this.init();

		return errors;
	}

	@Override
	protected DrawShape computeNext() {
		this.number++;
		return null;
	}

	@Override
	public void reset() {
		this.number = 0;
		this.init();
	}

	protected abstract void init();

	protected abstract EnumDrawBean[] buildShapes();

	protected abstract EnumDrawBean getDefaultShape();
}
