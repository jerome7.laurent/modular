/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.computer.fractal;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.computer.AbstractIterativeDrawComputer;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.PolygonDrawShape;

/**
 * @author Jérôme LAURENT
 *
 */
public class SierpinskiDrawComputer extends AbstractIterativeDrawComputer {

	public enum SierpinskiShape {
		TRIANGLE,
		SQUARE
		;
	}

	public static final String SHAPE_PROPERTY_ID = "shape";

	private SierpinskiShape currentShape = SierpinskiShape.TRIANGLE;

	private final DrawColor backgoundColor = new DrawColor(255, 255, 255);

	private final int length = 729;

	public SierpinskiDrawComputer() {
		this.getConfiguration().addEnumProperty(SHAPE_PROPERTY_ID, EnumDrawBean.getEnumDrawBeans(this, SierpinskiShape.values()));
	}

	@Override
	protected List<DrawShape> getFirstShapes() {
		final PolygonDrawShape shape = new PolygonDrawShape();
		shape.setFillColor(this.backgoundColor);
		if (this.currentShape == SierpinskiShape.TRIANGLE) {
			shape.setPoints(DrawShapes.createEquilateralTrianglePoints(this.length));
		} else {
			shape.setPoints(DrawShapes.createSquarePoints(this.length));
		}
		shape.setColor(null);
		return Arrays.asList(shape);
	}

	@Override
	protected List<DrawShape> computeNextIteration(final List<DrawShape> previousShapes) {
		final List<DrawShape> shapes = new LinkedList<>();
		for (final DrawShape shape : previousShapes) {
			if (shape instanceof PolygonDrawShape) {
				this.addChildren((PolygonDrawShape) shape, shapes);
			}
		}
		return shapes;
	}

	private void addChildren(final PolygonDrawShape shape, final List<DrawShape> shapes) {
		if (shape.getFillColor() == this.color) {
			shapes.add(shape);
			return;
		}

		final PointDraw[] originalPoints = shape.getPoints();
		final PointDraw[] newPoints = new PointDraw[originalPoints.length];

		if (this.currentShape == SierpinskiShape.TRIANGLE) {
			for (int i = 0; i < originalPoints.length; i++) {
				final int j = (i+1) % originalPoints.length;
				newPoints[i] = new PointDraw((originalPoints[i].getX() + originalPoints[j].getX()) / 2,
						(originalPoints[i].getY() + originalPoints[j].getY()) / 2);
			}

			shapes.add(new PolygonDrawShape(new PointDraw[] {originalPoints[0], newPoints[0], newPoints[2]}, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {newPoints[0], originalPoints[1], newPoints[1]}, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {newPoints[2], newPoints[1], originalPoints[2]}, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {newPoints[0], newPoints[1], newPoints[2]}, this.color, null));
		} else {

			// 3--2
			// |  |
			// 0--1
			final double newLength = ((originalPoints[1].getX() - originalPoints[0].getX())) / 3.0;
			final double newHalfLength = newLength / 2.0;
			// bottom line
			final PointDraw[] leftBottomSquare = DrawShapes.createSquarePoints(
					new PointDraw(
							(originalPoints[0].getX() + newHalfLength),
							(originalPoints[0].getY() + newHalfLength)
							), newLength);
			final PointDraw[] rightBottomSquare = DrawShapes.createSquarePoints(
					new PointDraw(
							(originalPoints[1].getX() - newHalfLength),
							(originalPoints[1].getY() + newHalfLength)
							), newLength);
			shapes.add(new PolygonDrawShape(leftBottomSquare, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {
					leftBottomSquare[1], rightBottomSquare[0], rightBottomSquare[3], leftBottomSquare[2]
			}, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(rightBottomSquare, this.backgoundColor, null));
			// top line
			final PointDraw[] leftTopSquare = new PointDraw[] {
					new PointDraw(
							originalPoints[3].getX(),
							(originalPoints[3].getY() - newLength)
							),
					new PointDraw(
							(originalPoints[3].getX() + newLength),
							(originalPoints[3].getY() - newLength)
							),
					new PointDraw(
							(originalPoints[3].getX() + newLength),
							originalPoints[3].getY()
							),
					originalPoints[3].clone()
			};
			shapes.add(new PolygonDrawShape(leftTopSquare, this.backgoundColor, null));
			final PointDraw[] rightTopSquare = new PointDraw[] {
					new PointDraw(
							(originalPoints[2].getX() - newLength),
							(originalPoints[2].getY() - newLength)
							),
					new PointDraw(
							originalPoints[2].getX(),
							(originalPoints[2].getY() - newLength)
							),
					originalPoints[2].clone(),
					new PointDraw(
							(originalPoints[2].getX() - newLength),
							originalPoints[2].getY()
							)
			};
			shapes.add(new PolygonDrawShape(rightTopSquare, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {
					leftTopSquare[1], rightTopSquare[0], rightTopSquare[3], leftTopSquare[2]
			}, this.backgoundColor, null));
			// middle line
			shapes.add(new PolygonDrawShape(new PointDraw[] {
					leftBottomSquare[3], leftBottomSquare[2], leftTopSquare[1], leftTopSquare[0]
			}, this.backgoundColor, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {
					leftBottomSquare[2], rightBottomSquare[3], rightTopSquare[0], leftTopSquare[1]
			}, this.color, null));
			shapes.add(new PolygonDrawShape(new PointDraw[] {
					rightBottomSquare[3], rightBottomSquare[2], rightTopSquare[1], rightTopSquare[0]
			}, this.backgoundColor, null));
		}
	}

	@Override
	public List<DrawError> applyConfiguration() {

		final List<DrawError> errors = super.applyConfiguration();

		final EnumDrawBean[] shapes = this.getPropertyValue(EnumDrawBean[].class, SHAPE_PROPERTY_ID);
		for (final EnumDrawBean shape : shapes) {
			if (shape.isSelected()) {
				this.currentShape = SierpinskiShape.valueOf(shape.getId());
			}
		}

		return errors;
	}
}
