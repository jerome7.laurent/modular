/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.computer.geometric;

import java.util.Arrays;
import java.util.List;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.CircleDrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.LineDrawShape;
import org.jerome.laurent.jdrawer.api.shape.MacroDrawShape;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;

/**
 * @author Jérôme LAURENT
 *
 */
public class FormDrawComputer extends GeometricDrawComputer {

	public enum FormShape {
		LINE,
		TRIANGLE,
		SQUARE,
		CIRCLE,
		DIAMOND,
		RECTANGLE,
		PENTAGON,
		HEXAGON,
		HEPTAGONE,
		OCTOGONE,
		ENNEAGONE,
		DECAGONE,
		HENDECAGONE,
		DODECAGONE
		;
	}

	public static final String ZOOM_FACTOR_PROPERTY_ID = "zoom.factor";

	public static final String ROTATION_FACTOR_PROPERTY_ID = "rotation.factor";

	public static final String ORIGIN_ROTATION_FACTOR_PROPERTY_ID = "origin.rotation.factor";

	protected static final double DEFAULT_ZOOM_FACTOR = 0.99;

	protected static final double DEFAULT_ROTATION_FACTOR = 1.0;

	protected static final double DEFAULT_ORIGIN_ROTATION_FACTOR = 0.0;

	private double zoomFactor = DEFAULT_ZOOM_FACTOR;

	private double rotationFactor = DEFAULT_ROTATION_FACTOR;

	private double originRotationFactor = DEFAULT_ORIGIN_ROTATION_FACTOR;

	public FormDrawComputer() {
		super();

		this.getConfiguration().addNumberProperty(ORIGIN_ROTATION_FACTOR_PROPERTY_ID, this.originRotationFactor, 0.0, 360.0, 1.0);
		this.getConfiguration().addNumberProperty(ROTATION_FACTOR_PROPERTY_ID, this.rotationFactor, 0.0, 360.0, 1.0);
		this.getConfiguration().addNumberProperty(ZOOM_FACTOR_PROPERTY_ID, this.zoomFactor, 0.0, 2.0, 0.01);
	}

	@Override
	public List<DrawError> applyConfiguration() {

		final List<DrawError> errors = super.applyConfiguration();
		this.originRotationFactor = this.getPropertyValue(Double.class, ORIGIN_ROTATION_FACTOR_PROPERTY_ID);
		this.rotationFactor = this.getPropertyValue(Double.class, ROTATION_FACTOR_PROPERTY_ID);
		this.zoomFactor = this.getPropertyValue(Double.class, ZOOM_FACTOR_PROPERTY_ID);

		this.init();

		return errors;
	}

	@Override
	protected DrawShape computeNext() {
		super.computeNext();
		DrawShapes.rotateZ(this.shape, Math.toRadians(this.rotationFactor));
		DrawShapes.zoom(this.zoomFactor, this.shape);
		return this.shape;
	}

	@Override
	protected void init() {

		this.shape = new MacroDrawShape();
		this.shape.setTempo(this.tempo);

		switch ((FormShape) this.currentShape.getEnumeration()) {
		case CIRCLE:
			this.shape.getChildren().add(new CircleDrawShape(new PointDraw(), this.side));
			break;
		case LINE:
			this.shape.getChildren().add(new LineDrawShape(new PointDraw(-this.side / 2.0, 0), new PointDraw(this.side / 2.0, 0)));
			break;
		case SQUARE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createSquarePoints(this.side))));
			break;
		case TRIANGLE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createEquilateralTriangleLines(this.side)));
			break;
		case DIAMOND:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createDiamondLines(this.side, this.side / 2.0)));
			break;
		case RECTANGLE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createRectangleLines(this.side, this.side / 2.0)));
			break;
		case PENTAGON:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(5, this.side / 2.0))));
			break;
		case HEXAGON:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(6, this.side / 2.0))));
			break;
		case HEPTAGONE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(7, this.side / 2.0))));
			break;
		case OCTOGONE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(8, this.side / 2.0))));
			break;
		case ENNEAGONE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(9, this.side / 2.0))));
			break;
		case DECAGONE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(10, this.side / 2.0))));
			break;
		case HENDECAGONE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(11, this.side / 2.0))));
			break;
		case DODECAGONE:
			this.shape.getChildren().addAll(Arrays.asList(DrawShapes.createClosedLines(DrawShapes.createRegularPolygonPoints(12, this.side / 2.0))));
			break;
		default:
			break;
		}

		this.shape.setColor(this.color);

		if (this.originRotationFactor != 0.0) {
			DrawShapes.rotateZ(this.shape, Math.toRadians(this.originRotationFactor));
		}
	}

	@Override
	protected EnumDrawBean[] buildShapes() {
		return EnumDrawBean.getEnumDrawBeans(this, FormShape.values());
	}

	@Override
	protected EnumDrawBean getDefaultShape() {
		return new EnumDrawBean(FormShape.SQUARE);
	}
}
