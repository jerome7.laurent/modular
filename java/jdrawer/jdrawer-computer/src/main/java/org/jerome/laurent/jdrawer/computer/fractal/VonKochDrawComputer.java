/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.computer.fractal;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.computer.AbstractIterativeDrawComputer;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.LineDrawShape;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.VectorDraw;

/**
 * @author Jérôme LAURENT
 *
 */
public class VonKochDrawComputer extends AbstractIterativeDrawComputer {

	public enum VonKochShape {
		TRIANGLE,
		LINE,
		SQUARE,
		RANDOM
		;
	}

	private final boolean isExternal = true;

	public static final String SHAPE_PROPERTY_ID = "shape";

	public static final String IS_INTERNAL_PROPERTY_ID = "is.internal";

	private VonKochShape currentShape = VonKochShape.TRIANGLE;

	private boolean isInternal;



	public VonKochDrawComputer() {
		super();
		this.isInternal = false;
		this.getConfiguration().addEnumProperty(SHAPE_PROPERTY_ID, EnumDrawBean.getEnumDrawBeans(this, VonKochShape.values()));
		this.getConfiguration().addBooleanProperty(IS_INTERNAL_PROPERTY_ID, this.isInternal);
	}

	@Override
	protected List<DrawShape> getFirstShapes() {

		if (this.currentShape == VonKochShape.TRIANGLE) {
			return Arrays.asList(DrawShapes.createEquilateralTriangleLines(600));
		} else if (this.currentShape == VonKochShape.SQUARE) {
			final int halfSizeSquare = 200;
			return Arrays.asList(
					new LineDrawShape(new PointDraw(-halfSizeSquare, -halfSizeSquare), new PointDraw(halfSizeSquare, -halfSizeSquare)),
					new LineDrawShape(new PointDraw(halfSizeSquare, -halfSizeSquare), new PointDraw(halfSizeSquare, halfSizeSquare)),
					new LineDrawShape(new PointDraw(halfSizeSquare, halfSizeSquare), new PointDraw(-halfSizeSquare, halfSizeSquare)),
					new LineDrawShape(new PointDraw(-halfSizeSquare, halfSizeSquare), new PointDraw(-halfSizeSquare, -halfSizeSquare))
					);
		} else if (this.currentShape == VonKochShape.RANDOM) {
			final List<DrawShape> lines = new LinkedList<>();
			final Random random = new Random(System.currentTimeMillis());
			final int snowFlakeNumber = random.nextInt(15);
			for (int i = 0; i <= snowFlakeNumber; i++) {
				final VectorDraw translationVector = new VectorDraw(200 - random.nextInt(400), 200 - random.nextInt(400));
				final int size = random.nextInt(100);
				final LineDrawShape[] triangleLines = DrawShapes.createEquilateralTriangleLines(size);
				DrawShapes.translate(translationVector, triangleLines);
				lines.addAll(Arrays.asList(triangleLines));
			}
			return lines;
		}

		final LineDrawShape line = new LineDrawShape(new PointDraw(-450, 0), new PointDraw(450, 0));
		return Arrays.asList(line);
	}

	@Override
	protected List<DrawShape> computeNextIteration(final List<DrawShape> previousShapes) {

		final List<DrawShape> newShapes = new LinkedList<>();
		for (final DrawShape shape : previousShapes) {
			if (shape instanceof LineDrawShape) {
				final LineDrawShape line = ((LineDrawShape) shape);
				final VectorDraw vector = new VectorDraw(line.getPoint1(), line.getPoint2());
				final VectorDraw thirdVector = DrawShapes.scalar(vector, 1, 3);
				final PointDraw point1 = line.getPoint1();
				final PointDraw point2 = line.getPoint1().clone();
				DrawShapes.translate(thirdVector, point2);
				final PointDraw point3 = point2.clone();
				final VectorDraw halfVector = DrawShapes.scalar(thirdVector, 1, 2);
				DrawShapes.translate(halfVector, point3);
				if (this.isInternal) {
					DrawShapes.translate(DrawShapes.reversePerpendicular(DrawShapes.scalar(this.isExternal ? DrawShapes.negative(halfVector) : halfVector, Math.sqrt(3.0))), point3);
				} else {
					DrawShapes.translate(DrawShapes.perpendicular(DrawShapes.scalar(this.isExternal ? DrawShapes.negative(halfVector) : halfVector, Math.sqrt(3.0))), point3);
				}
				final PointDraw point4 = line.getPoint2().clone();
				DrawShapes.translate(DrawShapes.negative(thirdVector), point4);
				final PointDraw point5 = line.getPoint2();
				final LineDrawShape line1 = new LineDrawShape(point1.clone(), point2.clone());
				final LineDrawShape line2 = new LineDrawShape(point2.clone(), point3.clone());
				final LineDrawShape line3 = new LineDrawShape(point3.clone(), point4.clone());
				final LineDrawShape line4 = new LineDrawShape(point4.clone(), point5.clone());
				newShapes.add(line1);
				newShapes.add(line2);
				newShapes.add(line3);
				newShapes.add(line4);
			}
		}
		return newShapes;
	}

	@Override
	public List<DrawError> applyConfiguration() {

		final List<DrawError> errors = super.applyConfiguration();

		final EnumDrawBean[] shapes = this.getPropertyValue(EnumDrawBean[].class, SHAPE_PROPERTY_ID);
		for (final EnumDrawBean shape : shapes) {
			if (shape.isSelected()) {
				this.currentShape = VonKochShape.valueOf(shape.getId());
			}
		}

		this.isInternal = this.getPropertyValue(Boolean.class, IS_INTERNAL_PROPERTY_ID);

		return errors;
	}

}
