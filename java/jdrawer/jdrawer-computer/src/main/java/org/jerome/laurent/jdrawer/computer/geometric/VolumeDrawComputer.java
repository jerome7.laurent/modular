/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.computer.geometric;

import java.util.List;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;

/**
 * @author Jérôme LAURENT
 *
 */
public class VolumeDrawComputer extends GeometricDrawComputer {

	public enum FormShape {
		TETRAHEDRON,
		CUBOID,
		CUBE,
		OCTAHEDRON,
		ICOSAHEDRON,
		CYLINDER,
		SPHERE
		;
	}

	public static final String FACE_COLOR_PROPERTY_ID = "face.color";

	public static final String IS_FACE_VISIBLE_PROPERTY_ID = "is.face.visible";

	public static final String IS_EDGE_VISIBLE_PROPERTY_ID = "is.edge.visible";

	public static final String X_ROTATION_ANGLE_PROPERTY_ID = "x.rotation.angle";

	public static final String Y_ROTATION_ANGLE_PROPERTY_ID = "y.rotation.angle";

	public static final String Z_ROTATION_ANGLE_PROPERTY_ID = "z.rotation.angle";

	public static final String RESOLUTION_PROPERTY_ID = "resolution";

	public final static DrawColor DEFAULT_FACE_COLOR = DrawColor.BLACK;

	private DrawColor fillColor = DEFAULT_FACE_COLOR;

	private boolean isFaceVisible;

	private boolean isEdgeVisible = true;

	private double xRotationAngle = Math.toRadians(5.0);

	private double yRotationAngle = Math.toRadians(5.0);

	private double zRotationAngle = Math.toRadians(5.0);

	private int resolution = 20;

	public VolumeDrawComputer() {
		super();
		this.getConfiguration().addColorProperty(FACE_COLOR_PROPERTY_ID, this.fillColor);
		this.getConfiguration().addBooleanProperty(IS_FACE_VISIBLE_PROPERTY_ID, this.isFaceVisible);
		this.getConfiguration().addBooleanProperty(IS_EDGE_VISIBLE_PROPERTY_ID, this.isEdgeVisible);
		this.getConfiguration().addNumberProperty(X_ROTATION_ANGLE_PROPERTY_ID, Math.toDegrees(this.xRotationAngle), 0.0, 360.0, 1.0);
		this.getConfiguration().addNumberProperty(Y_ROTATION_ANGLE_PROPERTY_ID, Math.toDegrees(this.yRotationAngle), 0.0, 360.0, 1.0);
		this.getConfiguration().addNumberProperty(Z_ROTATION_ANGLE_PROPERTY_ID, Math.toDegrees(this.zRotationAngle), 0.0, 360.0, 1.0);
		this.getConfiguration().addNumberProperty(RESOLUTION_PROPERTY_ID, this.resolution, 1, 100, 1);
	}

	@Override
	public List<DrawError> applyConfiguration() {
		final List<DrawError> errors = super.applyConfiguration();

		this.isFaceVisible= this.getPropertyValue(Boolean.class, IS_FACE_VISIBLE_PROPERTY_ID);
		if (this.isFaceVisible) {
			this.fillColor = this.getPropertyValue(DrawColor.class, FACE_COLOR_PROPERTY_ID);
		} else {
			this.fillColor = null;
		}
		this.isEdgeVisible= this.getPropertyValue(Boolean.class, IS_EDGE_VISIBLE_PROPERTY_ID);
		if (!this.isEdgeVisible) {
			this.color = null;
		}
		this.shape.setFillColor(this.fillColor);
		this.xRotationAngle = Math.toRadians(this.getPropertyValue(Double.class, X_ROTATION_ANGLE_PROPERTY_ID));
		this.yRotationAngle = Math.toRadians(this.getPropertyValue(Double.class, Y_ROTATION_ANGLE_PROPERTY_ID));
		this.zRotationAngle = Math.toRadians(this.getPropertyValue(Double.class, Z_ROTATION_ANGLE_PROPERTY_ID));

		this.resolution = this.getPropertyValue(Integer.class, RESOLUTION_PROPERTY_ID);

		this.init();

		return errors;
	}

	@Override
	protected DrawShape computeNext() {
		super.computeNext();
		DrawShapes.rotateXYZ(this.shape, this.xRotationAngle, this.yRotationAngle, this.zRotationAngle);
		return this.shape;
	}

	@Override
	public void init() {
		if (this.resolution == 0) {
			this.resolution = 1;
		}

		switch ((FormShape)this.currentShape.getEnumeration()) {
		case TETRAHEDRON:
			this.shape = DrawShapes.createTetrahedron(this.side, this.fillColor, this.color, this.resolution);
			break;

		case CUBOID:
			this.shape = DrawShapes.createCuboid(this.side, this.side / 2, this.side * 2, this.fillColor, this.color, this.resolution);
			break;

		case CUBE:
			this.shape = DrawShapes.createCube(this.side, this.fillColor, this.color, this.resolution);
			break;

		case OCTAHEDRON:
			this.shape = DrawShapes.createOctahedron(this.side, this.fillColor, this.color, this.resolution);
			break;
			
		case ICOSAHEDRON:
			this.shape = DrawShapes.createIcosahedron(this.side, this.fillColor, this.color, this.resolution);
			break;

		case CYLINDER:
			this.shape = DrawShapes.createCylinder(this.side, this.side, this.fillColor, this.color, this.resolution);
			break;

		case SPHERE:
			this.shape = DrawShapes.createSphere(this.side / 2.0, this.fillColor, this.color, this.resolution);
			break;

		default:
			break;
		}

		this.shape.getChildren().add(0, DrawShapes.CLEAR_DRAW_SHAPE);
		this.shape.setFillColor(this.fillColor);
		this.shape.setTempo(this.tempo);
	}

	@Override
	protected EnumDrawBean[] buildShapes() {
		return EnumDrawBean.getEnumDrawBeans(this, FormShape.values());
	}

	@Override
	protected EnumDrawBean getDefaultShape() {
		return new EnumDrawBean(FormShape.CUBE);
	}

}
