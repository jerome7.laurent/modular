#!/bin/bash
##
# @author Jérôme LAURENT
# @brief Do the the release of the jdrawer applictions
#
# @param $1 release version
# @param $2 furure release version
##

# be serious, it is the minimum
set -euo pipefail
trap finish EXIT

# step of the release
release_step=0
gitlab_project_id=16402486


function finish {
  if [ "${release_step}" == "0" ]; then
    usage
  elif [ ${release_step} -lt 8 ]; then
    logInfo "release step: ${release_step}"
  fi
}


##
# Log information message.
# @param $1 message
##
function logInfo() {
  echo "${1}"
}

##
# Display command usage.
##
function usage() {
  logInfo "usage: -v release_version -f future_release_version [-s release_step]"
}

##
# set variables
##
function set_variables() {
  while [ "$#" != "0" ]; do

    case ${1} in

      -v)
      release_version="${2}"
      ;;

      -f)
      future_release_version="${2}"
      ;;

      -s)
      release_step="${2}"
    esac

    shift 2
  done

  release_branch_name="${release_version}_release"

  echo "Release version: ${release_version}"
  echo "Future release version: ${future_release_version}"
  echo "Release step: ${release_step}"
  echo "Release branch name: ${release_branch_name}"
}

##
# Increment step.
##
function increment_step() {
  release_step=$((release_step+1))
}

##
# Create release branch
##
function create_release_branch() {
  git checkout -b "${release_branch_name}"
  increment_step
}

##
# Set version
# @param $1 version
##
function set_version() {
  mvn versions:set -DgenerateBackupPoms=false -DnewVersion="${1}"
}


##
# Check snapshot
##
function check_snapshot() {
  # grep on each pom file and OK if it is not found
  # shellcheck disable=SC2046,SC2015
  grep SNAPSHOT $(find . -name pom.xml) && exit 2 || logInfo "No snapshot found"
  increment_step
}

##
# Build release
##
function build_release() {
  mvn clean install
  git commit -a -m "${release_version} release"
  increment_step
}

##
# Merge and tag the release
##
function machine_release() {
  git checkout master
  git merge "${release_branch_name}"
  git tag -a "${release_version}" -m "${release_version} release version"
  increment_step
}

##
# Init future release
##
function init_future_release() {
  git checkout team
  git merge master
  set_version "${future_release_version}-SNAPSHOT"
  sed -i "s/\-[0-9]\+\.[0-9]\+\.[0-9]\+/-${release_version}/g" ../../README.md
  git commit -a -m "Initialize ${future_release_version}-SNAPSHOT"
  increment_step
}

##
# Delete release branch
##
function delete_release_branch() {
  git branch -d "${release_branch_name}"
  increment_step
}

##
# Deploy release
##
function deploy_release() {
  for branch in master team; do
    for remote in origin public; do
      git checkout "${branch}"
      git push "${remote}" "${branch}"
      git push "${remote}" "${branch}" --tags
    done
  done

for f in jdrawer-swing/target/jdrawer-swing-${release_version}.exe jdrawer-swing/target/jdrawer-swing-${release_version}-jar-with-dependencies.jar jdrawer-javafx/target/jdrawer-javafx-${release_version}.exe jdrawer-javafx/target/jdrawer-javafx-${release_version}-jar-with-dependencies.jar; do
	logInfo "uploading: ${f}" 
	ftp-upload -h ftpperso.free.fr -u jerome7.laurent --password "$(pass show dev/ftp/ftpperso.free.fr/jerome7.laurent)" -d jdrawer "${f}"
done

  curl --header 'Content-Type: application/json' \
       --header "PRIVATE-TOKEN: $(pass show dev/gitlab/jerome7.laurent.token)" \
       --data "{ \"name\": \"${release_version}\", \"tag_name\": \"${release_version}\", \"description\": \"Release ${release_version}\", \"assets\": { \"links\": [{ \"name\": \"JavaFX\", \"url\": \"http://jerome7.laurent.free.fr/jdrawer/jdrawer-javafx-${release_version}-jar-with-dependencies.jar\" }, { \"name\": \"JavaFX exe\", \"url\": \"http://jerome7.laurent.free.fr/jdrawer/jdrawer-javafx-${release_version}.exe\" }, { \"name\": \"Swing\", \"url\": \"http://jerome7.laurent.free.fr/jdrawer/jdrawer-swing-${release_version}-jar-with-dependencies.jar\" }, { \"name\": \"Swing exe\", \"url\": \"http://jerome7.laurent.free.fr/jdrawer/jdrawer-swing-${release_version}.exe\" }] } }" \
       --request POST "https://gitlab.com/api/v4/projects/${gitlab_project_id}/releases"
  increment_step
}

#
# MAIN
#
set_variables "$@"

if [ ${release_step} -le 1 ]; then
  create_release_branch
fi

if [ ${release_step} -le 2 ]; then
  set_version "${release_version}"
  increment_step
fi

if [ ${release_step} -le 3 ]; then
  check_snapshot
fi

if [ ${release_step} -le 4 ]; then
  build_release
fi

if [ ${release_step} -le 5 ]; then
  machine_release
fi

if [ ${release_step} -le 6 ]; then
  delete_release_branch
fi

if [ ${release_step} -le 7 ]; then
  init_future_release
fi

if [ ${release_step} -le 8 ]; then
  deploy_release
fi

