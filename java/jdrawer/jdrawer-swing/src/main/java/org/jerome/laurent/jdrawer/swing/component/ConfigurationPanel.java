/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.swing.component;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jerome.laurent.jdrawer.api.configuration.Configurartions;
import org.jerome.laurent.jdrawer.api.configuration.DrawConfigurable;
import org.jerome.laurent.jdrawer.api.configuration.DrawProperty;
import org.jerome.laurent.jdrawer.api.configuration.PropertyComponent;
import org.jerome.laurent.jdrawer.swing.SwingDrawFactory;

/**
 * @author Jérôme LAURENT
 *
 */
public class ConfigurationPanel extends JPanel {
	private static final long serialVersionUID = 4001406891948277028L;

	private DrawConfigurable configurable;

	private JButton applyButton;
	private final JPanel actionPanel;
	private JComponent configurationPanel;

	private final Map<String, PropertyComponent<?>> componentValues;

	public ConfigurationPanel() {
		super(new BorderLayout());

		this.actionPanel = this.createActionPanel();
		this.componentValues = new HashMap<>();
	}

	public void loadConfigurationPanel() {

		if (this.configurable != null) {

			if (this.configurationPanel != null) {
				this.removeAll();
			}

			this.componentValues.clear();

			final List<JLabel> labels = new ArrayList<>(this.configurable.getConfiguration().getProperties().size());
			@SuppressWarnings("rawtypes")
			final List<SwingPropertyComponent> components = new ArrayList<>(this.configurable.getConfiguration().getProperties().size());

			for (final DrawProperty<?> property : this.configurable.getConfiguration().getProperties()) {

				final JLabel label = SwingDrawFactory.createLabel(property);
				final SwingPropertyComponent<?> value = SwingDrawFactory.createPropertyComponent(property.getValue() == null ? property.getDefaultValue() : property.getValue());

				labels.add(label);
				components.add(value);

				this.componentValues.put(property.getId(), value);
			}

			this.configurationPanel = new JPanel();

			final GroupLayout layout = new GroupLayout(this.configurationPanel);
			this.configurationPanel.setLayout(layout);

			// Turn on automatically adding gaps between components
			layout.setAutoCreateGaps(true);

			// Turn on automatically creating gaps between components that touch
			// the edge of the container and the container.
			layout.setAutoCreateContainerGaps(true);

			// Create a sequential group for the horizontal axis.

			final GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

			// The sequential group in turn contains two parallel groups.
			// One parallel group contains the labels, the other the text fields.
			// Putting the labels in a parallel group along the horizontal axis
			// positions them at the same x location.
			//
			// Variable indentation is used to reinforce the level of grouping.
			final ParallelGroup hLabelParallelGroup = layout.createParallelGroup();
			final ParallelGroup hComponentParallelGroup = layout.createParallelGroup();
			for (int i = 0; i < labels.size(); i++) {
				hLabelParallelGroup.addComponent(labels.get(i));
				hComponentParallelGroup.addComponent(components.get(i).getComponent());
			}
			hGroup.addGroup(hLabelParallelGroup);
			hGroup.addGroup(hComponentParallelGroup);
			layout.setHorizontalGroup(hGroup);

			// Create a sequential group for the vertical axis.
			final GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();

			// The sequential group contains two parallel groups that align
			// the contents along the baseline. The first parallel group contains
			// the first label and text field, and the second parallel group contains
			// the second label and text field. By using a sequential group
			// the labels and text fields are positioned vertically after one another.
			for (int i = 0; i < labels.size(); i++) {
				vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE).
						addComponent(labels.get(i)).addComponent(components.get(i).getComponent()));

			}
			layout.setVerticalGroup(vGroup);

			this.add(new JScrollPane(this.configurationPanel), BorderLayout.CENTER);
			this.add(this.actionPanel, BorderLayout.SOUTH);
			this.revalidate();
		}
	}

	/**
	 * Add action listener to apply button.
	 * @param l
	 */
	public void addApplyActionListener(final ActionListener l) {
		this.applyButton.addActionListener(l);
	}

	private JPanel createActionPanel() {
		this.applyButton = new JButton(SwingDrawFactory.createAction("swing.configuration.panel.button.apply"));
		this.applyButton.addActionListener(e -> Configurartions.applyConfiguration(this.configurable, this.componentValues));

		final JButton resetButton = new JButton(SwingDrawFactory.createAction("swing.configuration.panel.button.reset"));
		resetButton.addActionListener(e -> Configurartions.resetConfiguration(this.configurable, this.componentValues));

		final JPanel actionPanel = new JPanel();
		actionPanel.add(this.applyButton);
		actionPanel.add(resetButton);
		return actionPanel;
	}

	/**
	 *
	 * @param configurable
	 */
	public void setConfigurable(final DrawConfigurable configurable) {
		this.configurable = configurable;
		this.loadConfigurationPanel();
	}

}
