/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.swing.action;

import java.awt.event.ActionEvent;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import org.jerome.laurent.jdrawer.api.bean.DrawBean;
import org.jerome.laurent.jdrawer.api.i18n.I18ns;
import org.jerome.laurent.jdrawer.swing.SwingDrawFactory;
import org.jerome.laurent.jdrawer.swing.SwingDrawViewer;

/**
 * @author Jérôme LAURENT
 *
 */
public class SimpleSwingAction extends AbstractAction implements DrawBean {
	private static final long serialVersionUID = 2438557799006770449L;

	private static final Logger LOGGER = Logger.getLogger(SwingDrawViewer.class.getName());

	private final String id;

	private final Consumer<ActionEvent> actionPerformed;

	private final boolean isOnlyIconDisplaying;

	public SimpleSwingAction(final String id, final boolean isOnlyIconDisplaying, final Consumer<ActionEvent> actionPredicate) {
		this.id = id;
		this.isOnlyIconDisplaying = isOnlyIconDisplaying;
		this.actionPerformed = actionPredicate;
		SwingDrawFactory.registerAction(this);
	}

	public SimpleSwingAction(final String id) {
		this(id, false, null);
	}

	public void loadProperties() {
		final ImageIcon smallIcon = SwingDrawFactory.getImageIcon(this.id + ".small");
		if (smallIcon != null) {
			this.putValue(Action.SMALL_ICON, smallIcon);
		}
		final ImageIcon largeIcon = SwingDrawFactory.getImageIcon(this.id + ".large");
		if (largeIcon != null) {
			this.putValue(Action.LARGE_ICON_KEY, largeIcon);
		}
		if (!(this.isOnlyIconDisplaying && smallIcon != null)) {
			this.putValue(Action.NAME, I18ns.getLabel(SwingDrawFactory.getCurrentLocale(), this));
		}
		this.putValue(Action.SHORT_DESCRIPTION, I18ns.getDescription(SwingDrawFactory.getCurrentLocale(), this));
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public DrawBean getParent() {
		return SwingDrawFactory.PARENT;
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		if (this.actionPerformed != null) {
			this.actionPerformed.accept(e);
		}
	}



}
