/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import org.jerome.laurent.jdrawer.api.bean.DrawBean;
import org.jerome.laurent.jdrawer.api.bean.SimpleDrawBean;
import org.jerome.laurent.jdrawer.api.controller.DrawController;
import org.jerome.laurent.jdrawer.api.factory.DrawFactory;
import org.jerome.laurent.jdrawer.api.i18n.I18ns;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.viewer.Drawer;
import org.jerome.laurent.jdrawer.swing.action.SimpleSwingAction;
import org.jerome.laurent.jdrawer.swing.component.SwingPropertyComponent;

/**
 * @author Jérôme LAURENT
 *
 */
public class SwingDrawFactory extends SimpleDrawBean implements DrawFactory {

	private static final Logger LOGGER = Logger.getLogger(SwingDrawFactory.class.getName());

	private static Locale currentLocale = Locale.FRANCE;

	public static final SwingDrawFactory PARENT = new SwingDrawFactory();

	private static final Set<SimpleSwingAction> ACTIONS = new HashSet<>();

	private SwingDrawViewer viewer;

	private DrawController controller;

	/**
	 *
	 */
	public SwingDrawFactory() {
		super("swing.parent.draw.bean");
		Locale.setDefault(currentLocale);
	}

	/**
	 *
	 */
	@Override
	public void setDrawController(final DrawController controller) {
		this.controller = controller;
	}

	/**
	 *
	 */
	@Override
	public SwingDrawViewer getDrawViewer() {
		if (this.viewer == null) {
			this.viewer = new SwingDrawViewer(this.controller);
		}

		return this.viewer;
	}

	/**
	 *
	 */
	@Override
	public Drawer getDrawer() {
		return this.getDrawViewer().getDrawer();
	}

	/**
	 * @return the currentLocale
	 */
	public static Locale getCurrentLocale() {
		return currentLocale;
	}

	/**
	 *
	 * @param locale
	 */
	public static void setCurrentLocale(final Locale locale) {
		currentLocale = locale;
		Locale.setDefault(locale);
		reloadActions();
	}

	/**
	 *
	 * @param actionId
	 * @param actionPerformed
	 * @return
	 */
	public static Action createAction(final String actionId, final Consumer<ActionEvent> actionPerformed) {
		return createAction(actionId, false, actionPerformed);
	}

	/**
	 *
	 * @param actionId
	 * @param isOnlyIconVisible
	 * @param actionPerformed
	 * @return
	 */
	public static Action createAction(final String actionId, final boolean isOnlyIconVisible, final Consumer<ActionEvent> actionPerformed) {
		return new SimpleSwingAction(actionId, isOnlyIconVisible, actionPerformed);
	}

	/**
	 *
	 * @param actionId
	 * @return
	 */
	public static Action createAction(final String actionId) {
		return new SimpleSwingAction(actionId);
	}

	/**
	 *
	 * @param action
	 */
	public static final void registerAction(final SimpleSwingAction action) {
		ACTIONS.add(action);
	}

	/**
	 *
	 */
	public static final void reloadActions() {
		for (final SimpleSwingAction action : ACTIONS) {
			action.loadProperties();
		}
	}

	/**
	 *
	 * @param action
	 * @return
	 */
	public static JButton createButton(final Action action) {
		return new JButton(action);
	}

	/**
	 *
	 * @param imageId
	 * @return
	 */
	public static final ImageIcon getImageIcon(final String imageId) {
		final URL imgURL = SimpleSwingAction.class.getClassLoader().getResource("META-INF/icons/default/" + imageId + ".png");
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			if (LOGGER.isLoggable(Level.FINE)) {
				LOGGER.fine("The image '" + imageId + "' is not found.");
			}
			return null;
		}
	}

	/**
	 *
	 * @param <T>
	 * @param value
	 * @return
	 */
	public static final <T> SwingPropertyComponent<T> createPropertyComponent(final T value) {
		return new SwingPropertyComponent<>(value);
	}

	/**
	 *
	 * @param bean
	 * @return
	 */
	public static final JLabel createLabel(final DrawBean bean) {
		final JLabel label = new JLabel(I18ns.getLabel(getCurrentLocale(), bean));
		label.setToolTipText(I18ns.getDescription(getCurrentLocale(), bean));
		return label;
	}

	/**
	 *
	 * @param color
	 * @return
	 */
	public static final Color getColor(final DrawColor color) {
		if (color == null) {
			return null;
		}
		return new Color(color.getRed(), color.getBlue(), color.getGreen());
	}

	public static final DrawColor getDrawColor(final Color color) {
		return new DrawColor(color.getRed(), color.getBlue(), color.getGreen());
	}

	@Override
	public void saveViewerImage() {
		final JFileChooser fc = new JFileChooser();
		fc.showOpenDialog(null);
		// TODO
	}
}
