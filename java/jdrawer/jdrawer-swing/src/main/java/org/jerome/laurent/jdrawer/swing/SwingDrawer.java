/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jerome.laurent.jdrawer.api.shape.CircleDrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.LineDrawShape;
import org.jerome.laurent.jdrawer.api.shape.MacroDrawShape;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.PolygonDrawShape;
import org.jerome.laurent.jdrawer.api.shape.TextDrawShape;
import org.jerome.laurent.jdrawer.api.shape.VectorDraw;
import org.jerome.laurent.jdrawer.api.viewer.Drawer;

/**
 * @author Jérôme LAURENT
 *
 */
public class SwingDrawer extends JPanel implements Drawer, ComponentListener {
	private static final long serialVersionUID = -4836670876518173993L;

	private static final Logger LOGGER = Logger.getLogger(SwingDrawer.class.getName());

	private final BlockingQueue<DrawShape> shapes;

	private VectorDraw centerVector;

	private Color currentBackgroundColor;

	public SwingDrawer() {
		super();
		this.shapes = new LinkedBlockingQueue<>();
		this.setDoubleBuffered(true);
		this.centerVector = new VectorDraw(this.getWidth()/2, this.getHeight()/2);
		this.addComponentListener(this);
	}

	@Override
	public void draw(final List<DrawShape> shapes) {
		if (LOGGER.isLoggable(Level.FINER)) {
			LOGGER.fine("Drawing shape: " + shapes);
		}
		if (this.shapes != null) {
			this.shapes.addAll(shapes);
			SwingUtilities.invokeLater(() -> this.repaint());
		}
	}

	@Override
	protected void paintComponent(final Graphics g) {

		//		super.paintComponent(g);

		if (this.shapes.isEmpty()) {
			return;
		}

		while (!this.shapes.isEmpty()) {
			this.drawShape(g, this.shapes.poll());
		}
	}

	protected void drawShape(final Graphics g, final DrawShape shape) {

		if (shape == null) {
			return;
		}

		g.setColor(SwingDrawFactory.getColor(shape.getColor()));

		if (shape == DrawShapes.CLEAR_DRAW_SHAPE) {
			if (this.currentBackgroundColor != null) {
				g.setColor(this.currentBackgroundColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
			} else {
				g.clearRect(0, 0, this.getWidth(), this.getHeight());
			}
		} else if (shape instanceof TextDrawShape) {
			final TextDrawShape text = (TextDrawShape) shape;
			this.transformToCoordinateSystem(text.getPoint1());
			g.drawString(text.getText(), (int) Math.round(text.getPoint1().getX()), (int) Math.round(text.getPoint1().getY()));
		} else if (shape instanceof CircleDrawShape) {
			final CircleDrawShape circle = (CircleDrawShape) shape;
			this.transformToCoordinateSystem(circle.getPoint1());
			g.drawOval(
					(int) Math.round(circle.getPoint1().getX() - circle.getRadius()),
					(int) Math.round(circle.getPoint1().getY() - circle.getRadius()),
					(int) Math.round(circle.getRadius() * 2.0),
					(int) Math.round(circle.getRadius() * 2.0));
		} else if (shape instanceof LineDrawShape) {
			final LineDrawShape line = (LineDrawShape) shape;
			this.transformToCoordinateSystem(line.getPoint1(), line.getPoint2());
			g.drawLine(
					(int) Math.round(line.getPoint1().getX()),
					(int) Math.round(line.getPoint1().getY()),
					(int) Math.round(line.getPoint2().getX()),
					(int) Math.round(line.getPoint2().getY()));
		} else if (shape instanceof MacroDrawShape) {
			for (final DrawShape childShape : ((MacroDrawShape)shape).getChildren()) {
				this.drawShape(g, childShape);
			}
		} else if (shape instanceof PolygonDrawShape) {
			final PolygonDrawShape polygon = (PolygonDrawShape) shape;

			final int pointNumber = polygon.getPoints().length;
			final int x[] = new int[pointNumber];
			final int y[] = new int[pointNumber];

			this.transformToCoordinateSystem(polygon.getPoints());

			for (int i = 0; i < pointNumber; i++) {
				x[i] = (int) Math.round(polygon.getPoints()[i].getX());
				y[i] = (int) Math.round(polygon.getPoints()[i].getY());
			}
			if (polygon.getColor() != null) {
				g.drawPolygon(x, y, pointNumber);
			}
			if (polygon.getFillColor() != null) {
				g.setColor(SwingDrawFactory.getColor(polygon.getFillColor()));
				g.fillPolygon(x, y, pointNumber);
			}
		}
	}

	protected void transformToCoordinateSystem(final PointDraw... points) {
		for (final PointDraw point : points) {
			point.setY(-point.getY());
		}
		DrawShapes.translate(this.getVectorCenter(), points);
	}

	protected VectorDraw getVectorCenter() {
		return this.centerVector;
	}

	@Override
	public void setBackground(final Color bg) {
		this.currentBackgroundColor = bg;
		super.setBackground(bg);
		this.draw(Arrays.asList(DrawShapes.CLEAR_DRAW_SHAPE));
	}

	@Override
	public void componentHidden(final ComponentEvent arg0) {
		// Do nothing
	}

	@Override
	public void componentMoved(final ComponentEvent arg0) {
		// Do nothing
	}

	@Override
	public void componentResized(final ComponentEvent arg0) {
		this.centerVector = new VectorDraw(this.getWidth()/2, this.getHeight()/2);
	}

	@Override
	public void componentShown(final ComponentEvent arg0) {
		// Do nothing
	}
}
