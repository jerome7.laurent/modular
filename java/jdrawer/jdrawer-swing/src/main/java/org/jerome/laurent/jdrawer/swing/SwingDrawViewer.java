/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.jerome.laurent.jdrawer.api.bean.DrawBean;
import org.jerome.laurent.jdrawer.api.bean.SimpleDrawBean;
import org.jerome.laurent.jdrawer.api.computer.AbstractDrawComputer;
import org.jerome.laurent.jdrawer.api.controller.DrawController;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerEvent;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerListener;
import org.jerome.laurent.jdrawer.api.extra.EasterEgg;
import org.jerome.laurent.jdrawer.api.i18n.I18ns;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.VectorDraw;
import org.jerome.laurent.jdrawer.api.viewer.DrawViewer;
import org.jerome.laurent.jdrawer.swing.component.AbstractDrawComputerComboRenderer;
import org.jerome.laurent.jdrawer.swing.component.ConfigurationPanel;

/**
 * @author Jérôme LAURENT
 *
 */
public class SwingDrawViewer extends JFrame implements DrawViewer, DrawControllerListener, MouseWheelListener, MouseListener, KeyListener {
	private static final long serialVersionUID = -136474626315647089L;

	private static final Logger LOGGER = Logger.getLogger(SwingDrawViewer.class.getName());

	private final DrawController controller;

	private final SwingDrawer drawer;

	private final ConfigurationPanel configurationPanel;
	private final ConfigurationPanel globalConfigurationPanel;
	private final JDialog globalConfigurationDialog;
	private final DrawBean globalConfigurationDrawBean;

	private final EasterEgg easterEgg;

	private JButton startButton;
	private JButton stopButton;
	private JButton loopButton;
	private JButton cleanButton;

	private PointDraw pressedPoint;

	public SwingDrawViewer(final DrawController controller) {
		this.controller = controller;
		this.drawer = new SwingDrawer();
		this.configurationPanel = new ConfigurationPanel();
		this.globalConfigurationDrawBean = new SimpleDrawBean(SwingDrawFactory.PARENT, "swing.configuration.dialog.title");
		this.globalConfigurationDialog = new JDialog(this);
		this.globalConfigurationPanel = new ConfigurationPanel();

		this.easterEgg = new EasterEgg();

		this.controller.addListener(this);
		this.drawer.addMouseWheelListener(this);
		this.drawer.addMouseListener(this);
		this.drawer.addKeyListener(this);
	}

	@Override
	public void run() {
		this.setTitle("JDrawer - Pour Juliette"); // \u2665
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(1280, 800));

		// menu bar
		this.setJMenuBar(this.createMenuBar());

		// action button
		this.getContentPane().add(this.createToolBar(), BorderLayout.NORTH);

		// configuration/viewer
		final JSplitPane configurationDrawerSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.configurationPanel, this.drawer);
		configurationDrawerSplitPane.setOneTouchExpandable(true);
		this.getContentPane().add(configurationDrawerSplitPane, BorderLayout.CENTER);

		SwingDrawFactory.reloadActions();

		// pack
		this.pack();

		this.setLocationRelativeTo(this);
		this.setVisible(true);
	}

	protected JMenuBar createMenuBar() {

		// preference dialog
		this.globalConfigurationDialog.setTitle(I18ns.getLabel(SwingDrawFactory.getCurrentLocale(), this.globalConfigurationDrawBean));

		this.globalConfigurationPanel.setConfigurable(this.controller);
		this.globalConfigurationPanel.addApplyActionListener(e -> this.globalConfigurationDialog.setVisible(false));

		final JPanel globalConfigurationContentPanel = new JPanel(new BorderLayout());
		globalConfigurationContentPanel.add(this.globalConfigurationPanel);
		this.globalConfigurationDialog.getContentPane().add(globalConfigurationContentPanel);
		this.globalConfigurationDialog.setPreferredSize(new Dimension(350, 150));
		this.globalConfigurationDialog.revalidate();
		this.globalConfigurationDialog.pack();

		this.globalConfigurationDialog.setLocationRelativeTo(this);

		// preference item
		final JMenuItem preferenceMenuItem = new JMenuItem(SwingDrawFactory.createAction("swing.menu.bar.menu.file.item.configuration", e -> this.globalConfigurationDialog.setVisible(true)));

		// exit item
		final JMenuItem exitMenuItem = new JMenuItem(SwingDrawFactory.createAction("swing.menu.bar.menu.file.item.quit", e -> System.exit(0)));

		// File
		final JMenu fileMenu = new JMenu(SwingDrawFactory.createAction("swing.menu.bar.menu.file"));
		fileMenu.add(preferenceMenuItem);
		fileMenu.add(exitMenuItem);

		// look and feel
		final JMenu lookAndFeelMenu = new JMenu(SwingDrawFactory.createAction("swing.menu.bar.menu.lookandfeel"));

		final ButtonGroup bg1 = new ButtonGroup();
		final UIManager.LookAndFeelInfo[] lookAndFeelInfos = UIManager.getInstalledLookAndFeels();
		final JRadioButtonMenuItem[] radioButtonItems = new JRadioButtonMenuItem[lookAndFeelInfos.length];
		for (int i = 0; i < lookAndFeelInfos.length; i++) {
			radioButtonItems[i] = new JRadioButtonMenuItem();
			radioButtonItems[i].setText(lookAndFeelInfos[i].getName());
			radioButtonItems[i].setSelected(i == 0);
			bg1.add(radioButtonItems[i]);
			radioButtonItems[i].addItemListener(new java.awt.event.ItemListener() {
				// Changer le Look And Feel
				@Override
				public void itemStateChanged(final java.awt.event.ItemEvent e) {
					int i = 0;
					for (i = 0; i < radioButtonItems.length; ++i) {
						if (radioButtonItems[i] == e.getSource()) {
							break;
						}
					}

					try {
						UIManager.setLookAndFeel(lookAndFeelInfos[i].getClassName());
						SwingUtilities.updateComponentTreeUI(SwingDrawViewer.this);
					} catch (final Exception exception) {
						LOGGER.log(Level.SEVERE, exception.getLocalizedMessage(), exception);
					}
				}
			});
			lookAndFeelMenu.add(radioButtonItems[i]);
		}

		// language
		final List<Locale> supportedLanguages = I18ns.getSupportedLocales();

		final JMenu languageMenu = new JMenu(SwingDrawFactory.createAction("swing.menu.bar.menu.language"));

		final ButtonGroup languageButtonGroup = new ButtonGroup();
		final JRadioButtonMenuItem[] languageRadioButtonItems = new JRadioButtonMenuItem[supportedLanguages.size()];
		for (int i = 0; i < supportedLanguages.size(); i++) {
			final Locale currentLanguage = supportedLanguages.get(i);
			languageRadioButtonItems[i] = new JRadioButtonMenuItem(currentLanguage.getDisplayLanguage(currentLanguage), Locale.getDefault().equals(currentLanguage));
			languageRadioButtonItems[i].addItemListener(e -> this.setLocale(e));
			languageButtonGroup.add(languageRadioButtonItems[i]);
			languageMenu.add(languageRadioButtonItems[i]);
		}

		// menu bar
		final JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(lookAndFeelMenu);
		menuBar.add(languageMenu);

		return menuBar;
	}

	protected JToolBar createToolBar() {

		final JToolBar toolBar = new JToolBar();
		toolBar.setRollover(true);

		final JComboBox<AbstractDrawComputer> computeCombo = new JComboBox<>(
				this.controller.getDrawComputers().values().toArray(new AbstractDrawComputer[] {}));
		computeCombo.setRenderer(new AbstractDrawComputerComboRenderer());
		computeCombo.addItemListener(e -> {
			this.controller.selectDrawComputer(((AbstractDrawComputer) e.getItem()).getId());
			// configuration
			this.loadConfigurationPanel(((AbstractDrawComputer) e.getItem()));
		});
		final AbstractDrawComputer selectedComputer = this.controller.getDrawComputers().values().iterator().next();
		this.controller.selectDrawComputer(selectedComputer.getId());
		this.loadConfigurationPanel(selectedComputer);

		//		this.startButton.setEnabled(!this.controller.isRunning());
		this.startButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.start", true, e -> this.controller.start()));

		//		this.stopButton.setEnabled(this.controller.isRunning());
		this.stopButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.stop", true, e -> this.controller.stop()));

		//		this.loopButton.setEnabled(!this.controller.isRunning());
		this.loopButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.loop", true, e -> this.controller.loop()));

		this.cleanButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.clean", true, e -> this.controller.clear()));
		final JButton resetButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.reset", true, e -> this.controller.reset()));

		// zoom
		final JButton zoomInButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.zoom.in", true, e -> this.controller.zoomIn()));
		final JButton zoomOutButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.zoom.out", true, e -> this.controller.zoomOut()));

		// translate
		final JButton translateUpButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.translate.up", true, e -> this.controller.translateUp()));
		final JButton translateDownButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.translate.down", true, e -> this.controller.translateDown()));
		final JButton translateLeftButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.translate.left", true, e -> this.controller.translateLeft()));
		final JButton translateRightButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.translate.right", true, e -> this.controller.translateRight()));

		// negative view
		final JButton negativeViewButton = SwingDrawFactory.createButton(SwingDrawFactory.createAction("swing.tool.bar.button.negative.view", true, e -> {
			this.controller.setNegativeView(!this.controller.isNegativeView());
			this.drawer.setBackground(SwingDrawFactory.getColor(DrawShapes.negativeColor(SwingDrawFactory.getDrawColor(this.drawer.getBackground()))));
		}));

		toolBar.add(computeCombo);
		toolBar.addSeparator();
		toolBar.add(this.startButton);
		toolBar.add(this.stopButton);
		toolBar.add(this.loopButton);
		toolBar.add(this.cleanButton);
		toolBar.add(resetButton);
		toolBar.add(zoomInButton);
		toolBar.add(zoomOutButton);
		toolBar.add(translateUpButton);
		toolBar.add(translateDownButton);
		toolBar.add(translateLeftButton);
		toolBar.add(translateRightButton);
		toolBar.add(negativeViewButton);

		this.manageButtonStates();

		return toolBar;
	}

	private void loadConfigurationPanel(final AbstractDrawComputer computer) {
		this.configurationPanel.setConfigurable(computer);
	}

	protected void manageButtonStates() {
		if (this.startButton != null) {
			this.startButton.setEnabled(!this.controller.isRunning());
		}
		if (this.stopButton != null) {
			this.stopButton.setEnabled(this.controller.isRunning());
		}
		if (this.loopButton != null) {
			this.loopButton.setEnabled(!this.controller.isRunning());
		}
		if (this.cleanButton != null) {
			this.cleanButton.setEnabled(!this.controller.isRunning());
		}
	}

	/**
	 * @return the drawer
	 */
	protected SwingDrawer getDrawer() {
		return this.drawer;
	}

	@Override
	public void actionPerformed(final DrawControllerEvent event) {
		this.manageButtonStates();
	}

	private void setLocale(final ItemEvent e) {
		Locale locale;
		if (Locale.FRANCE.getDisplayLanguage(Locale.FRANCE).equals(((JRadioButtonMenuItem)e.getItem()).getText())) {
			locale = Locale.FRANCE;
		} else {
			locale = Locale.ENGLISH;
		}

		SwingDrawFactory.setCurrentLocale(locale);
		this.configurationPanel.loadConfigurationPanel();
		this.globalConfigurationDialog.setTitle(I18ns.getLabel(SwingDrawFactory.getCurrentLocale(), this.globalConfigurationDrawBean));
		this.globalConfigurationPanel.setConfigurable(this.controller);
	}

	@Override
	public void mouseWheelMoved(final MouseWheelEvent e) {
		final int notches = e.getWheelRotation();
		if (notches < 0) {
			this.controller.zoomIn();
		} else {
			this.controller.zoomOut();
		}
	}

	@Override
	public void mouseClicked(final MouseEvent event) {
		// Do nothing
	}

	@Override
	public void mouseEntered(final MouseEvent event) {
		// Do nothing
	}

	@Override
	public void mouseExited(final MouseEvent event) {
		// Do nothing
	}

	@Override
	public void mousePressed(final MouseEvent event) {
		if ((event.getModifiers() & MouseEvent.BUTTON1_MASK) > 0) {
			this.pressedPoint = new PointDraw(event.getX(), -event.getY());
		}
	}

	@Override
	public void mouseReleased(final MouseEvent event) {
		if ((event.getModifiers() & MouseEvent.BUTTON1_MASK) > 0) {
			final PointDraw releasedPoint = new PointDraw(event.getX(), -event.getY());
			final VectorDraw translationVector = new VectorDraw(this.pressedPoint, releasedPoint);
			this.controller.setTranslationVector(translationVector);
		}
	}

	@Override
	public void keyPressed(final KeyEvent event) {
		final String easterEggString = this.easterEgg.getEasterEgg(event.getKeyChar());
		if (easterEggString != null) {
			this.setTitle(this.getTitle() + easterEggString);
		}
	}

	@Override
	public void keyReleased(final KeyEvent arg0) {
		// Do nothing
	}

	@Override
	public void keyTyped(final KeyEvent arg0) {
		// Do nothing
	}
}
