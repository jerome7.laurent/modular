/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.swing.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.configuration.PropertyComponent;
import org.jerome.laurent.jdrawer.api.i18n.I18ns;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.swing.SwingDrawFactory;

/**
 * @author Jérôme LAURENT
 *
 * @param <T> Type
 */
public class SwingPropertyComponent<T> implements PropertyComponent<T> {

	private final JComponent component;
	private final Class<T> classValue;
	private final T value;

	class ColorCellRenderer extends JButton implements ListCellRenderer {
		public ColorCellRenderer() {
			this.setOpaque(true);

		}
		boolean b=false;
		@Override
		public void setBackground(final Color bg) {
			if(!this.b)
			{
				return;
			}

			super.setBackground(bg);
		}
		@Override
		public Component getListCellRendererComponent(
				final JList list,
				final Object value,
				final int index,

				final boolean isSelected,
				final boolean cellHasFocus)
		{

			this.b=true;
			this.setText(" ");
			this.setBackground((Color)value);
			this.b=false;
			return this;
		}
	}


	public SwingPropertyComponent(final T value) {
		super();
		this.value = value;
		this.classValue = (Class<T>) value.getClass();
		if (value instanceof EnumDrawBean[]) {
			this.component = new JComboBox<>((EnumDrawBean[]) value);
			for (final EnumDrawBean bean : (EnumDrawBean[]) value) {
				if (bean.isSelected()) {
					((JComboBox)this.component).setSelectedItem(bean);
				}
			}
			((JComboBox)this.component).setRenderer(new EnumDrawBeanComboBoxRenderer());
		} else if (this.classValue == Boolean.class) {
			//			this.component = new JToggleButton("I", (boolean) value);
			//			this.component = new JCheckBox((String) null, (boolean) value);
			if (value == Boolean.TRUE) {
				this.component = new JComboBox(new Boolean[] {Boolean.TRUE, Boolean.FALSE});
			} else {
				this.component = new JComboBox(new Boolean[] {Boolean.FALSE, Boolean.TRUE});
			}
		} else if (this.classValue == DrawColor.class) {
			final Color[] colors = {Color.BLACK, Color.DARK_GRAY, Color.GRAY, Color.LIGHT_GRAY, Color.WHITE, Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.PINK, Color.RED, new Color(102, 0, 180)};
			final JComboBox<Color> colorBox = new JComboBox<>(colors);
			colorBox.setRenderer(new ColorCellRenderer());
			colorBox.setPreferredSize(new Dimension(50,20));
			this.component = colorBox; // new JColorChooser(SwingDrawFactory.getColor((DrawColor) value));
		} else {
			this.component = new JTextField(value.toString());
		}
		this.component.setPreferredSize(new Dimension(50, 22));
	}

	@Override
	public T getValue() {
		if (this.classValue == Boolean.class) {
			return (T) ((JComboBox)this.component).getSelectedItem();
		} else if (this.classValue == EnumDrawBean[].class) {
			final EnumDrawBean selectedItem = (EnumDrawBean) ((JComboBox)this.component).getSelectedItem();
			EnumDrawBean.setSelected((EnumDrawBean[]) this.value, selectedItem.getEnumeration());
			return this.value;
		} else if (this.classValue == DrawColor.class) {
			return (T) SwingDrawFactory.getDrawColor((Color) ((JComboBox)this.component).getSelectedItem());
		} else {
			return this.getValue(((JTextField)this.component).getText());
		}
	}

	@SuppressWarnings("unchecked")
	private T getValue(final String value) {
		if (this.classValue == Long.class) {
			return (T) Long.valueOf(value);
		} else if (this.classValue == Integer.class) {
			return (T) Integer.valueOf(value);
		} else if (this.classValue == Double.class) {
			return (T) Double.valueOf(value);
		} else if (this.classValue == Boolean.class) {
			return (T) Boolean.valueOf(value);
		}

		return (T) value;
	}

	@Override
	public void setValue(final T defaultValue) {
		if (this.classValue == String[].class || this.classValue == Boolean.class) {
			((JComboBox)this.component).setSelectedItem(defaultValue);
		} else {
			((JTextField)this.component).setText(defaultValue.toString());
		}
	}

	/**
	 * @return the component
	 */
	public JComponent getComponent() {
		return this.component;
	}

	/**
	 *
	 * @param error
	 */
	@Override
	public void setError(final DrawError error) {
		if (error == null) {
			this.resetError();
		} else {
			this.getComponent().setBackground(new Color(255, 225, 225));
			this.getComponent().setToolTipText(I18ns.getLabel(SwingDrawFactory.getCurrentLocale(), error));
		}
	}

	/**
	 *
	 */
	public void resetError() {
		this.getComponent().setBackground(Color.WHITE);
		this.getComponent().setToolTipText(null);
	}
}
