/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.computer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.MacroDrawShape;

/**
 * @author Jérôme LAURENT
 *
 */
public abstract class AbstractIterativeDrawComputer extends AbstractCloneDrawComputer {

	private static final Logger LOGGER = Logger.getLogger(AbstractIterativeDrawComputer.class.getName());

	public static final String TEMPO_PROPERTY_ID = "tempo";

	public static final String ITERATION_NUMBER_PROPERTY_ID = "iteration.number";

	private List<DrawShape> previousShapes;

	private int currrentIteration;

	private final ForkJoinPool pool;

	private long tempo;

	private int iterationNumber;

	private boolean isPoolComputeAvailable;

	private final int computeThreadNumber;

	public AbstractIterativeDrawComputer() {
		this.tempo = this.getDefaultTempo();
		this.iterationNumber = this.getDefaultIterationNumber();

		this.getConfiguration().addNumberProperty(TEMPO_PROPERTY_ID, this.getTempo(), 0L, null, 100L);
		this.getConfiguration().addNumberProperty(ITERATION_NUMBER_PROPERTY_ID, this.getIterationNumber(), 1, this.getMaximumtIterationNumber(), 1);
		this.computeThreadNumber = Runtime.getRuntime().availableProcessors() - 2;
		if (this.computeThreadNumber > 1) {
			this.pool = new ForkJoinPool(Math.max(1, this.computeThreadNumber));
		} else {
			this.pool = null;
		}
	}

	/**
	 *
	 * @author Jérôme LAURENT
	 *
	 */
	private static final class ComputeShapeTask extends RecursiveTask<List<DrawShape>> {
		private static final long serialVersionUID = -5779989316239792959L;

		private final List<DrawShape> previousShapes;

		private final AbstractIterativeDrawComputer computer;

		public ComputeShapeTask(final AbstractIterativeDrawComputer computer, final List<DrawShape> previousShapes) {
			super();
			this.computer = computer;
			this.previousShapes = previousShapes;
		}


		@Override
		protected List<DrawShape> compute() {

			final int size = this.previousShapes.size();

			if (size > 1) {

				final int halfSize = size / 2;

				final ComputeShapeTask firstTask = new ComputeShapeTask(this.computer, this.previousShapes.subList(0, halfSize));
				firstTask.fork();

				final ComputeShapeTask secondTask = new ComputeShapeTask(this.computer, this.previousShapes.subList(halfSize, size));
				secondTask.fork();

				final List<DrawShape> result = new LinkedList<>();
				result.addAll(firstTask.join());
				result.addAll(secondTask.join());

				return result;
			}


			return this.computer.cloneCompute(this.previousShapes);
		}
	}

	protected List<DrawShape> cloneCompute(final List<DrawShape> previousShapes) {
		final List<DrawShape> clonedShapes = new ArrayList<>(this.previousShapes.size());
		for (final DrawShape shape : this.previousShapes) {
			try {
				clonedShapes.add(shape.clone());
			} catch (final CloneNotSupportedException e) {
				if (LOGGER.isLoggable(Level.SEVERE)) {
					LOGGER.log(Level.SEVERE, e.getLocalizedMessage(), e);
				}
			}
		}
		final List<DrawShape> newShapes = this.computeNextIteration(clonedShapes);
		return newShapes;
	}
	/**
	 *
	 */
	@Override
	public boolean hasNext() {
		return this.currrentIteration < this.getIterationNumber();
	}

	/**
	 *
	 */
	@Override
	protected DrawShape computeNext() {

		if (this.currrentIteration == 0) {
			this.previousShapes = this.getFirstShapes();
		} else {
			if (this.pool != null && this.isPoolComputeAvailable()) {
				this.previousShapes = this.pool.invoke(new ComputeShapeTask(this, this.previousShapes));
			} else {
				this.previousShapes = this.cloneCompute(this.previousShapes);
			}
		}

		final MacroDrawShape shape = new MacroDrawShape();
		shape.setTempo(this.tempo);
		shape.getChildren().add(DrawShapes.CLEAR_DRAW_SHAPE);
		shape.getChildren().addAll(this.previousShapes);
		shape.setColor(this.color);

		this.currrentIteration++;

		return shape;
	}

	@Override
	public void reset() {
		this.currrentIteration = 0;
		this.previousShapes = null;
	}

	/**
	 * @return the tempo
	 */
	protected long getTempo() {
		return this.tempo;
	}

	/**
	 * Get default tempo.
	 * @return Return the default tempo.
	 */
	protected long getDefaultTempo() {
		return 500L;
	}

	/**
	 * Get the default iteration number.
	 * @return Return the default iteration number.
	 */
	protected int getDefaultIterationNumber() {
		return 7;
	}

	/**
	 * Get the maximum iteration number.
	 * @return Return the maximum iteration number.
	 */
	protected int getMaximumtIterationNumber() {
		return 9;
	}

	/**
	 * Set tempo field.
	 * @param tempo the tempo to set
	 */
	protected void setTempo(final long tempo) {
		this.tempo = tempo;
	}

	/**
	 * Get iteration number.
	 * @return the iterationNumber
	 */
	protected int getIterationNumber() {
		return this.iterationNumber;
	}

	/**
	 * @param iterationNumber the iterationNumber to set
	 */
	protected void setIterationNumber(final int iterationNumber) {
		this.iterationNumber = iterationNumber;
	}

	@Override
	public List<DrawError> applyConfiguration() {
		final List<DrawError> errors = super.applyConfiguration();

		this.setTempo(this.getPropertyValue(Long.class, TEMPO_PROPERTY_ID));
		this.setIterationNumber(this.getPropertyValue(Integer.class, ITERATION_NUMBER_PROPERTY_ID));

		return errors;
	}


	/**
	 * @return the isPoolComputeAvailable
	 */
	public boolean isPoolComputeAvailable() {
		return this.isPoolComputeAvailable;
	}

	/**
	 * @param isPoolComputeAvailable the isPoolComputeAvailable to set
	 */
	public void setPoolComputeAvailable(final boolean isPoolComputeAvailable) {
		this.isPoolComputeAvailable = isPoolComputeAvailable;
	}
	/**
	 * Get the first draw shapes.
	 * @return Return the first draw shapes.
	 */

	protected abstract List<DrawShape> getFirstShapes();

	/**
	 * Compute the next iteration.
	 * @param previousShapes
	 * @return Return the draw shape of the next iteration.
	 */
	protected abstract List<DrawShape> computeNextIteration(final List<DrawShape> previousShapes);

}
