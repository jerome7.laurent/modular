/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

import org.jerome.laurent.jdrawer.api.controller.DrawController;

/**
 * @author Jérôme LAURENT
 *
 */
public final class DrawShapes {

	public static final DrawShape CLEAR_DRAW_SHAPE = new SimpleDrawShape();

	public static final double GOLDEN_MEAN = 1.61803398875;

	private DrawShapes() {
		super();
	}

	/**
	 *
	 * @param translationVector
	 * @param shapes
	 */
	public static void translate(final VectorDraw translationVector, final DrawShape... shapes) {
		if (translationVector == null) {
			return;
		}

		for (final DrawShape shape : shapes) {
			if (shape instanceof CircleDrawShape) {
				final CircleDrawShape circle = (CircleDrawShape) shape;
				translate(translationVector, circle.getPoint1());
			} else if (shape instanceof LineDrawShape) {
				final LineDrawShape line = (LineDrawShape) shape;
				translate(translationVector, line.getPoint1(), line.getPoint2());
			} else if (shape instanceof PointDrawShape) {
				final PointDrawShape point = (PointDrawShape) shape;
				translate(translationVector, point.getPoint1());
			} else if (shape instanceof PolygonDrawShape) {
				final PolygonDrawShape polygon = (PolygonDrawShape) shape;
				translate(translationVector, polygon.getPoints());
			} else if (shape instanceof MacroDrawShape) {
				translate(translationVector, ((MacroDrawShape)shape).getChildren().toArray(new DrawShape[((MacroDrawShape)shape).getChildren().size()]));
			}
		}
	}

	/***
	 * Translate the point with the vector.
	 * @param vector
	 * @param points
	 */
	public static void translate(final VectorDraw vector, final PointDraw... points) {

		if (vector == null ||points == null) {
			return;
		}

		for (final PointDraw point : points) {
			point.setX(point.getX() + vector.getX());
			point.setY(point.getY() + vector.getY());
			point.setZ(point.getZ() + vector.getZ());
		}
	}

	/**
	 *
	 * @param zoomFactor
	 * @param shapes
	 */
	public static void zoom(final double zoomFactor, final List<DrawShape> shapes) {
		for (final DrawShape shape : shapes) {
			zoom(zoomFactor, shape);
		}
	}

	/**
	 * Zoom shape with zoom factor.
	 * @param zoomFactor
	 * @param shapes
	 */
	public static void zoom(final double zoomFactor, final DrawShape... shapes) {
		for (final DrawShape shape : shapes) {
			zoom(zoomFactor, shape);
		}
	}

	/**
	 *
	 * @param zoomFactor
	 * @param shape
	 */
	public static void zoom(final double zoomFactor, final DrawShape shape) {
		if (shape instanceof CircleDrawShape) {
			final CircleDrawShape circle = (CircleDrawShape) shape;
			circle.setRadius(circle.getRadius() * zoomFactor);
		} else if (shape instanceof LineDrawShape) {
			final LineDrawShape line = (LineDrawShape) shape;
			zoom(zoomFactor, line.getPoint1(), line.getPoint2());
		} else if (shape instanceof PointDrawShape) {
			final PointDrawShape point = (PointDrawShape) shape;
			zoom(zoomFactor, point.getPoint1());
		} else if (shape instanceof PolygonDrawShape) {
			final PolygonDrawShape polygon = (PolygonDrawShape) shape;
			zoom(zoomFactor, polygon.getPoints());
		} else if (shape instanceof MacroDrawShape) {
			zoom(zoomFactor, ((MacroDrawShape)shape).getChildren().toArray(new DrawShape[((MacroDrawShape)shape).getChildren().size()]));
		}
	}

	/**
	 *
	 * @param zoomFactor
	 * @param points
	 */
	public static void zoom(final double zoomFactor, final PointDraw... points) {
		for (final PointDraw point : points) {
			point.setX(point.getX() * zoomFactor);
			point.setY(point.getY() * zoomFactor);
			point.setZ(point.getZ() * zoomFactor);
		}
	}

	/**
	 *
	 * @param point
	 * @return
	 */
	public static PointDraw negative(final PointDraw point) {
		return new PointDraw(-point.getX(), -point.getY(), -point.getZ());
	}

	/**
	 *
	 * @param point
	 * @return
	 */
	public static VectorDraw negative(final VectorDraw point) {
		return new VectorDraw(-point.getX(), -point.getY(), -point.getZ());
	}

	/**
	 *
	 * @param point1
	 * @param point2
	 * @return
	 */
	public static VectorDraw add(final VectorDraw point1, final VectorDraw point2) {
		return new VectorDraw(
				point1.getX() + point2.getX(),
				point1.getY() + point2.getY(),
				point1.getZ() + point2.getZ()
				);
	}

	/**
	 *
	 * @param numerator
	 * @param denumerator
	 * @return
	 */
	public static VectorDraw scalar(final VectorDraw v, final int numerator,final int denumerator) {
		return new VectorDraw(
				(v.getX() * numerator) / denumerator,
				(v.getY() * numerator) / denumerator,
				(v.getZ() * numerator) / denumerator
				);
	}

	public static VectorDraw scalar(final VectorDraw v, final double scalar) {
		return new VectorDraw(
				v.getX() * scalar,
				v.getY() * scalar,
				v.getZ() * scalar
				);
	}

	/**
	 *
	 * @param v
	 * @param numerator
	 * @return
	 */
	public static VectorDraw scalar(final VectorDraw v, final int numerator) {
		return scalar(v, numerator, 1);
	}

	/**
	 *
	 * @param v
	 * @return
	 */
	public static VectorDraw perpendicular(final VectorDraw v) {
		return new VectorDraw(-v.getY(), v.getX());
	}

	/**
	 *
	 * @param v
	 * @return
	 */
	public static VectorDraw reversePerpendicular(final VectorDraw v) {
		return new VectorDraw(v.getY(), -v.getX());
	}

	/**
	 *
	 * @param length
	 * @return
	 */
	public static LineDrawShape[] createEquilateralTriangleLines(final int length) {
		final PointDraw[] points = createEquilateralTrianglePoints(length);

		final LineDrawShape[] lines = createClosedLines(points);

		DrawShapes.translate(new VectorDraw(0, -((length * Math.sqrt(3.0)) / 12.0)), lines);

		return lines;
	}

	/**
	 *
	 * @param largeDiagonal
	 * @param smallDiagonal
	 * @return
	 */
	public static PointDraw[] createDiamondPoints(final double largeDiagonal, final double smallDiagonal) {

		final double halfLargeDiagonal = largeDiagonal / 2.0;
		final double halfSmallDiagonal = smallDiagonal / 2.0;

		return new PointDraw[] {
				new PointDraw(halfLargeDiagonal, 0.0),
				new PointDraw(0.0, halfSmallDiagonal),
				new PointDraw(-halfLargeDiagonal, 0.0),
				new PointDraw(0.0, -halfSmallDiagonal)
		};
	}

	public static LineDrawShape[] createDiamondLines(final double largeDiagonal, final double smallDiagonal) {
		final PointDraw[] points = createDiamondPoints(largeDiagonal, smallDiagonal);
		return createClosedLines(points);
	}

	/**
	 *
	 * @param largeDiagonal
	 * @param smallDiagonal
	 * @return
	 */
	public static PointDraw[] createRectanglePoints(final double largeDiagonal, final double smallDiagonal) {

		final double halfLargeDiagonal = largeDiagonal / 2.0;
		final double halfSmallDiagonal = smallDiagonal / 2.0;

		return new PointDraw[] {
				new PointDraw(halfLargeDiagonal, halfSmallDiagonal),
				new PointDraw(-halfLargeDiagonal, halfSmallDiagonal),
				new PointDraw(-halfLargeDiagonal, -halfSmallDiagonal),
				new PointDraw(halfLargeDiagonal, -halfSmallDiagonal)
		};
	}

	public static LineDrawShape[] createRectangleLines(final double largeDiagonal, final double smallDiagonal) {
		return createClosedLines(createRectanglePoints(largeDiagonal, smallDiagonal));
	}

	public static LineDrawShape[] createClosedLines(final PointDraw[] points) {
		final LineDrawShape[] lines = new LineDrawShape[points.length];
		for (int i = 0; i < points.length; i++) {
			lines[i] = new LineDrawShape(points[i], points[(i+1) % points.length]);
		}
		return lines;
	}

	/**
	 *
	 * @param length
	 * @return
	 */
	public static PointDraw[] createEquilateralTrianglePoints(final int length) {
		return createEquilateralTrianglePoints(new PointDraw(), length);
	}

	/**
	 *
	 * @param length
	 * @return
	 *   3
	 *  / \
	 * 1---2
	 */
	public static PointDraw[] createEquilateralTrianglePoints(final PointDraw center, final int length) {

		final int halfLength = length/2;

		final PointDraw point1 = new PointDraw(-halfLength, 0);
		final PointDraw point2 = new PointDraw(halfLength, 0);
		final PointDraw point3 = new PointDraw(0.0, length * Math.sqrt(3.0)/2.0);


		final PointDraw[] points = new PointDraw[] {point1, point2, point3};
		final PointDraw barycenter = barycenter(points);

		translate(add(negative(new VectorDraw(barycenter)), new VectorDraw(center)), points);

		return points;
	}

	/**
	 *
	 * @param point center of the square.
	 * @param length length of the square.
	 * @return Returns the points of the square.
	 * 3--2
	 * |  |
	 * 0--1
	 */
	public static PointDraw[] createSquarePoints(final PointDraw point, final double length) {
		final double halfLength = length / 2.0;

		final PointDraw[] points = new PointDraw[] {
				new PointDraw(point.getX() - halfLength, point.getY() - halfLength),
				new PointDraw(point.getX() + halfLength, point.getY() - halfLength),
				new PointDraw(point.getX() + halfLength, point.getY() + halfLength),
				new PointDraw(point.getX() - halfLength, point.getY() + halfLength)
		};

		return points;
	}


	/**
	 *
	 * @param length
	 * @return
	 */
	public static PointDraw[] createSquarePoints(final double length) {
		return createSquarePoints(new PointDraw(), length);
	}



	/**
	 *
	 * @param center
	 * @param length
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createCube(final PointDraw center, final int length, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createCuboid(center, length, length, length, fillColor, stokeColor, resolution);
	}

	/**
	 *
	 * @param length
	 * @param width
	 * @param height
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createCuboid(final int length, final int width, final int height, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createCuboid(new PointDraw(), length, width, height, fillColor, stokeColor, resolution);
	}

	/**
	 *
	 * @param center
	 * @param length
	 * @param width
	 * @param height
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createCuboid(final PointDraw center, final int length, final int width, final int height, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		final int halfLength = length / 2;
		final int halfWidth = width / 2;
		final int halfHeight = height / 2;

		//  4----5
		// /    /|
		// 3---2 6
		// |   |/
		// 0---1
		final PointDraw[] points = new PointDraw[8];
		points[0] = new PointDraw(center.getX() - halfLength, center.getY() - halfHeight, center.getZ() + halfWidth);
		points[1] = new PointDraw(points[0].getX() + length, points[0].getY(), points[0].getZ());
		points[2] = new PointDraw(points[1].getX(), points[1].getY() + height, points[1].getZ());
		points[3] = new PointDraw(points[0].getX(), points[0].getY() + height, points[0].getZ());
		points[4] = new PointDraw(points[3].getX(), points[3].getY(), points[3].getZ() - width);
		points[5] = new PointDraw(points[4].getX() + length, points[4].getY(), points[4].getZ());
		points[6] = new PointDraw(points[5].getX(), points[5].getY() - height, points[5].getZ());
		points[7] = new PointDraw(points[6].getX() - length, points[6].getY(), points[6].getZ());

		final MacroDrawShape cube = new MacroDrawShape();

		// face 0, 1, 2, 3
		computeResolution(cube.getChildren(), fillColor, stokeColor, resolution, points[0], points[1], points[2], points[3]);

		// face 6, 7, 4, 5
		computeResolution(cube.getChildren(), fillColor, stokeColor, resolution, points[6], points[7], points[4], points[5]);

		// face 1, 6, 5, 2
		computeResolution(cube.getChildren(), fillColor, stokeColor, resolution, points[1], points[6], points[5], points[2]);
		//
		// face 7, 0, 3, 4
		computeResolution(cube.getChildren(), fillColor, stokeColor, resolution, points[7], points[0], points[3], points[4]);

		// face 3, 2, 5, 4
		computeResolution(cube.getChildren(), fillColor, stokeColor, resolution, points[3], points[2], points[5], points[4]);

		// face 1, 0, 7, 6
		computeResolution(cube.getChildren(), fillColor, stokeColor, resolution, points[1], points[0], points[7], points[6]);

		return cube;
	}

	/**
	 *
	 * @param drawShapes
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @param points
	 */
	public static final void computeResolution(final List<DrawShape> drawShapes, final DrawColor fillColor, final DrawColor stokeColor, final int resolution, final PointDraw... points) {

		if (points.length == 4) {
			// cuboid
			// 3--2
			// |  |
			// 0--1
			final double xStepX = (points[1].getX() - points[0].getX()) / resolution;
			final double xStepY = (points[1].getY() - points[0].getY()) / resolution;
			final double xStepZ = (points[1].getZ() - points[0].getZ()) / resolution;
			final double yStepX = (points[2].getX() - points[1].getX()) / resolution;
			final double yStepY = (points[2].getY() - points[1].getY()) / resolution;
			final double yStepZ = (points[2].getZ() - points[1].getZ()) / resolution;

			for (int i = 0; i < resolution; i++) {
				for (int j = 0; j < resolution; j++) {
					drawShapes.add(new PolygonDrawShape(new PointDraw[] {
							new PointDraw(
									points[0].getX() + (j * xStepX) + (i * yStepX),
									points[0].getY() + (j * xStepY) + (i * yStepY),
									points[0].getZ() + (j * xStepZ) + (i * yStepZ)),
							new PointDraw(
									points[0].getX() + ((j + 1) * xStepX) + (i * yStepX),
									points[0].getY() + ((j + 1) * xStepY) + (i * yStepY),
									points[0].getZ() + ((j + 1) * xStepZ) + (i * yStepZ)),
							new PointDraw(
									points[0].getX() + ((j + 1) * xStepX) + ((i + 1) * yStepX),
									points[0].getY() + ((j + 1) * xStepY) + ((i + 1) * yStepY),
									points[0].getZ() + ((j + 1) * xStepZ) + ((i + 1) * yStepZ)),
							new PointDraw(
									points[0].getX() + (j * xStepX) + ((i + 1) * yStepX),
									points[0].getY() + (j * xStepY) + ((i + 1) * yStepY),
									points[0].getZ() + (j * xStepZ) + ((i + 1) * yStepZ))
					}, fillColor, stokeColor));
				}
			}
		} else if (points.length == 3) {
			// triangle
			//   2
			//  / \
			// 0---1
			final double xStepX = (points[1].getX() - points[0].getX()) / resolution;
			final double xStepY = (points[1].getY() - points[0].getY()) / resolution;
			final double xStepZ = (points[1].getZ() - points[0].getZ()) / resolution;
			final double yStepX = (points[0].getX() - points[2].getX()) / resolution;
			final double yStepY = (points[0].getY() - points[2].getY()) / resolution;
			final double yStepZ = (points[0].getZ() - points[2].getZ()) / resolution;

			for (int i = 0; i < resolution; i++) {
				for (int j = 0; j <= i; j++) {
					final PointDraw[] currentPoints = new PointDraw[] {
							new PointDraw(
									points[2].getX() + (j * xStepX) + ((i + 1) * yStepX),
									points[2].getY() + (j * xStepY) + ((i + 1) * yStepY),
									points[2].getZ() + (j * xStepZ) + ((i + 1) * yStepZ)),
							new PointDraw(
									points[2].getX() + ((j + 1) * xStepX) + ((i + 1) * yStepX),
									points[2].getY() + ((j + 1) * xStepY) + ((i + 1) * yStepY),
									points[2].getZ() + ((j + 1) * xStepZ) + ((i + 1) * yStepZ)),
							new PointDraw(
									points[2].getX() + (j * xStepX) + (i * yStepX),
									points[2].getY() + (j * xStepY) + (i * yStepY),
									points[2].getZ() + (j * xStepZ) + (i * yStepZ))
					};
					drawShapes.add(new PolygonDrawShape(currentPoints, fillColor, stokeColor));
					if (i > 0 && j < i) {
						// triangle
						// 2---3
						//  \ /
						//   1
						final PointDraw[] reversePoints = new PointDraw[] {
								currentPoints[1].clone(),
								new PointDraw(
										points[2].getX() + ((j+1) * xStepX) + (i * yStepX),
										points[2].getY() + ((j+1) * xStepY) + (i * yStepY),
										points[2].getZ() + ((j+1) * xStepZ) + (i * yStepZ)),
								currentPoints[2].clone()
						};
						drawShapes.add(new PolygonDrawShape(reversePoints, fillColor, stokeColor));
					}
				}
			}
		}
	}

	/**
	 *
	 * @param center
	 * @param length
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createTetrahedron(final PointDraw center, final int length, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {

		//   2
		//	/ \
		// 0---1
		final PointDraw[] equilateralTrianglePoints = createEquilateralTrianglePoints(new PointDraw(), length);

		final MacroDrawShape tetrahedron = new MacroDrawShape();

		final PointDraw[] points = new PointDraw[4];
		points[0] = equilateralTrianglePoints[0];
		points[1] = equilateralTrianglePoints[1];
		points[2] = equilateralTrianglePoints[2];
		final double d = norm(new VectorDraw(points[0], barycenter(equilateralTrianglePoints)));
		points[3] = new PointDraw(0.0, 0.0, - Math.sqrt((length * length) - (d * d)));

		final PointDraw c = barycenter(points);
		translate(new VectorDraw(c, center), points);

		computeResolution(tetrahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {points[0],points[1], points[2]});
		computeResolution(tetrahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {points[1],points[3], points[2]});
		computeResolution(tetrahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {points[0],points[2], points[3]});
		computeResolution(tetrahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {points[0],points[3], points[1]});

		return tetrahedron;
	}

	/**
	 *
	 * @param length
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createTetrahedron(final int length, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createTetrahedron(new PointDraw(), length, fillColor, stokeColor, resolution);
	}

	/**
	 *
	 * @param center
	 * @param length
	 * @param fillColor
	 * @param stokeColor
	 * @return
	 */
	public static MacroDrawShape createCube(final PointDraw center, final int length, final DrawColor fillColor, final DrawColor stokeColor) {
		return createCube(center, length, fillColor, stokeColor, 1);
	}

	/**
	 *
	 * @param length
	 * @return
	 */
	public static MacroDrawShape createCube(final int length, final DrawColor fillColor, final DrawColor stokeColor) {
		return createCube(new PointDraw(), length, fillColor, stokeColor);
	}

	public static MacroDrawShape createCube(final int length, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createCube(new PointDraw(), length, fillColor, stokeColor, resolution);
	}


	public static MacroDrawShape createCylinder(final PointDraw center, final double diameter, final double height, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {

		final MacroDrawShape cylinder = new MacroDrawShape();
		final double radius = diameter / 2.0;
		final double dh = height / resolution;
		final double mh = height / 2.0;
		final double angle = Math.PI / 2.0;
		final double dt = angle / resolution;
		final double dr = radius / resolution;
		final int linePointNumbers = resolution * 4;
		PointDraw[] previousLine = null;
		PointDraw[] currentLine = new PointDraw[linePointNumbers];

		for (int i = 0; i < resolution; i++) {
			final double da = (i * dt);
			currentLine[i] = new PointDraw(radius * Math.sin(angle - da), mh, radius * Math.cos(angle - da));
			currentLine[resolution + i] = new PointDraw(-currentLine[i].getZ(), mh, currentLine[i].getX());
			currentLine[2 * resolution + i] = new PointDraw(-currentLine[i].getX(), mh, -currentLine[i].getZ());
			currentLine[3 * resolution + i] = new PointDraw(currentLine[i].getZ(), mh, -currentLine[i].getX());
		}

		// disc
		PointDraw[] discPreviousLine = null;
		PointDraw[] discCurrentLine = currentLine;

		for (int i = 1; i <= resolution; i++) {

			discPreviousLine = discCurrentLine;
			discCurrentLine = new PointDraw[linePointNumbers];

			final double r = radius - (dr * i);

			for (int j = 0; j < resolution; j++) {
				final double da = (j * dt);
				discCurrentLine[j] = new PointDraw(r * Math.sin(angle - da), mh, r * Math.cos(angle - da));
				discCurrentLine[resolution + j] = new PointDraw(-discCurrentLine[j].getZ(), mh, discCurrentLine[j].getX());
				discCurrentLine[2 * resolution + j] = new PointDraw(-discCurrentLine[j].getX(), mh, -discCurrentLine[j].getZ());
				discCurrentLine[3 * resolution + j] = new PointDraw(discCurrentLine[j].getZ(), mh, -discCurrentLine[j].getX());
			}

			//			PointDraw p0;
			//			PointDraw p1;

			for (int j = 1; j < linePointNumbers; j++) {
				if (discCurrentLine[j].equals(discCurrentLine[j-1])) {
					// top
					cylinder.getChildren().add(new PolygonDrawShape(
							new PointDraw[] {
									discCurrentLine[j].clone(),
									discPreviousLine[j].clone(),
									discPreviousLine[j-1].clone()
							}, fillColor, stokeColor
							));
					// bottom
					final PointDraw p0 = discCurrentLine[j].clone();
					p0.setY(-mh);
					final PointDraw p1 = discPreviousLine[j-1].clone();
					p1.setY(-mh);
					final PointDraw p2 = discPreviousLine[j].clone();
					p2.setY(-mh);
					cylinder.getChildren().add(new PolygonDrawShape(
							new PointDraw[] {p0, p1, p2}, fillColor, stokeColor
							));
				} else {
					// top
					cylinder.getChildren().add(new PolygonDrawShape(
							new PointDraw[] {
									discCurrentLine[j-1].clone(),
									discCurrentLine[j].clone(),
									discPreviousLine[j].clone(),
									discPreviousLine[j-1].clone()
							}, fillColor, stokeColor
							));
					// bottom
					final PointDraw p0 = discPreviousLine[j-1].clone();
					p0.setY(-mh);
					final PointDraw p1 = discPreviousLine[j].clone();
					p1.setY(-mh);
					final PointDraw p2 = discCurrentLine[j].clone();
					p2.setY(-mh);
					final PointDraw p3 = discCurrentLine[j-1].clone();
					p3.setY(-mh);
					cylinder.getChildren().add(new PolygonDrawShape(
							new PointDraw[] {p0, p1, p2, p3}, fillColor, stokeColor
							));
				}
			}
			if (discCurrentLine[linePointNumbers-1].equals(discCurrentLine[0])) {
				// top
				cylinder.getChildren().add( new PolygonDrawShape(
						new PointDraw[] {
								discCurrentLine[0].clone(),
								discPreviousLine[0].clone(),
								discPreviousLine[linePointNumbers-1].clone()
						}, fillColor, stokeColor
						));
				// bottom
				final PointDraw p0 = discCurrentLine[0].clone();
				p0.setY(-mh);
				final PointDraw p1 = discPreviousLine[linePointNumbers-1].clone();
				p1.setY(-mh);
				final PointDraw p2 = discPreviousLine[0].clone();
				p2.setY(-mh);
				cylinder.getChildren().add(new PolygonDrawShape(
						new PointDraw[] {p0, p1, p2}, fillColor, stokeColor
						));
			} else {
				// top
				cylinder.getChildren().add( new PolygonDrawShape(
						new PointDraw[] {
								discCurrentLine[linePointNumbers-1].clone(),
								discCurrentLine[0].clone(),
								discPreviousLine[0].clone(),
								discPreviousLine[linePointNumbers-1].clone()
						}, fillColor, stokeColor
						));
				// bottom
				final PointDraw p0 = discPreviousLine[linePointNumbers-1].clone();
				p0.setY(-mh);
				final PointDraw p1 = discPreviousLine[0].clone();
				p1.setY(-mh);
				final PointDraw p2 = discCurrentLine[0].clone();
				p2.setY(-mh);
				final PointDraw p3 = discCurrentLine[linePointNumbers-1].clone();
				p3.setY(-mh);
				cylinder.getChildren().add(new PolygonDrawShape(
						new PointDraw[] {p0, p1, p2, p3}, fillColor, stokeColor
						));
			}
		}

		// cylinder
		for (int i = 0; i < resolution; i ++) {
			final double y = currentLine[0].getY() - dh;
			previousLine = currentLine;
			currentLine = new PointDraw[linePointNumbers];

			for (int j = 0; j < linePointNumbers; j++) {
				currentLine[j] = previousLine[j].clone();
				currentLine[j].setY(y);
				if (j > 0) {
					cylinder.getChildren().add( new PolygonDrawShape(
							new PointDraw[] {
									previousLine[j-1].clone(),
									previousLine[j].clone(),
									currentLine[j].clone(),
									currentLine[j-1].clone()
							}, fillColor, stokeColor
							));
				}
			}
			cylinder.getChildren().add( new PolygonDrawShape(
					new PointDraw[] {
							previousLine[linePointNumbers-1].clone(),
							previousLine[0].clone(),
							currentLine[0].clone(),
							currentLine[linePointNumbers-1].clone()
					}, fillColor, stokeColor
					));
		}

		return cylinder;
	}

	public static MacroDrawShape createCylinder(final int radius, final int height, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createCylinder(new PointDraw(), radius, height, fillColor, stokeColor, resolution);
	}

	/**
	 * x = r * cos(theta) * cos(phi)
	 * y = r * cos(theta) * sin(phi)
	 * z = r * sin(theta)
	 * @param center
	 * @param radius
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return sphere
	 */
	public static MacroDrawShape createSphere(final PointDraw center, final double radius, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {

		final double angle = Math.PI / 2.0;
		final double dAngle = angle / resolution;
		PointDraw[] previousLine = null;
		PointDraw[] currentLine = null;
		final MacroDrawShape sphere = new MacroDrawShape();
		final int pointNumber = resolution + 1;

		for (int i = 0; i < pointNumber; i++) {

			previousLine = currentLine;
			currentLine = new PointDraw[pointNumber];

			final double theta = i * dAngle;
			final double cosTheta = radius * Math.cos(theta);
			final double sinTheta = radius * Math.sin(theta);
			for (int j = 0; j < pointNumber; j++) {
				final double phi = j * dAngle;
				currentLine[j] = new PointDraw(cosTheta * Math.sin(phi), sinTheta, cosTheta * Math.cos(phi));
			}

			if (previousLine != null) {
				for (int j = 1; j < pointNumber; j++) {
					final PointDraw a = previousLine[j-1].clone();
					final PointDraw b = previousLine[j].clone();
					final PointDraw c = currentLine[j].clone();
					final PointDraw d = currentLine[j-1].clone();

					if (i != resolution) {
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										a,
										b,
										c,
										d
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), a.getY(), a.getZ()),
										new PointDraw(-d.getX(), d.getY(), d.getZ()),
										new PointDraw(-c.getX(), c.getY(), c.getZ()),
										new PointDraw(-b.getX(), b.getY(), b.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(a.getX(), -a.getY(), a.getZ()),
										new PointDraw(d.getX(), -d.getY(), d.getZ()),
										new PointDraw(c.getX(), -c.getY(), c.getZ()),
										new PointDraw(b.getX(), -b.getY(), b.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), -a.getY(), a.getZ()),
										new PointDraw(-b.getX(), -b.getY(), b.getZ()),
										new PointDraw(-c.getX(), -c.getY(), c.getZ()),
										new PointDraw(-d.getX(), -d.getY(), d.getZ())
								}, fillColor, stokeColor
								));

						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(a.getX(), a.getY(), -a.getZ()),
										new PointDraw(d.getX(), d.getY(), -d.getZ()),
										new PointDraw(c.getX(), c.getY(), -c.getZ()),
										new PointDraw(b.getX(), b.getY(), -b.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), a.getY(), -a.getZ()),
										new PointDraw(-b.getX(), b.getY(), -b.getZ()),
										new PointDraw(-c.getX(), c.getY(), -c.getZ()),
										new PointDraw(-d.getX(), d.getY(), -d.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(a.getX(), -a.getY(), -a.getZ()),
										new PointDraw(b.getX(), -b.getY(), -b.getZ()),
										new PointDraw(c.getX(), -c.getY(), -c.getZ()),
										new PointDraw(d.getX(), -d.getY(), -d.getZ()),

								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), -a.getY(), -a.getZ()),
										new PointDraw(-d.getX(), -d.getY(), -d.getZ()),
										new PointDraw(-c.getX(), -c.getY(), -c.getZ()),
										new PointDraw(-b.getX(), -b.getY(), -b.getZ())

								}, fillColor, stokeColor
								));
					} else {
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										a,
										b,
										c
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), a.getY(), a.getZ()),
										new PointDraw(-d.getX(), d.getY(), d.getZ()),
										new PointDraw(-b.getX(), b.getY(), b.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(a.getX(), -a.getY(), a.getZ()),
										new PointDraw(d.getX(), -d.getY(), d.getZ()),
										new PointDraw(b.getX(), -b.getY(), b.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), -a.getY(), a.getZ()),
										new PointDraw(-b.getX(), -b.getY(), b.getZ()),
										new PointDraw(-d.getX(), -d.getY(), d.getZ())
								}, fillColor, stokeColor
								));

						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(a.getX(), a.getY(), -a.getZ()),
										new PointDraw(d.getX(), d.getY(), -d.getZ()),
										new PointDraw(b.getX(), b.getY(), -b.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), a.getY(), -a.getZ()),
										new PointDraw(-b.getX(), b.getY(), -b.getZ()),
										new PointDraw(-d.getX(), d.getY(), -d.getZ())
								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(a.getX(), -a.getY(), -a.getZ()),
										new PointDraw(b.getX(), -b.getY(), -b.getZ()),
										new PointDraw(c.getX(), -c.getY(), -c.getZ())

								}, fillColor, stokeColor
								));
						sphere.getChildren().add( new PolygonDrawShape(
								new PointDraw[] {
										new PointDraw(-a.getX(), -a.getY(), -a.getZ()),
										new PointDraw(-d.getX(), -d.getY(), -d.getZ()),
										new PointDraw(-b.getX(), -b.getY(), -b.getZ())

								}, fillColor, stokeColor
								));
					}
				}
			}
		}
		return sphere;
	}

	public static MacroDrawShape createSphere(final double radius, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createSphere(new PointDraw(), radius, fillColor, stokeColor, resolution);
	}

	/**
	 *
	 * @param center
	 * @param length
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createOctahedron(final PointDraw center, final double length, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {

		final double halfLength = length / 2.0;
		final double h = halfLength * Math.sqrt(2.0);
		final PointDraw a = new PointDraw(-halfLength, 0.0, halfLength);
		final PointDraw b = new PointDraw(halfLength, 0.0, halfLength);
		final PointDraw c = new PointDraw(halfLength, 0.0, -halfLength);
		final PointDraw d = new PointDraw(-halfLength, 0.0, -halfLength);
		final PointDraw e = new PointDraw(0.0, h, 0.0);
		final PointDraw f = new PointDraw(0.0, -h, 0.0);

		final MacroDrawShape octahedron = new MacroDrawShape();
		// top
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {a, b, e});
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {b, c, e});
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, d, e});
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {d, a, e});

		// bottom
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {a, f, b});
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {b, f, c});
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, f, d});
		computeResolution(octahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {d, f, a});

		return octahedron;
	}

	/**
	 *
	 * @param length
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createOctahedron(final double length, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createOctahedron(new PointDraw(), length, fillColor, stokeColor, resolution);
	}

	/**
	 *
	 * @param center
	 * @param side
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createIcosahedron(final PointDraw center, final int side, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {

		final double halfEdge = side / 2.0;
		final double halfWidth = halfEdge / GOLDEN_MEAN;

		final PointDraw a = new PointDraw(-halfWidth, -halfEdge, 0.0);
		final PointDraw b = new PointDraw(halfWidth, -halfEdge, 0.0);
		final PointDraw c = new PointDraw(halfWidth, halfEdge, 0.0);
		final PointDraw d = new PointDraw(-halfWidth, halfEdge, 0.0);

		final PointDraw e = new PointDraw(-halfEdge, 0.0, halfWidth);
		final PointDraw f = new PointDraw(halfEdge, 0.0, halfWidth);
		final PointDraw g = new PointDraw(halfEdge, 0.0, -halfWidth);
		final PointDraw h = new PointDraw(-halfEdge, 0.0, -halfWidth);

		final PointDraw i = new PointDraw(0.0, -halfWidth, halfEdge);
		final PointDraw j = new PointDraw(0.0, -halfWidth, -halfEdge);
		final PointDraw k = new PointDraw(0.0, halfWidth, -halfEdge);
		final PointDraw l = new PointDraw(0.0, halfWidth, halfEdge);

		final MacroDrawShape icosahedron = new MacroDrawShape();
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, d, l});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, l, f});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, f, g});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, g, k});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {c, k, d});

		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {i, a, b});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {i, b, f});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {i, f, l});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {i, l, e});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {i, e, a});

		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {j, k, g});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {j, g, b});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {j, b, a});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {j, a, h});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {j, h, k});

		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {l, d, e});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {f, b, g});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {h, e, d});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {h, a, e});
		computeResolution(icosahedron.getChildren(), fillColor, stokeColor, resolution, new PointDraw[] {h, d, k});

		return icosahedron;
	}

	/**
	 *
	 * @param side
	 * @param fillColor
	 * @param stokeColor
	 * @param resolution
	 * @return
	 */
	public static MacroDrawShape createIcosahedron(final int side, final DrawColor fillColor, final DrawColor stokeColor, final int resolution) {
		return createIcosahedron(new PointDraw(), side, fillColor, stokeColor, resolution);
	}

	/**
	 * Create regular polygon points.
	 * @param pointNumber Point number.
	 * @param radius Radius.
	 * @return Return regular points.
	 */
	public static PointDraw[] createRegularPolygonPoints(final int pointNumber, final double radius) {
		final PointDraw[] points = new PointDraw[pointNumber];
		final double angularUnit = 2 * Math.PI / pointNumber;
		for (int i = 0; i < points.length; i++) {
			final double angular = angularUnit * i;
			points[i] = new PointDraw(radius * Math.sin(angular), radius * Math.cos(angular));
		}
		return points;
	}

	/**
	 *
	 * @param shape
	 * @param angle
	 */
	public static void rotateX(final DrawShape shape, final double angle) {
		final List<PointDraw> points = getPoints(shape);
		for (final PointDraw point : points) {
			final double z = point.getZ();
			final double y = point.getY();
			point.setY(z * Math.sin(angle) + y * Math.cos(angle));
			point.setZ(z * Math.cos(angle) - y * Math.sin(angle));
		}
	}

	/**
	 *
	 * @param shape
	 * @param angle
	 */
	public static void rotateY(final DrawShape shape, final double angle) {
		final List<PointDraw> points = getPoints(shape);
		for (final PointDraw point : points) {
			final double x = point.getX();
			final double z = point.getZ();
			point.setX(x * Math.cos(angle) - z * Math.sin(angle));
			point.setZ(x * Math.sin(angle) + z * Math.cos(angle));
		}
	}

	/**
	 *
	 * @param shape
	 * @param angle
	 */
	public static void rotateZ(final DrawShape shape, final double angle) {
		final List<PointDraw> points = getPoints(shape);
		for (final PointDraw point : points) {
			final double x = point.getX();
			final double y = point.getY();
			point.setX(x * Math.cos(angle) - y * Math.sin(angle));
			point.setY(x * Math.sin(angle) + y * Math.cos(angle));
		}
	}

	/**
	 *
	 * @param shape
	 * @param psi
	 * @param theta
	 * @param phi
	 */
	public static void rotateXYZ(final DrawShape shape, final double psi, final double theta, final double phi) {
		rotateX(shape, psi);
		rotateY(shape, theta);
		rotateZ(shape, phi);
	}

	/**
	 *
	 * @param shape
	 * @param radian
	 */
	public static void rotateXYZ(final DrawShape shape, final double radian) {
		rotateXYZ(shape, radian, radian, radian);
	}

	public static List<PointDraw> getPoints(final DrawShape shape) {

		if (shape instanceof MacroDrawShape) {
			final List<PointDraw> points = new LinkedList<>();
			for (final DrawShape childShape : ((MacroDrawShape) shape).getChildren()) {
				points.addAll(getPoints(childShape));
			}
			return points;
		} else if (shape instanceof PolygonDrawShape) {
			return Arrays.asList(((PolygonDrawShape)shape).getPoints());
		} else if (shape instanceof LineDrawShape) {
			return Arrays.asList(((LineDrawShape)shape).getPoint1(), ((LineDrawShape)shape).getPoint2());
		} else if (shape instanceof PointDrawShape) {
			return Arrays.asList(((PointDrawShape)shape).getPoint1());
		}

		return Collections.emptyList();
	}

	/**
	 *
	 * @param shapes
	 */
	public static void negativeColor(final DrawShape... shapes) {

		for (final DrawShape shape : shapes) {
			if (shape == null) {
				continue;
			}
			if (shape instanceof MacroDrawShape) {
				negativeColor(((MacroDrawShape)shape).getChildren().toArray(new DrawShape[((MacroDrawShape)shape).getChildren().size()]));
			}
			shape.setColor(negativeColor(shape.getColor()));

			if (shape instanceof FillDrawShape) {
				final FillDrawShape fillShape = (FillDrawShape) shape;
				fillShape.setFillColor(negativeColor(fillShape.getFillColor()));
			}
		}
	}

	/**
	 *
	 * @param color
	 * @return
	 */
	public static DrawColor negativeColor(final DrawColor color) {
		if (color == null) {
			return null;
		}
		return new DrawColor(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue());
	}

	/**
	 *
	 * @param polygon
	 * @return
	 */
	public static PointDraw barycenter(final PolygonDrawShape polygon) {
		if (polygon == null) {
			return null;
		}

		return barycenter(polygon.getPoints());
	}

	/**
	 *
	 * @param points
	 * @return
	 */
	public static final PointDraw barycenter(final PointDraw... points) {
		if (points == null || points.length == 0) {
			return null;
		}

		int x = 0;
		int y = 0;
		int z = 0;

		for (final PointDraw point : points) {
			x += point.getX();
			y += point.getY();
			z += point.getZ();
		}

		return new PointDraw(
				x / points.length,
				y / points.length,
				z / points.length
				);
	}

	public static VectorDraw add(final PointDraw a, final PointDraw b, final PointDraw c, final PointDraw d) {
		return add(new VectorDraw(a, b), new VectorDraw(c, d));
	}

	public static double scalarProduct(final VectorDraw v1, final VectorDraw v2) {
		return v1.getX() * v2.getX() + v1.getY() * v2.getY() + v1.getZ() * v2.getZ();
	}

	public static double norm(final VectorDraw v) {
		return Math.sqrt(Math.pow(v.getX(), 2.0) + Math.pow(v.getY(), 2.0) + Math.pow(v.getZ(), 2.0));
	}

	public static VectorDraw computeNormalVector(final PointDraw a, final PointDraw b, final PointDraw c) {
		return computeNormalVector(new VectorDraw(a, b), new VectorDraw(a, c));
	}

	public static VectorDraw computeNormalVector(final VectorDraw v1, final VectorDraw v2) {

		final VectorDraw perpendicularVactor = new VectorDraw(
				v1.getY() * v2.getZ() - v2.getY() * v1.getZ(),
				v1.getZ() * v2.getX() - v2.getZ() * v1.getX(),
				v1.getX() * v2.getY() - v2.getX() * v1.getY()
				);

		final double norm = norm(perpendicularVactor);

		return scalar(perpendicularVactor, 1.0 / norm);
	}

	public static VectorDraw computeNormalVector(final PolygonDrawShape polygon) {
		if (polygon.getPoints().length > 2) {
			return computeNormalVector(
					new VectorDraw(polygon.getPoints()[0], polygon.getPoints()[1]),
					new VectorDraw(polygon.getPoints()[0], polygon.getPoints()[2])
					);
		}

		return null;
	}

	public static VectorDraw normalize(final VectorDraw v) {
		final double n = norm(v);
		return new VectorDraw(v.getX() / n, v.getY() / n, v.getZ() / n);
	}

	/**
	 *
	 * @param polygon
	 * @param light
	 * @return
	 */
	public static DrawColor computePointLightColor(final PolygonDrawShape polygon, final PointDraw light) {
		return computeLightColor(polygon, light, (c,p) -> {return p;});
	}

	/**
	 *
	 * @param polygon
	 * @param light
	 * @return
	 */
	public static DrawColor computeSunLightColor(final PolygonDrawShape polygon, final PointDraw light) {
		return computeLightColor(polygon, light, DrawShapes::computeSunPoint);
	}

	/**
	 *
	 * @param polygon
	 * @param light
	 * @param getLightPoint
	 * @return
	 */
	public static DrawColor computeLightColor(final PolygonDrawShape polygon, final PointDraw light, final BiFunction<PointDraw, PointDraw, PointDraw> getLightPoint) {
		final PointDraw m = DrawShapes.barycenter(polygon);
		final VectorDraw om = DrawShapes.normalize(new VectorDraw(m, getLightPoint.apply(m, light)));

		double scalar = DrawShapes.scalarProduct(om, DrawShapes.computeNormalVector(polygon));
		final DrawColor c = polygon.getFillColor();
		if (scalar < 0) {
			scalar = 0.0;
		}
		return new DrawColor((int) (c.getRed() * scalar), (int) (c.getGreen() * scalar), (int) (c.getBlue() * scalar));
	}

	/**
	 *
	 * @param polygonCenter
	 * @param sun
	 * @return
	 */
	private static PointDraw computeSunPoint(final PointDraw polygonCenter, final PointDraw sun) {
		final PointDraw sunPoint = sun.clone();
		sunPoint.setX(polygonCenter.getX());
		sunPoint.setY(polygonCenter.getY());
		sunPoint.setZ(DrawController.SUN_Z);
		return sunPoint;
	}
}
