/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.bean;

/**
 * @author Jérôme LAURENT
 *
 */
public class EnumDrawBean implements DrawBean {

	/**
	 * Enumeration value/
	 */
	private final Enum<?> enumeration;

	/**
	 * Parent.
	 */
	private final DrawBean parent;

	/**
	 * Is it selected ?
	 */
	private boolean isSelected;

	/**
	 *
	 * @param enumeration Enumeration.
	 */
	public EnumDrawBean(final Enum<?> enumeration) {
		super();
		this.enumeration = enumeration;
		this.parent = null;
	}

	/**
	 * Build the object.
	 * @param parent Parent.
	 * @param enumeration Enumeration value.
	 * @param isSelected Is it selected ?
	 */
	public EnumDrawBean(final DrawBean parent, final Enum<?> enumeration, final boolean isSelected) {
		super();
		this.enumeration = enumeration;
		this.isSelected = isSelected;
		this.parent = parent;
	}

	/**
	 * @return the id
	 */
	@Override
	public String getId() {
		return this.enumeration.name();
	}

	/**
	 * @return the parent
	 */
	@Override
	public DrawBean getParent() {
		return this.parent;
	}

	/**
	 * @return the enumeration
	 */
	public Enum<?> getEnumeration() {
		return this.enumeration;
	}

	/**
	 * @return the isSelected
	 */
	public boolean isSelected() {
		return this.isSelected;
	}

	/**
	 * @param isSelected the isSelected to set
	 */
	public void setSelected(final boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * Return a copy.
	 * @return Returns the copy of the object.
	 */
	public EnumDrawBean copy() {
		return new EnumDrawBean(this.parent, this.enumeration, this.isSelected);
	}

	/**
	 * @return Returns the main value of the object.
	 */
	@Override
	public String toString() {
		return "{id=" + this.getId() + ", isSelected=" + this.isSelected + ", parent=" + this.parent + "} " + super.toString();
	}

	/**
	 * Returns the EnumDrawBean[] of the enumeration.
	 * @param parent Bean parent.
	 * @param values Enumeration values.
	 * @return Returns the EnumDrawBean[] of the enumeration.
	 */
	public static final EnumDrawBean[] getEnumDrawBeans(final DrawBean parent, final Enum<?>[] values) {
		final EnumDrawBean[] shapes = new EnumDrawBean[values.length];
		for (int i = 0; i < values.length; i++) {
			shapes[i] = new EnumDrawBean(parent, values[i], i == 0);
		}
		return shapes;
	}

	/**
	 * Set the selected EnumDrawBean in the EnumDrawBean[].
	 * @param values Values.
	 * @param selectedEnum Enumeration value to select.
	 */
	public static final void setSelected(final EnumDrawBean[] values, final Enum<?> selectedEnum) {
		for (int i = 0; i < values.length; i++) {
			if (values[i].getEnumeration() == selectedEnum) {
				values[i].setSelected(true);
			} else {
				values[i].setSelected(false);
			}
		}
	}

	/**
	 * Returns a copy of the EnumDrawBean[]
	 * @param enumBeans EnumDrawBean[] to copy.
	 * @return Returns a copy of the EnumDrawBean[]
	 */
	public static EnumDrawBean[] copy(final EnumDrawBean[] enumBeans) {
		final EnumDrawBean[] copyBeans = new EnumDrawBean[enumBeans.length];
		for (int i = 0; i < enumBeans.length; i++) {
			copyBeans[i] = enumBeans[i].copy();
		}
		return copyBeans;
	}

	/**
	 * Returns the selected EnumDrawBean of the EnumDrawBean[].
	 * @param enumBeans Values.
	 * @return Returns the selected EnumDrawBean of the EnumDrawBean[].
	 */
	public static EnumDrawBean getSelected(final EnumDrawBean[] enumBeans) {
		for (final EnumDrawBean bean : enumBeans) {
			if (bean.isSelected()) {
				return bean;
			}
		}

		return null;
	}

	/**
	 * Equals.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj == null || !(obj instanceof EnumDrawBean)) {
			return false;
		}
		return this.getId().equals(((EnumDrawBean)obj).getId());
	}

	/**
	 * @return Returns the hash code of the object.
	 */
	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
}
