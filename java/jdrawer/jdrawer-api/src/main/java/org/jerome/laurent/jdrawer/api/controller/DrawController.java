/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.controller;


import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.bean.SimpleDrawBean;
import org.jerome.laurent.jdrawer.api.computer.AbstractDrawComputer;
import org.jerome.laurent.jdrawer.api.configuration.DrawConfigurable;
import org.jerome.laurent.jdrawer.api.configuration.DrawConfiguration;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.configuration.LightSource;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerAction;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerEvent;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerEventType;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerListener;
import org.jerome.laurent.jdrawer.api.factory.DrawFactory;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.PolygonDrawShape;
import org.jerome.laurent.jdrawer.api.shape.VectorDraw;

/**
 * @author Jérôme LAURENT
 *
 */
public class DrawController extends SimpleDrawBean implements DrawConfigurable {

	private static final Logger LOGGER = Logger.getLogger(DrawController.class.getName());

	private static final String ZOOM_FACTOR_PROPERTY_ID = "zoom.factor.step";

	private static final String TRANSLATION_STEP_PROPERTY_ID = "translation.step";

	public static final double SUN_Z = 1_000_000.0;

	private Map<String,AbstractDrawComputer> computers;

	private AbstractDrawComputer selectedComputer;

	private boolean isRunning;

	private DrawFactory drawFactory;

	private final BlockingQueue<DrawShape> drawShapeQueue;

	private DrawComputeThread computeThread;

	private double zoomFactor;

	private double zoomStep;

	private int translationStep;

	private VectorDraw translationVector;

	private final List<DrawControllerListener> listeners;

	private final DrawConfiguration configuration;

	private boolean isNegativeView;

	private LightSource lightSource;

	private PointDraw spotLight;

	public DrawController() {
		super();

		this.isRunning = false;
		this.drawShapeQueue = new ArrayBlockingQueue<>(10000);
		this.zoomFactor = 1.0;
		this.zoomStep = 0.1;
		this.translationStep = 10;
		this.listeners = new LinkedList<>();
		this.isNegativeView = false;
		this.lightSource = LightSource.POINT;
		this.spotLight = new PointDraw(0.0, 0.0, 500.0);

		this.configuration = new DrawConfiguration(this);
		this.configuration.addNumberProperty(ZOOM_FACTOR_PROPERTY_ID, this.zoomStep, 0.0, null, 0.1);
		this.configuration.addNumberProperty(TRANSLATION_STEP_PROPERTY_ID, this.translationStep, 0, null, 10);

		new DrawThread(this).start();
	}

	public void start() {
		this.fireListeners(DrawControllerEventType.BEFORE_START);
		if (!this.getSelectedComputer().hasNext()) {
			this.getSelectedComputer().reset();
		}
		this.start(false);
		this.fireListeners(DrawControllerEventType.AFTER_START);
	}

	protected void start(final boolean isLooping) {

		if (!this.isRunning && this.getSelectedComputer() != null) {
			this.isRunning = true;
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info((isLooping ? "Looping" :"Starting") + " draw compute: " + this.getSelectedComputer());
			}
			this.startComputeThread(isLooping);
		}
	}

	public void run() {
		if (this.getDrawFactory() != null) {
			this.getDrawFactory().getDrawViewer().run();
		} else {
			LOGGER.severe("No DrawFactory found. It is impossible to start.");
		}
	}

	public void stop() {
		this.fireListeners(DrawControllerEventType.BEFORE_STOP);
		this.isRunning = false;
		if (LOGGER.isLoggable(Level.INFO)) {
			LOGGER.info("Stopped draw compute: " + this.getSelectedComputer());
		}
		this.fireListeners(DrawControllerEventType.AFTER_STOP);
	}

	/**
	 *
	 */
	public void reset() {
		this.fireListeners(DrawControllerEventType.BEFORE_RESET);
		this.getDrawShapeQueue().clear();
		this.getSelectedComputer().reset();
		this.clear();
		this.zoomFactor = 1.0;
		this.translationVector = null;
		if (LOGGER.isLoggable(Level.INFO)) {
			LOGGER.info("Reseted draw compute: " + this.getSelectedComputer());
		}
		this.fireListeners(DrawControllerEventType.AFTER_RESET);
	}

	public void clear() {
		try {
			this.fireListeners(DrawControllerEventType.BEFORE_CLEAR);
			this.getDrawShapeQueue().put(DrawShapes.CLEAR_DRAW_SHAPE);
			this.fireListeners(DrawControllerEventType.AFTER_CLEAR);
		} catch (final InterruptedException e) {
			LOGGER.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
	}

	/**
	 *
	 */
	public void loop() {
		this.fireListeners(DrawControllerEventType.BEFORE_LOOP);
		this.start(true);
	}

	public void zoomIn() {
		this.fireListeners(DrawControllerEventType.BEFORE_ZOOM_IN);
		this.zoomFactor += this.zoomStep;
		this.getDrawShapeQueue().clear();
		this.fireListeners(DrawControllerEventType.AFTER_ZOOM_IN);
	}

	public void zoomOut() {
		this.fireListeners(DrawControllerEventType.BEFORE_ZOOM_OUT);
		this.zoomFactor -= this.zoomStep;
		this.getDrawShapeQueue().clear();
		this.fireListeners(DrawControllerEventType.AFTER_ZOOM_OUT);
	}

	/**
	 *
	 * @param action
	 */
	public void executeAction(final DrawControllerAction action) {
		switch (action) {
		case CLEAN:
			this.clear();
			break;

		case LOOP:
			this.loop();
			break;

		case RESET:
			this.reset();
			break;

		case START:
			this.start();
			break;

		case STOP:
			this.stop();
			break;

		case NEGATIVE_VIEW:
			this.setNegativeView(!this.isNegativeView());
			break;

		case TRANSLATE_DOWN:
			this.translateDown();
			break;

		case TRANSLATE_LEFT:
			this.translateLeft();
			break;

		case TRANSLATE_RIGHT:
			this.translateRight();
			break;

		case TRANSLATE_UP:
			this.translateUp();
			break;

		case ZOOM_IN:
			this.zoomIn();
			break;

		case ZOOM_OUT:
			this.zoomOut();
			break;

		default:
			break;
		}
	}

	public  void saveDrawing() {
		this.getDrawFactory().saveViewerImage();
	}

	/**
	 * @return the zoomFactor
	 */
	protected double getZoomFactor() {
		return this.zoomFactor;
	}

	/**
	 * @return the translationVector
	 */
	public VectorDraw getTranslationVector() {
		return this.translationVector;
	}

	/**
	 * @param translationVector the translationVector to set
	 */
	public void setTranslationVector(final VectorDraw translationVector) {
		if (this.translationVector == null) {
			this.translationVector = translationVector;
		} else {
			this.translationVector = DrawShapes.add(this.translationVector, translationVector);
		}
		this.getDrawShapeQueue().clear();
	}

	/**
	 * Translate up.
	 */
	public void translateUp() {
		this.setTranslationVector(new VectorDraw(0, this.translationStep));
	}

	/**
	 * Translate down.
	 */
	public void translateDown() {
		this.setTranslationVector(new VectorDraw(0, -this.translationStep));
	}

	/**
	 * Translate right.
	 */
	public void translateRight() {
		this.setTranslationVector(new VectorDraw(this.translationStep, 0));
	}

	/**
	 * Translate left.
	 */
	public void translateLeft() {
		this.setTranslationVector(new VectorDraw(-this.translationStep, 0));
	}

	/**
	 *
	 * @return
	 */
	public Map<String,AbstractDrawComputer> getDrawComputers() {
		if (this.computers == null) {
			this.loadDrawComputers();
		}

		return this.computers;
	}
	/**
	 *
	 * @param drawComputerId
	 */
	public void selectDrawComputer(final String drawComputerId) {
		this.selectedComputer = this.getDrawComputers().get(drawComputerId);
	}

	/**
	 * @return the selectedComputer
	 */
	public AbstractDrawComputer getSelectedComputer() {
		return this.selectedComputer;
	}

	/**
	 * @return the isNegativeView
	 */
	public boolean isNegativeView() {
		return this.isNegativeView;
	}

	/**
	 * @param isNegativeView the isNegativeView to set
	 */
	public void setNegativeView(final boolean isNegativeView) {
		if (this.isNegativeView != isNegativeView) {
			this.isNegativeView = isNegativeView;
			this.getDrawShapeQueue().clear();
		}
	}

	/**
	 * @return the lightSource
	 */
	public LightSource getLightSource() {
		return this.lightSource;
	}

	/**
	 * @param lightSource the lightSource to set
	 */
	public void setLightSource(final LightSource lightSource) {
		this.lightSource = lightSource;
	}

	/**
	 * @return the isRunning
	 */
	public boolean isRunning() {
		return this.isRunning;
	}

	/**
	 *
	 */
	protected void loadDrawComputers() {
		this.computers = new HashMap<>();
		final ServiceLoader<AbstractDrawComputer> computerServices = ServiceLoader.load(AbstractDrawComputer.class);
		for (final AbstractDrawComputer computer : computerServices) {
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info("Loading draw compute: " + computer);
			}
			this.computers.put(computer.getId(), computer);
		}
	}

	/**
	 *
	 * @return
	 */
	public DrawFactory getDrawFactory() {

		if (this.drawFactory != null) {
			return this.drawFactory;
		}

		final ServiceLoader<DrawFactory> drawFactoryServices = ServiceLoader.load(DrawFactory.class);
		for (final DrawFactory drawFactoryService : drawFactoryServices) {
			this.drawFactory = drawFactoryService;
			this.drawFactory.setDrawController(this);
			if (LOGGER.isLoggable(Level.INFO)) {
				LOGGER.info("Loading draw factory: " + this.drawFactory);
			}
			return this.drawFactory;
		}

		return null;
	}

	/**
	 * @return the drawShapeQueue
	 */
	protected BlockingQueue<DrawShape> getDrawShapeQueue() {
		return this.drawShapeQueue;
	}

	protected void startComputeThread(final boolean isLooping) {
		if (this.computeThread == null || !this.computeThread.isAlive()) {
			this.computeThread = new DrawComputeThread(this, isLooping);
			this.computeThread.start();
		}
	}

	/**
	 *
	 * @param listener
	 */
	public void addListener(final DrawControllerListener listener) {
		this.listeners.add(listener);
	}

	/**
	 *
	 * @param listener
	 */
	public void removeListener(final DrawControllerListener listener) {
		this.listeners.remove(listener);
	}

	protected void fireListeners(final DrawControllerEventType eventType) {

		for (final DrawControllerListener listener : this.listeners) {
			try {
				final DrawControllerEvent event = new DrawControllerEvent();
				event.setType(eventType);
				event.setController(this);

				listener.actionPerformed(event);
			} catch (final Exception e) {
				LOGGER.log(Level.SEVERE, "Error of the listener: " + listener + ", for action :" + eventType, e);
			}
		}
	}

	@Override
	public DrawConfiguration getConfiguration() {
		return this.configuration;
	}

	@Override
	public List<DrawError> applyConfiguration() {
		this.zoomStep = this.configuration.getPropertyValue(Double.class, ZOOM_FACTOR_PROPERTY_ID);
		this.translationStep = this.configuration.getPropertyValue(Integer.class, TRANSLATION_STEP_PROPERTY_ID);

		return Collections.emptyList();
	}

	/**
	 * @return the spotLight
	 */
	public PointDraw getSpotLight() {
		return this.spotLight;
	}

	/**
	 * @param spotLight the spotLight to set
	 */
	public void setSpotLight(final PointDraw spotLight) {
		this.spotLight = spotLight;
	}

	/**
	 *
	 * @param polygon
	 * @return
	 */
	public DrawColor computeLight(final PolygonDrawShape polygon) {

		switch (this.getLightSource()) {
		case NONE:
			return polygon.getFillColor();

		case SOLAR:
			return DrawShapes.computeSunLightColor(polygon, this.getSpotLight());

		case POINT:
			return DrawShapes.computePointLightColor(polygon, this.getSpotLight());
		}

		return null;
	}
}
