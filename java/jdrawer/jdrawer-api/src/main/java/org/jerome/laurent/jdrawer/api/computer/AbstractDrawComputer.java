/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.computer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jerome.laurent.jdrawer.api.bean.SimpleDrawBean;
import org.jerome.laurent.jdrawer.api.configuration.DrawConfigurable;
import org.jerome.laurent.jdrawer.api.configuration.DrawConfiguration;
import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;

/**
 * @author Jérôme LAURENT
 *
 */
public abstract class AbstractDrawComputer extends SimpleDrawBean implements Iterable<DrawShape>, Iterator<DrawShape>, DrawConfigurable {

	public static final String TEMPO_PROPERTY_ID = "tempo";

	public static final String COLOR_PROPERTY_ID = "color";

	public final static DrawColor DEFAULT_COLOR = DrawColor.BLACK;

	protected DrawColor color = DEFAULT_COLOR;

	private final DrawConfiguration configuration;

	public AbstractDrawComputer() {
		super();
		this.configuration = new DrawConfiguration(this);
		this.getConfiguration().addColorProperty(COLOR_PROPERTY_ID, this.color);
	}

	/**
	 * Reset computer.
	 */
	public abstract void reset();

	/**
	 *
	 */
	@Override
	public Iterator<DrawShape> iterator() {
		return this;
	}

	@Override
	public String getId() {
		if (super.getId() == null) {
			this.setId(this.getClass().getSimpleName());
		}
		return super.getId();
	}

	/**
	 * @return the configuration
	 */
	@Override
	public DrawConfiguration getConfiguration() {
		return this.configuration;
	}

	/**
	 * Get property value.
	 * @param <T> Value type.
	 * @param type Value type.
	 * @param propertyName Property name.
	 * @return Return the property value.
	 */
	public <T> T getPropertyValue(final Class<T> type, final String propertyName) {
		return this.getConfiguration().getPropertyValue(type, propertyName);
	}

	@Override
	public List<DrawError> applyConfiguration() {
		this.color = this.getPropertyValue(DrawColor.class, COLOR_PROPERTY_ID);
		return new LinkedList<>();
	}
}
