/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Jérôme LAURENT
 *
 */
public class MacroDrawShape extends SimpleDrawShape {

	private List<DrawShape> children;

	private DrawColor fillColor;


	public MacroDrawShape() {
		super();
	}


	public MacroDrawShape(final long tempo, final DrawColor color) {
		super(tempo, color);
	}

	/**
	 * @return the children
	 */
	public List<DrawShape> getChildren() {
		if (this.children == null) {
			this.children = new LinkedList<>();
		}

		return this.children;
	}


	/**
	 * @param children the children to set
	 */
	public void setChildren(final List<DrawShape> children) {
		this.children = children;
		if (this.getColor() != null) {
			this.setColor(this.getColor());
		}
		if (this.getFillColor() != null) {
			this.setFillColor(this.getFillColor());
		}
	}



	@Override
	public MacroDrawShape clone() throws CloneNotSupportedException {
		final MacroDrawShape copy = new MacroDrawShape(this.getTempo(), this.getColor());

		if (this.children != null) {
			for (final DrawShape child : this.children) {
				if (child == DrawShapes.CLEAR_DRAW_SHAPE) {
					copy.getChildren().add(child);
				} else {
					copy.getChildren().add(child.clone());
				}
			}
		}

		return copy;
	}

	@Override
	public void setColor(final DrawColor color) {
		super.setColor(color);

		for (final DrawShape child : this.getChildren()) {
			child.setColor(color);
		}
	}


	public void setFillColor(final DrawColor fillColor) {
		this.fillColor = fillColor;

		for (final DrawShape child : this.getChildren()) {
			if (child instanceof PolygonDrawShape) {
				((PolygonDrawShape)child).setFillColor(this.fillColor);
			}
		}
	}


	/**
	 * @return the fillColor
	 */
	public DrawColor getFillColor() {
		return this.fillColor;
	}
}
