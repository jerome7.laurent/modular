/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

/**
 * @author Jérôme LAURENT
 *
 */
public class DrawColor {

	public static final DrawColor BLACK = new DrawColor(0, 0, 0);

	public static final DrawColor WHITE = new DrawColor(255, 255, 255);

	/**
	 *
	 */
	private int red;

	/**
	 *
	 */
	private int green;

	/**
	 *
	 */
	private int blue;

	public DrawColor(final int red, final int green, final int blue) {
		super();
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public DrawColor() {
		super();
	}

	/**
	 * @return the red
	 */
	public int getRed() {
		return this.red;
	}

	/**
	 * @param red the red to set
	 */
	public void setRed(final int red) {
		this.red = red;
	}

	/**
	 * @return the green
	 */
	public int getGreen() {
		return this.green;
	}

	/**
	 * @param green the green to set
	 */
	public void setGreen(final int green) {
		this.green = green;
	}

	/**
	 * @return the blue
	 */
	public int getBlue() {
		return this.blue;
	}

	/**
	 * @param blue the blue to set
	 */
	public void setBlue(final int blue) {
		this.blue = blue;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof DrawColor)) {
			return false;
		}

		final DrawColor color = (DrawColor) obj;

		return color == this || (this.red == color.getRed() && this.green == color.getGreen() && this.blue == color.getBlue());
	}

	@Override
	public int hashCode() {
		return this.red * 1000_000 + this.green * 1000 + this.blue;
	}
}
