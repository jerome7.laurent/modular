/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerEventType;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;

/**
 * @author Jérôme LAURENT
 *
 */
public class DrawComputeThread extends Thread {

	private static final Logger LOGGER = Logger.getLogger(DrawComputeThread.class.getName());

	private final DrawController controller;

	private final boolean isLooping;

	public DrawComputeThread(final DrawController controller, final boolean isLooping) {
		super(DrawComputeThread.class.getSimpleName());
		this.controller = controller;
		this.isLooping = isLooping;
	}

	@Override
	public void run() {

		do {
			for (final DrawShape shape : this.controller.getSelectedComputer()) {
				if (this.controller.isRunning()) {
					try {
						transform(this.controller, shape);
						this.controller.getDrawShapeQueue().put(shape);
					} catch (final Exception e) {
						LOGGER.log(Level.SEVERE, e.getLocalizedMessage(), e);
					}
				} else {
					break;
				}
			}

			if (this.isLooping && this.controller.isRunning()) {
				this.controller.getSelectedComputer().reset();
				this.controller.clear();
			}
		} while (this.isLooping && this.controller.isRunning());

		this.controller.fireListeners(DrawControllerEventType.AFTER_LOOP);
		this.controller.stop();
	}

	/**
	 *
	 * @param controller
	 * @param shape
	 */
	private static final void transform(final DrawController controller, final DrawShape shape) {
		DrawShapes.zoom(controller.getZoomFactor(), shape);
		DrawShapes.translate(controller.getTranslationVector(), shape);
		if (controller.isNegativeView()) {
			DrawShapes.negativeColor(shape);
		}
	}
}
