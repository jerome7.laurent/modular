/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

/**
 * @author Jérôme LAURENT
 *
 */
public class PolygonDrawShape extends SimpleDrawShape implements FillDrawShape {

	private DrawColor fillColor;

	private PointDraw[] points;

	/**
	 *
	 */
	public PolygonDrawShape() {
		super();
	}

	/**
	 *
	 * @param points
	 */
	public PolygonDrawShape(final PointDraw[] points) {
		this.points = points;
	}

	/**
	 *
	 * @param points
	 * @param fillColor
	 */
	public PolygonDrawShape(final PointDraw[] points, final DrawColor fillColor) {
		this.points = points;
		this.fillColor = fillColor;
	}

	public PolygonDrawShape(final PointDraw[] points, final DrawColor fillColor, final DrawColor stokeColor) {
		this(points, fillColor);
		this.setColor(stokeColor);
	}

	/**
	 * @return the fillColor
	 */
	@Override
	public DrawColor getFillColor() {
		return this.fillColor;
	}

	/**
	 * @param fillColor the fillColor to set
	 */
	@Override
	public void setFillColor(final DrawColor fillColor) {
		this.fillColor = fillColor;
	}

	/**
	 * @return the points
	 */
	public PointDraw[] getPoints() {
		return this.points;
	}

	/**
	 * @param points the points to set
	 */
	public void setPoints(final PointDraw[] points) {
		this.points = points;
	}

	@Override
	public PolygonDrawShape clone() throws CloneNotSupportedException {

		final PolygonDrawShape p = (PolygonDrawShape) super.clone();

		final PointDraw[] clonedPoints = new PointDraw[this.points.length];
		for (int i = 0; i < this.points.length; i++) {
			clonedPoints[i] = this.points[i].clone();
		}
		p.setPoints(clonedPoints);
		return p;
	}
}
