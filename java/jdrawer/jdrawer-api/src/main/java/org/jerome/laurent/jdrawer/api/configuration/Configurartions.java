/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.configuration;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Jérôme LAURENT
 *
 */
public final class Configurartions {

	/**
	 *
	 */
	private Configurartions() {
		super();
	}

	/**
	 * Apply configuration.
	 * @param configurable Configurable.
	 * @param componentValues Component values.
	 */
	@SuppressWarnings("unchecked")
	public static void applyConfiguration(final DrawConfigurable configurable, final Map<String, PropertyComponent<?>> componentValues) {

		final List<DrawError> errors = new LinkedList<>();

		for (@SuppressWarnings("rawtypes") final DrawProperty property : configurable.getConfiguration().getProperties()) {
			try {
				property.setValue(componentValues.get(property.getId()).getValue());
			} catch (final NotAllowedPropertyValueException e) {
				errors.add(e.getError());
			}
		}

		errors.addAll(configurable.applyConfiguration());
		if (errors.isEmpty()) {
			for (final PropertyComponent<?> propertyComponent : componentValues.values()) {
				propertyComponent.setError(null);
			}
		} else {
			for (final DrawError error : errors) {
				final PropertyComponent<?> propertyComponent = componentValues.get(error.getSubject().getId());
				propertyComponent.setError(error);
			}
		}
	}

	/**
	 * Reset configuration.
	 * @param configurable Configurable.
	 * @param componentValues Component values.
	 */
	public static void resetConfiguration(final DrawConfigurable configurable, final Map<String, PropertyComponent<?>> componentValues) {
		for (final DrawProperty<?> property : configurable.getConfiguration().getProperties()) {
			final PropertyComponent<?> propertyComponent = componentValues.get(property.getId());
			setPropertyComponentValue(propertyComponent, property);
		}
	}

	/**
	 * Set property component value.
	 * @param <T> Property component value type.
	 * @param propertyComponent Property component.
	 * @param property Property.
	 */
	@SuppressWarnings("unchecked")
	public static <T> void setPropertyComponentValue(final PropertyComponent<T> propertyComponent,  final DrawProperty<?> property) {
		propertyComponent.setValue((T) property.getDefaultValue());
	}
}
