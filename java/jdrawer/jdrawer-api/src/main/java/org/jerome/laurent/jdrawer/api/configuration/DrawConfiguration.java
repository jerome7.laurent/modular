/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.configuration;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jerome.laurent.jdrawer.api.bean.DrawBean;
import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;

/**
 * @author Jérôme LAURENT
 *
 */
public class DrawConfiguration {

	private List<DrawProperty<?>> properties;

	private Map<String, DrawProperty<?>> propertyIndex;

	private final DrawBean parent;

	public DrawConfiguration(final DrawBean parent) {
		super();
		this.parent = parent;
	}

	/**
	 * @return the properties
	 */
	public List<DrawProperty<?>> getProperties() {
		if (this.properties == null) {
			this.properties = new LinkedList<>();
			this.propertyIndex = new HashMap<>();
		}
		return this.properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(final List<DrawProperty<?>> properties) {
		this.properties = properties;
		if (properties != null) {
			if (this.propertyIndex == null) {
				this.propertyIndex = new HashMap<>();
			} else {
				this.propertyIndex.clear();
			}
			for (final DrawProperty<?> proterty : properties) {
				this.propertyIndex.put(proterty.getId(), proterty);
			}
		}
	}

	/**
	 * Add number property.
	 * @param id ID.
	 * @param defaultValue Default value.
	 * @param minimum Minimum possible value..
	 * @param maximum maximum possible value.
	 * @param step Step to increase or decrease the value.
	 */
	public <T extends Number> void addNumberProperty(final String id, final T defaultValue, final T minimum, final T maximum, final T step) {
		final NumberDrawProperty<T> property = new NumberDrawProperty<>(this.parent, id, defaultValue, minimum, maximum, step);
		this.addProperty(property);
	}

	/**
	 * Add boolean value.
	 * @param id ID.
	 * @param defaultValue Default value.
	 */
	public void addBooleanProperty(final String id, final boolean defaultValue) {
		final BooleanDrawProperty property = new BooleanDrawProperty(this.parent, id, defaultValue);
		this.addProperty(property);
	}

	/**
	 * Add enumeration value.
	 * @param id ID.
	 * @param defaultValue Default value.
	 */
	public void addEnumProperty(final String id, final EnumDrawBean[] defaultValue) {
		final EnumDrawProperty property = new EnumDrawProperty(this.parent, id, defaultValue);
		this.addProperty(property);
	}

	/**
	 * Add draw value.
	 * @param id ID.
	 * @param defaultValue Default value.
	 */
	public void addColorProperty(final String id, final DrawColor defaultValue) {
		final ColorDrawProperty property = new ColorDrawProperty(this.parent, id, defaultValue);
		this.addProperty(property);
	}

	/**
	 * Ad property.
	 *
	 * @param <T> Property type.
	 * @param property Property.
	 */
	protected <T> void addProperty(final DrawProperty<T> property) {
		this.getProperties().add(property);
		this.propertyIndex.put(property.getId(), property);
	}

	/**
	 * Get property.
	 * @param propertyId Property ID.
	 * @return Return the property with ID propertyId.
	 */
	public DrawProperty<?> getProperty(final String propertyId) {
		return this.propertyIndex.get(propertyId);
	}

	/**
	 * Set a property.
	 * @param <T> Type of the property.
	 * @param propertyId ID of the property.
	 * @param value Value of the property.
	 * @throws NotAllowedPropertyValueException
	 */
	public <T> void setPropertyValue(final String propertyId, final T value) throws NotAllowedPropertyValueException {

		@SuppressWarnings("unchecked")
		final DrawProperty<T> property = (DrawProperty<T>) this.propertyIndex.get(propertyId);
		if (property == null) {
			return;
		}

		property.setValue(value);
	}

	/**
	 * Get property value.
	 * @param <T> Property type.
	 * @param propertyClass Property class.
	 * @param propertyId Property ID.
	 * @return Return property value.
	 */
	public <T> T getPropertyValue(final Class<T> propertyClass, final String propertyId) {

		final DrawProperty<T> property = (DrawProperty<T>) this.propertyIndex.get(propertyId);

		if (property == null) {
			return null;
		}

		if (property.getValue() != null) {
			return property.getValue();
		}

		return property.getDefaultValue();
	}
}
