/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.i18n;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.bean.DrawBean;

/**
 * @author Jérôme LAURENT
 *
 */
public final class I18ns {

	private static final Logger LOGGER = Logger.getLogger(I18ns.class.getName());

	/**
	 * Languages
	 */
	private static List<Locale> SUPPORTED_LOCALES = Collections.unmodifiableList(
			Arrays.asList(Locale.FRANCE, Locale.ENGLISH)
			);

	/**
	 *
	 */
	private I18ns() {
		super();
	}

	/**
	 *
	 * @param locale
	 * @param bean
	 * @return
	 */
	public static String getLabel(final Locale locale, final DrawBean bean, final Object... args) {
		return getString(locale, bean, "label", args);
	}

	/**
	 *
	 * @param locale
	 * @param bean
	 * @return
	 */
	public static String getDescription(final Locale locale, final DrawBean bean, final Object... args) {
		return getString(locale, bean, "description", args);
	}

	/**
	 *
	 * @param locale
	 * @param bean
	 * @param field
	 * @param args
	 * @return
	 */
	public static String getString(final Locale locale, final DrawBean bean, final String field, final Object... args) {
		if (field == null || bean == null) {
			return null;
		}

		String baseName;
		if (bean.getParent() == null) {
			baseName = bean.getClass().getName();
		} else {
			baseName = bean.getParent().getClass().getName();
		}
		return getString(locale, baseName, bean.getId() + "." + field, args);
	}

	/**
	 *
	 * @param locale
	 * @param baseName
	 * @param field
	 * @param args
	 * @return
	 */
	public static String getString(final Locale locale, final String baseName, final String field, final Object... args) {
		try {

			final ResourceBundle rb = ResourceBundle.getBundle(baseName, locale);

			final String rawString = rb.getString(field);
			if (args != null && args.length > 0) {
				return new MessageFormat(rawString, locale).format(args);
			}
			return rawString;
		} catch (final Exception e) {
			LOGGER.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return field;
		}
	}

	/**
	 *
	 * @return
	 */
	public static List<Locale> getSupportedLocales() {
		return SUPPORTED_LOCALES;
	}
}
