/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

import java.util.Objects;

/**
 * @author Jérôme LAURENT
 *
 */
public class PointDraw implements Cloneable {

	/**
	 *
	 */
	double x;

	/**
	 *
	 */
	double y;

	/**
	 *
	 */
	double z;

	/**
	 * Create point (0.0, 0.0, 0.0).
	 */
	public PointDraw() {
		this(0.0, 0.0);
	}

	/**
	 * Create point (x, y, 0).
	 * @param x
	 * @param y
	 */
	public PointDraw(final double x, final double y) {
		this(x, y, 0.0);
	}

	/**
	 *
	 * @param x
	 * @param y
	 * @param z
	 */
	public PointDraw(final double x, final double y, final double z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return this.x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(final double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return this.y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(final double y) {
		this.y = y;
	}

	/**
	 * @return the z
	 */
	public double getZ() {
		return this.z;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(final double z) {
		this.z = z;
	}

	@Override
	public PointDraw clone() {
		try {
			return (PointDraw) super.clone();
		} catch (final CloneNotSupportedException e) {
			return new PointDraw(this.getX(), this.getY(), this.getZ());
		}
	}

	@Override
	public String toString() {
		return "{x=" + this.x + ", y=" + this.y + ", z=" + this.z + "} " + super.toString();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PointDraw)) {
			return false;
		}

		final PointDraw point = (PointDraw) obj;
		return this.getX() == point.getX() && this.getY() == point.getY() && this.getZ() == point.getZ();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getX(), this.getY(), this.getZ());
	}
}
