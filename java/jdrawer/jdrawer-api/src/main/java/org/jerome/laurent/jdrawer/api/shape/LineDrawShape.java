/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

/**
 * @author Jérôme LAURENT
 *
 */
public class LineDrawShape extends PointDrawShape {

	/**
	 * Point 2.
	 */
	private PointDraw point2;

	public LineDrawShape() {
		super();
	}

	/**
	 *
	 * @param point1
	 * @param point2
	 */
	public LineDrawShape(final PointDraw point1, final PointDraw point2) {
		super(point1);
		this.point2 = point2;
	}

	/**
	 *
	 * @param point1
	 * @param point2
	 * @param color
	 */
	public LineDrawShape(final PointDraw point1, final PointDraw point2, final DrawColor color) {
		this(point1, point2);
		this.setColor(color);
	}


	/**
	 * @return the point2
	 */
	public PointDraw getPoint2() {
		return this.point2;
	}

	/**
	 * @param point2 the point2 to set
	 */
	public void setPoint2(final PointDraw point2) {
		this.point2 = point2;
	}

	@Override
	public LineDrawShape clone() {
		LineDrawShape l;
		try {
			l = (LineDrawShape) super.clone();
			l.setPoint2(new PointDraw(this.point2.getX(), this.point2.getY(), this.point2.getZ()));
			return l;
		} catch (final CloneNotSupportedException e) {
			return new LineDrawShape(this.getPoint1().clone(), this.getPoint2().clone());
		}

	}

	@Override
	public String toString() {
		return "{point1=" + this.getPoint1() + ", point2=" + this.getPoint2() + "} " + super.toString();
	}
}
