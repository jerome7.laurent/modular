/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.api.shape;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * @author Jérôme LAURENT
 *
 */
class DrawShapesTest {

	/**
	 * Test method for {@link org.jerome.laurent.jdrawer.api.shape.DrawShapes#computeNormalVector(org.jerome.laurent.jdrawer.api.shape.VectorDraw, org.jerome.laurent.jdrawer.api.shape.VectorDraw)}.
	 */
	@Test
	void testComputeNormalVectorVectorDrawVectorDraw() {
		final VectorDraw n = DrawShapes.computeNormalVector(new VectorDraw(1.0, 0.0, 0.0), new VectorDraw(0.0, 1.0, 0.0));
		assertEquals(0.0, n.getX(), "X");
		assertEquals(0.0, n.getY(), "Y");
		assertEquals(1.0, n.getZ(), "Z");
	}

	@Test
	void testComputeLightColor() {
		final PointDraw spotLight1 = new PointDraw(0, 0, 200);
		final PointDraw spotLight2 = new PointDraw(0, 0, -200);
		final PolygonDrawShape square = new PolygonDrawShape(new PointDraw[] {
				new PointDraw(-200, -200, 0),
				new PointDraw(200, -200, 0),
				new PointDraw(200, 200, 0),
				new PointDraw(-200, 200, 0)
		}
		, DrawColor.WHITE);

		assertEquals(DrawColor.WHITE, DrawShapes.computePointLightColor(square, spotLight1), "square in front of");
		assertEquals(DrawColor.BLACK, DrawShapes.computePointLightColor(square, spotLight2), "square behind");

		final PolygonDrawShape triangle = new PolygonDrawShape(new PointDraw[] {
				new PointDraw(-300, -300, 0),
				new PointDraw(300, -300, 0),
				new PointDraw(0, 600, 0)
		}
		, DrawColor.WHITE);

		assertEquals(DrawColor.WHITE, DrawShapes.computePointLightColor(triangle, spotLight1), "triangle in front of");
		assertEquals(DrawColor.BLACK, DrawShapes.computePointLightColor(triangle, spotLight2), "triangle behind");

		final PolygonDrawShape equilateralTriangle =  new PolygonDrawShape(DrawShapes.createEquilateralTrianglePoints(300), DrawColor.WHITE);
		assertEquals(DrawColor.WHITE, DrawShapes.computePointLightColor(equilateralTriangle, spotLight1), "equilateral triangle in front of");
		assertEquals(DrawColor.BLACK, DrawShapes.computePointLightColor(equilateralTriangle, spotLight2), "equilateral triangle behind");
	}
}
