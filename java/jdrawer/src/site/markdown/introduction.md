La génèse
=======
Cette matière primitive, informe et grossière est née suite à la visualisation de la vidéo de "Mickaël Launay" intitulée "[La face cachée des tables de multiplication - Micmaths](https://www.youtube.com/watch?v=-X49VQgi86E)" puis de la vidéo "[Les fractales - Micmaths]"(https://www.youtube.com/watch?v=iFA3g_4myFw). Cette matière a été façonnée au clavier bit après bit, octet après octet en égrainant les secondes, les heures, les jours. Puisse-t-elle être utile à tous les petits êtres de lumière en devenir.

Oui mais pourquoi infliger cela au monde, il ne t'a rien fait ?
=======
Les mathématiques ne sont pas une science de torture pour punir les femmes et les hommes. Cet outil est là pour essayer de montrer la beauté des mathématiques en mettant en exergue leur pureté et que leur simplicité fait naître la complexité et l'émerveillement. Pour les incrédules, l'image d'une fratale dans votre oeil vous fera vaciller. Vive le chou romanesco!!!