Diagramme des classes
=======

![Image](images/class_diagram.png "Diagramme des classes")

Diagramme de séquences
=======
![Image](images/sequence_diagram_controller.png "Diagramme de séquences")

Icônes
=======
[Material icons](https://github.com/google/material-design-icons)