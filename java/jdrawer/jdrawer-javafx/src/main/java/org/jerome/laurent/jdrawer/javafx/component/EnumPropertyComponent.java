/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx.component;

import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.configuration.DrawProperty;
import org.jerome.laurent.jdrawer.javafx.JavaFXDrawFactory;

import javafx.scene.control.ChoiceBox;

/**
 * @author Jérôme LAURENT
 *
 */
public class EnumPropertyComponent extends AbstractPropertyComponent<EnumDrawBean[], ChoiceBox<EnumDrawBean>> {

	public EnumPropertyComponent(final DrawProperty<EnumDrawBean[]> property) {
		super(property, JavaFXDrawFactory.createChoiceBox(property));
		this.getControl().valueProperty().addListener((observable, oldValue, newValue) -> {
			this.setStyle();
		});
	}

	@Override
	public void setControlValue(final EnumDrawBean[] value) {
		final EnumDrawBean selectedBean = EnumDrawBean.getSelected(value);
		this.getControl().getSelectionModel().select(selectedBean);
	}

	@Override
	public EnumDrawBean[] getValue() {
		final EnumDrawBean[] beans = this.getControl().getItems().toArray(new EnumDrawBean[this.getControl().getItems().size()]);
		EnumDrawBean.setSelected(beans, this.getControl().getSelectionModel().getSelectedItem().getEnumeration());
		return beans;
	}

}
