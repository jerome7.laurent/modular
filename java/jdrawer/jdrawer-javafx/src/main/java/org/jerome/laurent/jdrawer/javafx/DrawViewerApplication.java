/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.jerome.laurent.jdrawer.api.computer.AbstractDrawComputer;
import org.jerome.laurent.jdrawer.api.configuration.LightSource;
import org.jerome.laurent.jdrawer.api.controller.DrawController;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerAction;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerEvent;
import org.jerome.laurent.jdrawer.api.controller.listener.DrawControllerListener;
import org.jerome.laurent.jdrawer.api.extra.EasterEgg;
import org.jerome.laurent.jdrawer.api.i18n.I18ns;
import org.jerome.laurent.jdrawer.api.viewer.DrawViewer;
import org.jerome.laurent.jdrawer.javafx.component.ConfigurationPane;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author Jérôme LAURENT
 *
 */
public class DrawViewerApplication extends Application implements DrawViewer, DrawControllerListener {

	private static DrawController controller;

	private ConfigurationPane controllerConfigurationPane;

	private ConfigurationPane computerConfiguratioPane;

	private final Button[] toolBarButtons;

	private HBox splitPane;

	private Scene primaryScene;

	private boolean isConfigurationPaneVisible;

	private final EasterEgg easterEgg;

	private final StringProperty titleProperty;

	/**
	 *
	 *  Build a DrawViewerApplication.
	 */
	public DrawViewerApplication() {
		super();
		this.toolBarButtons = new Button[DrawControllerAction.values().length];
		this.isConfigurationPaneVisible = true;
		this.easterEgg = new EasterEgg();
		this.titleProperty = new SimpleStringProperty("JDrawer - Pour Juliette");
	}

	/**
	 * @return the controller
	 */
	protected static DrawController getController() {
		return controller;
	}

	/**
	 * @param drawController the controller to set
	 */
	protected static void setController(final DrawController drawController) {
		controller = drawController;
	}

	protected MenuBar createMenuBar() {

		//
		// File menu
		//
		// configuration
		this.controllerConfigurationPane = new ConfigurationPane();
		this.controllerConfigurationPane.setConfigurable(getController());

		final Stage configurationDialog = JavaFXDrawFactory.createStage("configuration.dialog");
		configurationDialog.setScene(new Scene(this.controllerConfigurationPane));
		this.controllerConfigurationPane.addApplyEventHandler(e -> configurationDialog.close());

		final MenuItem configurationItem = JavaFXDrawFactory.createMenuItem("menu.bar.file.configuration.item", e -> {
			this.controllerConfigurationPane.setConfigurable(getController());
			configurationDialog.show();
		});

		final MenuItem saveDrawnImageItem = JavaFXDrawFactory.createMenuItem("menu.bar.file.save.drawn.image.item", e -> {
			controller.saveDrawing();
		});

		// quit
		final MenuItem exitItem = JavaFXDrawFactory.createMenuItem("menu.bar.file.exit.item", e -> System.exit(0));

		final Menu fileMenu = JavaFXDrawFactory.createMenu("menu.bar.file", configurationItem, saveDrawnImageItem, JavaFXDrawFactory.createSeparatorMenuItem(), exitItem);

		// View menu
		final CheckMenuItem configurationViewItem = JavaFXDrawFactory.createCheckMenuItem("menu.bar.view.configuration.item", true, e -> this.toogleConfigurationPane());
		final Menu viewMenu = JavaFXDrawFactory.createMenu("menu.bar.view", configurationViewItem);

		// Light menu
		final List<LightSource> lightSources = Arrays.asList(LightSource.values());

		final ToggleGroup lightGroup = new ToggleGroup();
		final RadioMenuItem[] lightRadioMenuItems = new RadioMenuItem[lightSources.size()];
		for (int i = 0; i < lightSources.size(); i++) {
			final LightSource currentLightSource = lightSources.get(i);
			lightRadioMenuItems[i] = JavaFXDrawFactory.createRadioMenuItem("menu.bar.light." + currentLightSource.name()+ ".item", lightGroup, e -> getController().setLightSource(currentLightSource)); // TODO set handler
			lightRadioMenuItems[i].setSelected(getController().getLightSource() == currentLightSource);
		}
		final Menu lightMenu = JavaFXDrawFactory.createMenu("menu.bar.light", lightRadioMenuItems);

		// Language menu
		final List<Locale> supportedLanguages = I18ns.getSupportedLocales();

		final ToggleGroup languageGroup = new ToggleGroup();
		final RadioMenuItem[] languageRadioMenuItems = new RadioMenuItem[supportedLanguages.size()];
		for (int i = 0; i < supportedLanguages.size(); i++) {
			final Locale currentLanguage = supportedLanguages.get(i);
			languageRadioMenuItems[i] = JavaFXDrawFactory.createRadioMenuItem("menu.bar.language." + currentLanguage.getISO3Language() + ".item", languageGroup, e -> JavaFXDrawFactory.setLocale(currentLanguage));
			languageRadioMenuItems[i].setSelected(JavaFXDrawFactory.getLocale().equals(currentLanguage));
		}

		final Menu languageMenu = JavaFXDrawFactory.createMenu("menu.bar.language", languageRadioMenuItems);

		// Menu bar
		final MenuBar menuBar = JavaFXDrawFactory.createMenuBar(fileMenu, viewMenu, lightMenu, languageMenu);

		return menuBar;
	}

	protected void toogleConfigurationPane() {
		if (this.splitPane != null) {
			if (this.isConfigurationPaneVisible) {
				this.splitPane.getChildren().remove(this.computerConfiguratioPane);
				this.isConfigurationPaneVisible = false;
			} else {
				this.splitPane.getChildren().add(0, this.computerConfiguratioPane);
				this.isConfigurationPaneVisible = true;
			}
		}
	}

	protected ToolBar createToolBar() {

		// draw computer selection
		@SuppressWarnings("unchecked")
		final ComboBox<AbstractDrawComputer> computerComboBox = JavaFXDrawFactory.createComboBox(getController().getDrawComputers().values(),
				e -> this.selectDrawComputer(((ComboBox<AbstractDrawComputer>)e.getSource()).getSelectionModel().getSelectedItem()));
		this.selectDrawComputer(computerComboBox.getSelectionModel().getSelectedItem());

		// action buttons
		for (final DrawControllerAction action : DrawControllerAction.values()) {
			this.toolBarButtons[action.ordinal()] = JavaFXDrawFactory.createButton("tool.bar." + action.name().toLowerCase().replace('_', '.') +  ".button", false, e -> controller.executeAction(action));
		}
		this.toolBarButtons[DrawControllerAction.NEGATIVE_VIEW.ordinal()].setOnAction(e -> {
			controller.executeAction(DrawControllerAction.NEGATIVE_VIEW);
			DrawerCanvas.getInstance().negativeView();
		});
		this.computeToolBarButtonStates();
		controller.addListener(this);

		final ToolBar toolBar = new ToolBar();
		toolBar.getItems().addAll(computerComboBox);
		toolBar.getItems().addAll(this.toolBarButtons);

		return toolBar;
	}

	protected void selectDrawComputer(final AbstractDrawComputer computer) {
		controller.selectDrawComputer(computer.getId());
		this.computerConfiguratioPane.setConfigurable(computer);
	}


	@Override
	public void run() {
		launch(new String[] {});
	}

	@Override
	public void start(final Stage primaryStage) throws Exception {

		this.computerConfiguratioPane = new ConfigurationPane();

		final HBox drawerHBox = new HBox(DrawerCanvas.getInstance());
		VBox.setVgrow(drawerHBox, Priority.ALWAYS);
		HBox.setHgrow(drawerHBox, Priority.ALWAYS);

		this.computerConfiguratioPane.setMinWidth(350.0);

		this.splitPane = new HBox();
		this.splitPane.getChildren().addAll(this.computerConfiguratioPane, JavaFXDrawFactory.createSeparator(Orientation.VERTICAL), drawerHBox);

		VBox.setVgrow(this.splitPane, Priority.ALWAYS);

		final VBox rootNode = new VBox();
		final MenuBar menuBar = this.createMenuBar();
		final ToolBar toolBar = this.createToolBar();
		rootNode.getChildren().addAll(menuBar, toolBar, this.splitPane);

		this.primaryScene = new Scene(rootNode);
		DrawerCanvas.getInstance().heightProperty().bind(Bindings.createDoubleBinding(() -> this.primaryScene.getHeight() - menuBar.getHeight() - toolBar.getHeight(), this.primaryScene.heightProperty()));
		DrawerCanvas.getInstance().widthProperty().bind(Bindings.createDoubleBinding(() -> this.computeDrawCanvasWidth(), this.primaryScene.widthProperty()));

		primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, e -> this.onKeyPressed(e));
		primaryStage.titleProperty().bind(this.titleProperty);
		primaryStage.setWidth(1280);
		primaryStage.setHeight(800);
		primaryStage.setScene(this.primaryScene);
		primaryStage.getIcons().add(JavaFXDrawFactory.getImage("application.icon"));
		primaryStage.show();
	}

	private void onKeyPressed(final KeyEvent e) {
		final String easterEggString = this.easterEgg.getEasterEgg((char)e.getCode().getCode());
		if (easterEggString != null) {
			this.titleProperty.set(this.titleProperty.get() + easterEggString);
		}
	}

	/**
	 *
	 * @return
	 */
	private double computeDrawCanvasWidth() {
		if (this.primaryScene != null) {
			return this.primaryScene.getWidth() - (this.isConfigurationPaneVisible ? this.computerConfiguratioPane.getWidth() : 0.0);
		}
		return 0.0;
	}

	@Override
	public void actionPerformed(final DrawControllerEvent event) {
		this.computeToolBarButtonStates();
	}

	protected void computeToolBarButtonStates() {
		this.toolBarButtons[DrawControllerAction.START.ordinal()].setDisable(controller.isRunning());
		this.toolBarButtons[DrawControllerAction.STOP.ordinal()].setDisable(!controller.isRunning());
		this.toolBarButtons[DrawControllerAction.LOOP.ordinal()].setDisable(controller.isRunning());
		this.toolBarButtons[DrawControllerAction.CLEAN.ordinal()].setDisable(controller.isRunning());
	}
}
