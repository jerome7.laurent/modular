/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.jerome.laurent.jdrawer.api.bean.DrawBean;
import org.jerome.laurent.jdrawer.api.bean.EnumDrawBean;
import org.jerome.laurent.jdrawer.api.configuration.BooleanDrawProperty;
import org.jerome.laurent.jdrawer.api.configuration.ColorDrawProperty;
import org.jerome.laurent.jdrawer.api.configuration.DrawProperty;
import org.jerome.laurent.jdrawer.api.configuration.NumberDrawProperty;
import org.jerome.laurent.jdrawer.api.controller.DrawController;
import org.jerome.laurent.jdrawer.api.factory.DrawFactory;
import org.jerome.laurent.jdrawer.api.i18n.I18ns;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.viewer.Drawer;
import org.jerome.laurent.jdrawer.javafx.component.AbstractPropertyComponent;
import org.jerome.laurent.jdrawer.javafx.component.BooleanPropertyComponent;
import org.jerome.laurent.jdrawer.javafx.component.ColorPropertyComponent;
import org.jerome.laurent.jdrawer.javafx.component.EnumPropertyComponent;
import org.jerome.laurent.jdrawer.javafx.component.NumberPropertyComponent;
import org.jerome.laurent.jdrawer.javafx.component.TextFieldPropertyComponent;
import org.jerome.laurent.jdrawer.javafx.component.spinner.LongSpinnerValueFactory;
import org.jerome.laurent.jdrawer.javafx.converter.DrawBeanStringConverter;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author Jérôme LAURENT
 *
 */
public class JavaFXDrawFactory implements DrawFactory {

	private static final Logger LOGGER = Logger.getLogger(JavaFXDrawFactory.class.getName());

	private static final ObjectProperty<Locale> locale;

	static {
		locale = new SimpleObjectProperty<>(getDefaultLocale());
		locale.addListener((observable, oldValue, newValue) -> Locale.setDefault(newValue));
	}

	private static DrawViewerApplication viewer;

	private static DrawController controller;

	@SuppressWarnings("static-access")
	@Override
	public void setDrawController(final DrawController controller) {
		this.controller = controller;
	}

	@SuppressWarnings("static-access")
	@Override
	public DrawViewerApplication getDrawViewer() {
		if (this.viewer == null) {

			this.viewer = new DrawViewerApplication();
			this.viewer.setController(this.controller);
		}

		return this.viewer;
	}

	@Override
	public Drawer getDrawer() {
		return DrawerCanvas.getInstance();
	}

	/**
	 * @return the controller
	 */
	public static DrawController getController() {
		return controller;
	}

	/**
	 *
	 * @return
	 */
	public static DrawViewerApplication getViewer() {
		return viewer;
	}

	/**
	 * Get the default locale. This is the systems default if contained in the supported locales, FRANCE otherwise.
	 *
	 * @return
	 */
	public static Locale getDefaultLocale() {
		final Locale sysDefault = Locale.getDefault();
		return I18ns.getSupportedLocales().contains(sysDefault) ? sysDefault : Locale.FRANCE;
	}

	/**
	 * Get current locale.
	 *
	 * @return
	 */
	public static Locale getLocale() {
		return locale.get();
	}

	/**
	 * Set locale.
	 * @param newLocale
	 */
	public static void setLocale(final Locale newLocale) {
		if (locale.get().equals(newLocale)) {
			return;
		}
		locale.set(newLocale);
		Locale.setDefault(newLocale);
	}

	/**
	 * Creates a String binding to a localized String for the given message bundle key.
	 * @param id ID
	 * @param args Arguments.
	 * @return Returns a string binding object.
	 */
	public static StringBinding createStringLocaleBinding(final String id, final Object... args) {
		return Bindings.createStringBinding(() -> getI18nString(id, args), locale);
	}

	/**
	 *
	 * @param drawBean
	 * @param args
	 * @return
	 */
	public static StringBinding createLabelBinding(final DrawBean drawBean, final Object... args) {
		return Bindings.createStringBinding(() -> I18ns.getLabel(getLocale(), drawBean, args), locale);
	}

	/**
	 *
	 * @param drawBean
	 * @param args
	 * @return
	 */
	public static StringBinding createDescriptionBinding(final DrawBean drawBean, final Object... args) {
		return Bindings.createStringBinding(() -> I18ns.getDescription(getLocale(), drawBean, args), locale);
	}

	/**
	 *
	 * @param id
	 * @param args
	 * @return
	 */
	private static String getI18nString(final String id, final Object... args) {
		return I18ns.getString(getLocale(), JavaFXDrawFactory.class.getName(), id, args);
	}

	/**
	 * Create a menu bar.
	 * @param menus The menu of the menu bar.
	 * @return Return a new menu bar.
	 */
	public static MenuBar createMenuBar(final Menu... menus) {
		final MenuBar menuBar = new MenuBar(menus);
		return menuBar;
	}

	/**
	 *
	 * @param menuId
	 * @param items
	 * @return
	 */
	public static Menu createMenu(final String menuId, final MenuItem... items) {
		final Menu menu = new Menu();
		menu.setId(menuId);
		menu.textProperty().bind(createStringLocaleBinding(menuId));
		menu.getItems().addAll(items);
		return menu;
	}


	/**
	 *
	 * @param menuItemId
	 * @return
	 */
	public static MenuItem createMenuItem(final String menuItemId, final EventHandler<ActionEvent> eventHandler) {
		final MenuItem menuItem = new MenuItem();
		setItem(menuItem, menuItemId, eventHandler);
		return menuItem;
	}

	/**
	 *
	 * @param menuItemId
	 * @return
	 */
	public static RadioMenuItem createRadioMenuItem(final String menuItemId, final ToggleGroup group, final EventHandler<ActionEvent> eventHandler) {
		final RadioMenuItem menuItem = new RadioMenuItem();
		setItem(menuItem, menuItemId, eventHandler);
		if (group != null) {
			menuItem.setToggleGroup(group);
		}
		return menuItem;
	}

	/**
	 *
	 * @param menuItemId
	 * @param isSelected
	 * @param eventHandler
	 * @return
	 */
	public static CheckMenuItem createCheckMenuItem(final String menuItemId, final boolean isSelected, final EventHandler<ActionEvent> eventHandler) {
		final CheckMenuItem menuItem = new CheckMenuItem();
		setItem(menuItem, menuItemId, eventHandler);
		menuItem
		.setSelected(isSelected);
		return menuItem;
	}

	/**
	 *
	 * @param <T>
	 * @param item
	 * @param itemId
	 * @param eventHandler
	 */
	private static <T extends MenuItem> void setItem(final T item, final String itemId, final EventHandler<ActionEvent> eventHandler) {
		item.setId(itemId);
		item.setGraphic(getImageView(itemId + ".icon"));
		item.textProperty().bind(createStringLocaleBinding(itemId));
		item.setOnAction(eventHandler);
	}

	/**
	 *
	 * @return
	 */
	public static MenuItem createSeparatorMenuItem() {
		return new SeparatorMenuItem();
	}

	public static Dialog<Void> createDialog() {
		final Dialog<Void> dialog = new Dialog<>();
		return dialog;
	}

	/**
	 *
	 * @param property
	 * @return
	 */
	public static Label createLabel(final DrawProperty<?> property) {
		final Label label = new Label();
		label.setId(property.getId());
		label.textProperty().bind(createLabelBinding(property));
		label.setTooltip(new Tooltip());
		label.getTooltip().textProperty().bind(createDescriptionBinding(property));
		return label;
	}

	/**
	 *
	 * @param <T>
	 * @param items
	 * @param eventHandler
	 * @return
	 */
	public static <T extends DrawBean> ComboBox<T> createComboBox(final Collection<T> items, final EventHandler<ActionEvent> eventHandler) {
		final ComboBox<T> comboBox = new ComboBox<>(FXCollections.observableArrayList(items));
		comboBox.setOnAction(eventHandler);
		comboBox.setConverter(new DrawBeanStringConverter<T>());
		comboBox.getSelectionModel().selectFirst();
		return comboBox;
	}

	/**
	 *
	 * @param <T>
	 * @param items
	 * @param eventHandler
	 * @return
	 */
	public static <T extends DrawBean> ChoiceBox<T> createChoiceBox(final Collection<T> items, final EventHandler<ActionEvent> eventHandler) {
		final ChoiceBox<T> comboBox = new ChoiceBox<>(FXCollections.observableArrayList(items));
		comboBox.setOnAction(eventHandler);
		comboBox.setConverter(new DrawBeanStringConverter<>());
		comboBox.getSelectionModel().selectFirst();
		return comboBox;
	}

	/**
	 *
	 * @param node
	 * @return
	 */
	public static ScrollPane createScrollPane(final Node node) {
		final ScrollPane scrollPane = new ScrollPane(node);
		scrollPane.setFitToHeight(true);
		scrollPane.setFitToWidth(true);
		return scrollPane;
	}

	/**
	 * Create a grid pane.
	 */
	public static GridPane createGridPane() {
		final GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(10));
		gridPane.setHgap(4);
		gridPane.setVgap(8);
		return gridPane;
	}

	/**
	 * Create a separator.
	 * @param orientation Separator orientation.
	 * @return Returns a new separator.
	 */
	public static Separator createSeparator(final Orientation orientation) {
		return new Separator(orientation);
	}

	/**
	 *
	 * @return
	 */
	public static ButtonBar createButtonBar() {
		final ButtonBar buttonBar = new ButtonBar();
		buttonBar.setPadding(new Insets(7));
		return buttonBar;
	}


	/**
	 *
	 * @param buttonId
	 * @param isLabelDisplayed
	 * @param eventHandler
	 * @return
	 */
	public static Button createButton(final String buttonId, final boolean isLabelDisplayed, final EventHandler<ActionEvent> eventHandler) {
		final Button button = new Button();
		button.setId(buttonId);
		button.setGraphic(getImageView(buttonId + ".icon"));
		if (isLabelDisplayed) {
			button.textProperty().bind(createStringLocaleBinding(buttonId + ".label"));
		}
		button.setTooltip(new Tooltip());
		button.getTooltip().textProperty().bind(createStringLocaleBinding(buttonId + ".description"));
		button.setOnAction(eventHandler);
		return button;
	}

	/**
	 * Create a button
	 * @param buttonId ID of the button.
	 * @param eventHandler Function executed on the click button action.
	 * @return
	 */
	public static Button createButton(final String buttonId, final EventHandler<ActionEvent> eventHandler) {
		return createButton(buttonId, true, eventHandler);
	}

	/**
	 *
	 * @param imgageId
	 * @return
	 */
	public static Image getImage(final String imgageId) {
		final String imagePath = "META-INF/icons/default/" + imgageId + ".png";
		final URL imageURL = JavaFXDrawFactory.class.getClassLoader().getResource(imagePath);

		if (imageURL == null) {
			if (LOGGER.isLoggable(Level.WARNING)) {
				LOGGER.warning("The image '" + imagePath + "' is not found");
			}
			return null;
		}

		try (InputStream input = imageURL.openStream()) {
			return new Image(input);
		} catch (final IOException e) {
			if (LOGGER.isLoggable(Level.SEVERE)) {
				LOGGER.log(Level.SEVERE, imagePath + ": " + e.getLocalizedMessage(), e);
			}
			return null;
		}
	}

	/**
	 *
	 * @param imgageId
	 * @return
	 */
	public static ImageView getImageView(final String imgageId) {
		final Image image = getImage(imgageId);
		if (image != null) {
			return new ImageView(image);
		}

		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> AbstractPropertyComponent<T,?> createPropertyControl(final DrawProperty<T> property) {
		if (property.getDefaultValue() instanceof Boolean) {
			return (AbstractPropertyComponent<T, ?>) new BooleanPropertyComponent((BooleanDrawProperty) property);
		}

		if (property.getDefaultValue().getClass() == EnumDrawBean[].class) {
			return (AbstractPropertyComponent<T, ?>) new EnumPropertyComponent((DrawProperty<EnumDrawBean[]>) property);
		}

		if (property.getDefaultValue() instanceof Number) {
			return new NumberPropertyComponent<>((NumberDrawProperty) property);
		}

		if (property.getDefaultValue() instanceof DrawColor) {
			return (AbstractPropertyComponent<T, ?>) new ColorPropertyComponent((ColorDrawProperty) property);
		}

		return new TextFieldPropertyComponent<>(property);
	}

	/**
	 *
	 * @param property
	 * @return
	 */
	public static CheckBox createCheckBox(final BooleanDrawProperty property) {
		final CheckBox checkBox =  new CheckBox();
		checkBox.setSelected(property.getValue() == null ? false : property.getValue());
		return checkBox;
	}

	/**
	 *
	 * @param property
	 * @return
	 */
	public static ChoiceBox<EnumDrawBean> createChoiceBox(final DrawProperty<EnumDrawBean[]> property) {
		final EnumDrawBean[] values = EnumDrawBean.copy(property.getDefaultValue());
		final ObservableList<EnumDrawBean> observalbelList = FXCollections.observableArrayList(values);
		final ChoiceBox<EnumDrawBean> choiceBox = new ChoiceBox<>(observalbelList);
		for (final EnumDrawBean bean : values) {
			if (bean.isSelected()) {
				choiceBox.getSelectionModel().select(bean);
			}
		}
		choiceBox.setConverter(new DrawBeanStringConverter<EnumDrawBean>());
		return choiceBox;
	}

	public static <T extends Number> Slider createSlider(final NumberDrawProperty<T> property) {
		final Slider slider = new Slider();
		if (property.getMinimum() != null) {
			slider.setMin(property.getMinimum().doubleValue());
		}
		if (property.getMaximum() != null) {
			slider.setMax(property.getMaximum().doubleValue());
		}
		if (property.getStep() != null) {
			slider.setBlockIncrement(property.getStep().doubleValue());
		}
		slider.setValue(property.getValue() == null ? property.getDefaultValue().doubleValue() : property.getValue().doubleValue());
		slider.setShowTickLabels(true);
		slider.setShowTickMarks(true);
		slider.setTooltip(new Tooltip());
		slider.getTooltip().textProperty().bind(
				Bindings.createStringBinding(() -> Double.toString(slider.getValue()), slider.valueProperty())
				);

		if (property.getDefaultValue() instanceof Integer) {
			slider.valueProperty().addListener((obs, oldval, newVal) -> slider.setValue(newVal.intValue()));
		} else  if (property.getDefaultValue() instanceof Long) {
			slider.valueProperty().addListener((obs, oldval, newVal) -> slider.setValue(newVal.longValue()));
		} else if (property.getDefaultValue() instanceof Short) {
			slider.valueProperty().addListener((obs, oldval, newVal) -> slider.setValue(newVal.shortValue()));
		} else if (property.getDefaultValue() instanceof Float) {
			slider.valueProperty().addListener((obs, oldval, newVal) -> slider.setValue(newVal.floatValue()));
		} else if (property.getDefaultValue() instanceof Byte) {
			slider.valueProperty().addListener((obs, oldval, newVal) -> slider.setValue(newVal.byteValue()));
		}


		return slider;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Number> Spinner<T> createSpinner(final NumberDrawProperty<T> property) {

		final SpinnerValueFactory<T> valueFactory;
		if (property.getDefaultValue() instanceof Integer) {
			valueFactory = (SpinnerValueFactory<T>) new SpinnerValueFactory.IntegerSpinnerValueFactory(
					(int) (property.getMinimum() == null ? Integer.MIN_VALUE : property.getMinimum()),
					(int) (property.getMaximum() == null ? Integer.MAX_VALUE : property.getMaximum()),
					(int) (property.getValue() == null ? property.getDefaultValue() : property.getValue()),
					(int) (property.getStep() == null ? 1 : property.getStep())
					);
		} else if (property.getDefaultValue() instanceof Double) {
			valueFactory = (SpinnerValueFactory<T>) new SpinnerValueFactory.DoubleSpinnerValueFactory(
					(double) (property.getMinimum() == null ? Double.MIN_VALUE : property.getMinimum()),
					(double) (property.getMaximum() == null ? Double.MAX_VALUE : property.getMaximum()),
					(double) (property.getValue() == null ? property.getDefaultValue() : property.getValue()),
					(double) (property.getStep() == null ? 1.0 : property.getStep())
					);
		} else if (property.getDefaultValue() instanceof Long) {
			valueFactory = (SpinnerValueFactory<T>) new LongSpinnerValueFactory(
					(long) (property.getMinimum() == null ? Long.MIN_VALUE : property.getMinimum()),
					(long) (property.getMaximum() == null ? Long.MAX_VALUE : property.getMaximum()),
					(long) (property.getValue() == null ? property.getDefaultValue() : property.getValue()),
					(long) (property.getStep() == null ? 1 : property.getStep())
					);
		} else {
			valueFactory = null;
		}
		final Spinner<T> spinner = new Spinner<>(valueFactory);
		spinner.setEditable(true);
		return spinner;
	}

	/**
	 *
	 * @param color
	 * @return
	 */
	public static Color getColor(final DrawColor color) {
		if (color == null) {
			return null;
		}
		return Color.color((color.getRed()) / 255.0, (color.getGreen()) / 255.0, (color.getRed()) / 255.0);
	}

	public static DrawColor getColor(final Color color) {
		if (color == null) {
			return null;
		}
		return new DrawColor(
				(int) (color.getRed() * 255.0),
				(int) (color.getGreen() * 255.0),
				(int) (color.getRed() * 255.0)
				);
	}

	/**
	 *
	 * @param color
	 * @return
	 */
	public static String toWeb(final Color color){
		return String.format(
				"#%02X%02X%02X",
				(int) (color.getRed() * 255),
				(int) (color.getGreen() * 255),
				(int) (color.getBlue() * 255)
				);
	}

	/**
	 *
	 * @param stageId
	 * @return
	 */
	public static Stage createStage(final String stageId) {
		final Stage stage = new Stage();
		stage.getIcons().add(getImage(stageId + ".icon"));
		stage.titleProperty().bind(createStringLocaleBinding(stageId + ".title"));
		stage.initModality(Modality.APPLICATION_MODAL);
		return stage;
	}

	@Override
	public void saveViewerImage() {
		final FileChooser fileChooser = new FileChooser();

		//Set extension filter for text files
		final FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image (*.png)", "*.png");
		fileChooser.getExtensionFilters().add(extFilter);
		final File saveFile = fileChooser.showSaveDialog(null);
		if (saveFile == null) {
			return;
		}

		final Canvas drawerCanvas = (Canvas) this.getDrawer();

		final WritableImage writableImage = new WritableImage((int) drawerCanvas.getWidth() + 1, (int) drawerCanvas.getHeight() + 1);
		drawerCanvas.snapshot(null, writableImage);

		try {
			ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", saveFile);
		} catch (final IOException e) {
			LOGGER.log(Level.SEVERE, e.getLocalizedMessage(), e);
			// TODO display the error to the user
		}
	}

	/**
	 * Create a color draw chooser.
	 * @param property
	 * @return
	 */
	public static ColorPicker createColorDrawChooser(final ColorDrawProperty property) {
		return new ColorPicker(JavaFXDrawFactory.getColor(property.getValue()));
	}
}
