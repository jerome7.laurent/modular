/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx.component.spinner;

import javafx.beans.NamedArg;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.scene.control.SpinnerValueFactory;
import javafx.util.converter.LongStringConverter;

/**
 * @author Jérôme LAURENT
 * Copy of IntegerSpinnerValueFactory
 */
/**
 * A {@link javafx.scene.control.SpinnerValueFactory} implementation designed to iterate through
 * integer values.
 *
 * <p>Note that the default {@link #converterProperty() converter} is implemented
 * as an {@link javafx.util.converter.IntegerStringConverter} instance.
 *
 * @since JavaFX 8u40
 */
public class LongSpinnerValueFactory extends SpinnerValueFactory<Long> {

	/***********************************************************************
	 *                                                                     *
	 * Constructors                                                        *
	 *                                                                     *
	 **********************************************************************/

	/**
	 * Constructs a new IntegerSpinnerValueFactory that sets the initial value
	 * to be equal to the min value, and a default {@code amountToStepBy} of one.
	 *
	 * @param min The minimum allowed integer value for the Spinner.
	 * @param max The maximum allowed integer value for the Spinner.
	 */
	public LongSpinnerValueFactory(@NamedArg("min") final long min,
			@NamedArg("max") final long max) {
		this(min, max, min);
	}

	/**
	 * Constructs a new IntegerSpinnerValueFactory with a default
	 * {@code amountToStepBy} of one.
	 *
	 * @param min The minimum allowed integer value for the Spinner.
	 * @param max The maximum allowed integer value for the Spinner.
	 * @param initialValue The value of the Spinner when first instantiated, must
	 *                     be within the bounds of the min and max arguments, or
	 *                     else the min value will be used.
	 */
	public LongSpinnerValueFactory(@NamedArg("min") final long min,
			@NamedArg("max") final long max,
			@NamedArg("initialValue") final long initialValue) {
		this(min, max, initialValue, 1L);
	}

	/**
	 * Constructs a new IntegerSpinnerValueFactory.
	 *
	 * @param min The minimum allowed integer value for the Spinner.
	 * @param max The maximum allowed integer value for the Spinner.
	 * @param initialValue The value of the Spinner when first instantiated, must
	 *                     be within the bounds of the min and max arguments, or
	 *                     else the min value will be used.
	 * @param amountToStepBy The amount to increment or decrement by, per step.
	 */
	public LongSpinnerValueFactory(@NamedArg("min") final long min,
			@NamedArg("max") final long max,
			@NamedArg("initialValue") final long initialValue,
			@NamedArg("amountToStepBy") final long amountToStepBy) {
		this.setMin(min);
		this.setMax(max);
		this.setAmountToStepBy(amountToStepBy);
		this.setConverter(new LongStringConverter());

		this.valueProperty().addListener((o, oldValue, newValue) -> {
			// when the value is set, we need to react to ensure it is a
			// valid value (and if not, blow up appropriately)
			if (newValue < this.getMin()) {
				this.setValue(this.getMin());
			} else if (newValue > this.getMax()) {
				this.setValue(this.getMax());
			}
		});
		this.setValue(initialValue >= min && initialValue <= max ? initialValue : min);
	}


	/***********************************************************************
	 *                                                                     *
	 * Properties                                                          *
	 *                                                                     *
	 **********************************************************************/

	// --- min
	private final LongProperty min = new SimpleLongProperty(this, "min") {
		@Override protected void invalidated() {
			final Long currentValue = LongSpinnerValueFactory.this.getValue();
			if (currentValue == null) {
				return;
			}

			final long newMin = this.get();
			if (newMin > LongSpinnerValueFactory.this.getMax()) {
				LongSpinnerValueFactory.this.setMin(LongSpinnerValueFactory.this.getMax());
				return;
			}

			if (currentValue < newMin) {
				LongSpinnerValueFactory.this.setValue(newMin);
			}
		}
	};

	public final void setMin(final long value) {
		this.min.set(value);
	}
	public final long getMin() {
		return this.min.get();
	}
	/**
	 * Sets the minimum allowable value for this value factory
	 * @return the minimum allowable value for this value factory
	 */
	public final LongProperty minProperty() {
		return this.min;
	}

	// --- max
	private final LongProperty max = new SimpleLongProperty(this, "max") {
		@Override protected void invalidated() {
			final Long currentValue = LongSpinnerValueFactory.this.getValue();
			if (currentValue == null) {
				return;
			}

			final long newMax = this.get();
			if (newMax < LongSpinnerValueFactory.this.getMin()) {
				LongSpinnerValueFactory.this.setMax(LongSpinnerValueFactory.this.getMin());
				return;
			}

			if (currentValue > newMax) {
				LongSpinnerValueFactory.this.setValue(newMax);
			}
		}
	};

	public final void setMax(final long value) {
		this.max.set(value);
	}
	public final long getMax() {
		return this.max.get();
	}
	/**
	 * Sets the maximum allowable value for this value factory
	 * @return the maximum allowable value for this value factory
	 */
	public final LongProperty maxProperty() {
		return this.max;
	}

	// --- amountToStepBy
	private final LongProperty amountToStepBy = new SimpleLongProperty(this, "amountToStepBy");
	public final void setAmountToStepBy(final long value) {
		this.amountToStepBy.set(value);
	}
	public final long getAmountToStepBy() {
		return this.amountToStepBy.get();
	}
	/**
	 * Sets the amount to increment or decrement by, per step.
	 * @return the amount to increment or decrement by, per step
	 */
	public final LongProperty amountToStepByProperty() {
		return this.amountToStepBy;
	}



	/***********************************************************************
	 *                                                                     *
	 * Overridden methods                                                  *
	 *                                                                     *
	 **********************************************************************/

	/** {@inheritDoc} */
	@Override public void decrement(final int steps) {
		final long min = this.getMin();
		final long max = this.getMax();
		final long newIndex = this.getValue() - steps * this.getAmountToStepBy();
		this.setValue(newIndex >= min ? newIndex : (this.isWrapAround() ? wrapValue(newIndex, min, max) + 1 : min));
	}

	/** {@inheritDoc} */
	@Override public void increment(final int steps) {
		final long min = this.getMin();
		final long max = this.getMax();
		final long currentValue = this.getValue();
		final long newIndex = currentValue + steps * this.getAmountToStepBy();
		this.setValue(newIndex <= max ? newIndex : (this.isWrapAround() ? wrapValue(newIndex, min, max) - 1 : max));
	}

	static long wrapValue(final long value, final long min, final long max) {
		if (max == 0) {
			throw new RuntimeException();
		}

		long r = value % max;
		if (r > min && max < min) {
			r = r + max - min;
		} else if (r < min && max > min) {
			r = r + max - min;
		}
		return r;
	}
}