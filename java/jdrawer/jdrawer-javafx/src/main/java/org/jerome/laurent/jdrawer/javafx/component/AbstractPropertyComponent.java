/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx.component;

import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.configuration.DrawProperty;
import org.jerome.laurent.jdrawer.api.configuration.PropertyComponent;
import org.jerome.laurent.jdrawer.javafx.JavaFXDrawFactory;

import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;

/**
 * @author Jérôme LAURENT
 * @param <T> Type of the property.
 * @param <C> Control used to manage the value of the property.
 */
public abstract class AbstractPropertyComponent<T, C extends Control> implements PropertyComponent<T> {

	/**
	 *
	 */
	private final DrawProperty<T> property;

	/**
	 *
	 */
	private final C control;

	private final String ERROR_BACKROUND = JavaFXDrawFactory.toWeb(Color.color(1.0, 0.75, 0.75));

	private final String UPDATING_BACKROUND = JavaFXDrawFactory.toWeb(Color.color(0.97, 0.97, 0.65));

	private Background defaultBackround;

	private T oldValue;


	/**
	 *
	 * @param control
	 */
	public AbstractPropertyComponent(final DrawProperty<T> property, final C control) {
		this.property = property;
		this.control = control;
		this.oldValue = property.getDefaultValue();
	}

	@Override
	public void setError(final DrawError error) {
		if (error == null) {
			this.getControl().setStyle(null);
			if (!this.getProperty().getDefaultValue().equals(this.getValue())) {
				this.getControl().setStyle("-fx-font-weight: bold");
			}
			this.control.setTooltip(null);
			this.oldValue = this.getValue();
		} else {
			if (this.defaultBackround == null) {
				this.defaultBackround = this.control.getBackground();
			}
			this.getControl().setStyle("-fx-control-inner-background: " + this.ERROR_BACKROUND);
			this.control.setTooltip(new Tooltip());
			this.control.getTooltip().textProperty().bind(JavaFXDrawFactory.createLabelBinding(error));
		}
	}

	@Override
	public void setValue(final T value) {
		this.setControlValue(value);
		this.setStyle();
	}

	/**
	 *
	 * Set style.
	 */
	protected void setStyle() {

		String style = "";

		if (!this.oldValue.equals(this.getValue())) {
			style += "; -fx-control-inner-background: " + this.UPDATING_BACKROUND
					+ "; -fx-background-color: -fx-shadow-highlight-color, -fx-outer-border, -fx-inner-border, -fx-body-color, " + this.UPDATING_BACKROUND
					+ "; -fx-background-insets: 0 0 -1 0, 0, 1, 2"
					+ "; -fx-background-radius: 3px, 3px, 2px, 1px "
					+ "; -fx-text-fill: -fx-text-base-color"
					;

		}

		if (!this.getProperty().getDefaultValue().equals(this.getValue())) {
			style += "; -fx-font-weight: bold";
		}

		//		if (this.getControl().isFocused()) {
		//			style += "-fx-border-color: lightblue; "
		//					+ "-fx-font-size: 20;"
		//					+ "-fx-border-insets: -5; "
		//					+ "-fx-border-radius: 5;"
		//					+ "-fx-border-style: dotted;"
		//					+ "-fx-border-width: 2;"
		//					;
		//		}

		this.getControl().setStyle(style);
	}

	/**
	 * Set control value.
	 * @param value
	 */
	protected abstract void setControlValue(T value);

	/**
	 * @return the property
	 */
	protected DrawProperty<T> getProperty() {
		return this.property;
	}

	/**
	 * @return the control
	 */
	protected C getControl() {
		return this.control;
	}
}
