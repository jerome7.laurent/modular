/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.shape.CircleDrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawColor;
import org.jerome.laurent.jdrawer.api.shape.DrawShape;
import org.jerome.laurent.jdrawer.api.shape.DrawShapes;
import org.jerome.laurent.jdrawer.api.shape.LineDrawShape;
import org.jerome.laurent.jdrawer.api.shape.MacroDrawShape;
import org.jerome.laurent.jdrawer.api.shape.PointDraw;
import org.jerome.laurent.jdrawer.api.shape.PolygonDrawShape;
import org.jerome.laurent.jdrawer.api.shape.TextDrawShape;
import org.jerome.laurent.jdrawer.api.shape.VectorDraw;
import org.jerome.laurent.jdrawer.api.shape.ZShapeComparator;
import org.jerome.laurent.jdrawer.api.viewer.Drawer;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;

/**
 * @author Jérôme LAURENT
 *
 */
public class DrawerCanvas extends Canvas implements Drawer {

	private static final Logger LOGGER = Logger.getLogger(DrawerCanvas.class.getName());

	private static final DrawerCanvas DRAWER_CANVAS = new DrawerCanvas();

	private final BlockingQueue<DrawShape> queuedShapes;

	private javafx.scene.paint.Color currentBackgroundColor;

	private final SimpleObjectProperty<VectorDraw> centerVectorProperty;

	private PointDraw pressedPoint;

	private DrawerCanvas() {
		super();
		this.queuedShapes = new LinkedBlockingQueue<>();
		this.centerVectorProperty = new SimpleObjectProperty<>(this.computeCenterVectorDraw());
		this.centerVectorProperty.bind(Bindings.createObjectBinding(() -> this.computeCenterVectorDraw(), this.heightProperty()));
		this.centerVectorProperty.bind(Bindings.createObjectBinding(() -> this.computeCenterVectorDraw(), this.widthProperty()));
		this.setOnScroll(DrawerCanvas::zoomOnScroll);
		this.setOnMousePressed(e -> this.pressedPoint = new PointDraw(e.getX(), -e.getY()));
		this.setOnMouseReleased(e -> this.translateOnMouseReleased(e));
	}

	private PointDraw computeSpotLight() {
		final PointDraw l = JavaFXDrawFactory.getController().getSpotLight().clone();
		this.transformToCoordinateSystem(l);
		return l;
	}

	/**
	 *
	 * @param e
	 */
	private static final void zoomOnScroll(final ScrollEvent e) {
		final double deltaY = e.getDeltaY();
		if (deltaY > 0) {
			JavaFXDrawFactory.getController().zoomIn();
		} else {
			JavaFXDrawFactory.getController().zoomOut();
		}
	}

	private final void translateOnMouseReleased(final MouseEvent e) {
		final PointDraw releasedPoint = new PointDraw((int)e.getX(), (int) -e.getY());
		final VectorDraw translationVector = new VectorDraw(this.pressedPoint, releasedPoint);
		JavaFXDrawFactory.getController().setTranslationVector(translationVector);
	}

	/**
	 *
	 * @return
	 */
	public static DrawerCanvas getInstance() {
		return DRAWER_CANVAS;
	}

	/**
	 *
	 * @return
	 */
	private VectorDraw computeCenterVectorDraw() {
		return new VectorDraw(this.getWidth()/2.0, this.getHeight()/2.0);
	}

	@Override
	public void draw(final List<DrawShape> shapes) {
		if (LOGGER.isLoggable(Level.FINER)) {
			LOGGER.fine("Drawing shape: " + this.queuedShapes);
		}
		if (this.queuedShapes != null) {
			this.queuedShapes.addAll(shapes);

			if (!this.queuedShapes.isEmpty()) {
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						DrawerCanvas.this.drawShapes();
					}
				});
			}
		}
	}

	protected void drawShapes() {
		while (!DrawerCanvas.this.queuedShapes.isEmpty()) {
			DrawerCanvas.this.drawShape(DrawerCanvas.this.queuedShapes.poll());
		}
	}

	protected void drawShape(final DrawShape shape) {

		if (shape == null) {
			return;
		}

		final GraphicsContext g = this.getGraphicsContext2D();

		g.setStroke(JavaFXDrawFactory.getColor(shape.getColor()));

		if (shape == DrawShapes.CLEAR_DRAW_SHAPE) {
			if (this.currentBackgroundColor != null) {
				g.setFill(this.currentBackgroundColor);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
			} else {
				g.clearRect(0, 0, this.getWidth(), this.getHeight());
			}
		} else if (shape instanceof TextDrawShape) {
			final TextDrawShape text = (TextDrawShape) shape;
			this.transformToCoordinateSystem(text.getPoint1());
			g.strokeText(text.getText(), text.getPoint1().getX(), text.getPoint1().getY());
		} else if (shape instanceof CircleDrawShape) {
			final CircleDrawShape circle = (CircleDrawShape) shape;
			this.transformToCoordinateSystem(circle.getPoint1());
			g.strokeOval(circle.getPoint1().getX() - circle.getRadius(), circle.getPoint1().getY() - circle.getRadius(), circle.getRadius() * 2, circle.getRadius() * 2);
		} else if (shape instanceof LineDrawShape) {
			final LineDrawShape line = (LineDrawShape) shape;
			this.transformToCoordinateSystem(line.getPoint1(), line.getPoint2());
			g.setLineWidth(1.0);
			g.strokeLine(line.getPoint1().getX(), line.getPoint1().getY(), line.getPoint2().getX(), line.getPoint2().getY());
		} else if (shape instanceof MacroDrawShape) {
			try {
				Collections.sort(((MacroDrawShape)shape).getChildren(), new ZShapeComparator());
			} catch (final IllegalArgumentException e) {
				if (LOGGER.isLoggable(Level.SEVERE)) {
					LOGGER.log(Level.SEVERE, e.getLocalizedMessage(), e);
				}
			}
			for (final DrawShape childShape : ((MacroDrawShape)shape).getChildren()) {
				this.drawShape(childShape);
			}
		} else if (shape instanceof PolygonDrawShape) {
			final PolygonDrawShape polygon = (PolygonDrawShape) shape;

			final DrawColor computedLighColor;
			if (polygon.getFillColor() != null) {
				computedLighColor = JavaFXDrawFactory.getController().computeLight(polygon);
			} else {
				computedLighColor = null;
			}

			final int pointNumber = polygon.getPoints().length;
			final double x[] = new double[pointNumber];
			final double y[] = new double[pointNumber];

			this.transformToCoordinateSystem(polygon.getPoints());

			for (int i = 0; i < pointNumber; i++) {
				x[i] = polygon.getPoints()[i].getX();
				y[i] = polygon.getPoints()[i].getY();
			}

			if (polygon.getColor() != null) {
				g.strokePolygon(x, y, pointNumber);
			}

			if (polygon.getFillColor() != null) {
				final Color drawColor = JavaFXDrawFactory.getColor(computedLighColor);
				if (polygon.getColor() == null) {
					g.setStroke(drawColor);
				}
				g.setFill(drawColor);
				if (polygon.getColor() == null) {
					g.strokePolygon(x, y, pointNumber);
				}
				g.fillPolygon(x, y, pointNumber);
			}
		}
	}



	protected void transformToCoordinateSystem(final PointDraw... points) {
		for (final PointDraw point : points) {
			point.setY(-point.getY());
		}
		DrawShapes.translate(this.getVectorCenter(), points);
	}

	protected VectorDraw getVectorCenter() {
		return this.centerVectorProperty.get();
	}

	public void negativeView() {
		if (this.currentBackgroundColor == null) {
			this.currentBackgroundColor = javafx.scene.paint.Color.BLACK;
		} else {
			this.currentBackgroundColor = javafx.scene.paint.Color.color(
					1.0 - this.currentBackgroundColor.getRed(),
					1.0 - this.currentBackgroundColor.getGreen(),
					1.0 - this.currentBackgroundColor.getBlue()
					);
		}
	}

}
