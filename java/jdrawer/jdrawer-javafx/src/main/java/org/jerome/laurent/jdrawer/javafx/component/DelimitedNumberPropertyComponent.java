/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx.component;

import java.util.logging.Logger;

import org.jerome.laurent.jdrawer.api.configuration.DrawError;
import org.jerome.laurent.jdrawer.api.configuration.NumberDrawProperty;
import org.jerome.laurent.jdrawer.javafx.JavaFXDrawFactory;

import javafx.beans.binding.Bindings;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;

/**
 * @author Jérôme LAURENT
 *
 */
public class DelimitedNumberPropertyComponent<T extends Number> extends AbstractPropertyComponent<T, Slider> {

	private static final Logger LOGGER = Logger.getLogger(NumberPropertyComponent.class.getName());

	public DelimitedNumberPropertyComponent(final NumberDrawProperty<T> property) {
		super(property, JavaFXDrawFactory.createSlider(property));
	}

	@Override
	public void setControlValue(final T value) {
		this.getControl().setValue(value.doubleValue());
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getValue() {
		final Double value = Double.valueOf(this.getControl().getValue());

		if (this.getProperty().getDefaultValue() instanceof Double) {
			return (T) value;
		}

		if (this.getProperty().getDefaultValue() instanceof Integer) {
			return (T) Integer.valueOf(value.intValue());
		}

		if (this.getProperty().getDefaultValue() instanceof Long) {
			return (T) Long.valueOf(value.longValue());
		}

		if (this.getProperty().getDefaultValue() instanceof Short) {
			return (T) Short.valueOf(value.shortValue());
		}

		if (this.getProperty().getDefaultValue() instanceof Float) {
			return (T) Float.valueOf(value.floatValue());
		}

		if (this.getProperty().getDefaultValue() instanceof Byte) {
			return (T) Byte.valueOf(value.byteValue());
		}

		LOGGER.severe("The number type '" + this.getProperty().getDefaultValue().getClass() + "' is not managed");

		return null;
	}

	@Override
	public void setError(final DrawError error) {
		// TODO Auto-generated method stub
		super.setError(error);

		if (error == null) {
			this.getControl().setTooltip(new Tooltip());
			this.getControl().getTooltip().textProperty().bind(
					Bindings.createStringBinding(() -> Double.toString(this.getControl().getValue()), this.getControl().valueProperty())
					);
		}
	}
}
