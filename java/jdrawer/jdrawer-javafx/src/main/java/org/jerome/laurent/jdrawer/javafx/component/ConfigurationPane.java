/*-----------------------------------------------------------------------------*
 * Copyright 2019 JDrawer (Jérôme LAURENT). All  Rights Reserved.              *
 *                                                                             *
 * Licensed under the GNU General Public License, version 3                    *
 * http://www.gnu.org/licenses/gpl-3.0.txt                                     *
 * The license includes the following conditions.                              *
 *                                                                             *
 *  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY           *
 * APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT      *
 * HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY   *
 * OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,    *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR      *
 * PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM  *
 * IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF    *
 * ALL NECESSARY SERVICING, REPAIR OR CORRECTION.                              *
 *                                                                             *
 * IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING       *
 * WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS   *
 * THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY *
 * GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE    *
 * USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF   *
 * DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD  *
 * PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),    *
 * EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF   *
 * SUCH DAMAGES.                                                               *
 *                                                                             *
 *-----------------------------------------------------------------------------*/
package org.jerome.laurent.jdrawer.javafx.component;

import java.util.HashMap;
import java.util.Map;

import org.jerome.laurent.jdrawer.api.configuration.Configurartions;
import org.jerome.laurent.jdrawer.api.configuration.DrawConfigurable;
import org.jerome.laurent.jdrawer.api.configuration.DrawProperty;
import org.jerome.laurent.jdrawer.api.configuration.PropertyComponent;
import org.jerome.laurent.jdrawer.javafx.JavaFXDrawFactory;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Jérôme LAURENT
 *
 */
public class ConfigurationPane extends VBox {

	private DrawConfigurable configurable;

	private final Map<String, PropertyComponent<?>> componentValues;

	private final GridPane gridPane;

	private EventHandler<ActionEvent> applyEventHandler;

	public ConfigurationPane() {
		super();

		this.componentValues = new HashMap<>();

		//
		// Properties
		//
		this.gridPane = JavaFXDrawFactory.createGridPane();
		this.gridPane.setAlignment(Pos.TOP_LEFT);

		final ScrollPane propertyScrollPane = JavaFXDrawFactory.createScrollPane(this.gridPane);
		// fill empty space
		VBox.setVgrow(propertyScrollPane, Priority.ALWAYS);

		//
		// Action buttons
		//
		final Button applyButton = JavaFXDrawFactory.createButton("configuration.apply", e -> {
			Configurartions.applyConfiguration(this.configurable, this.componentValues);
			if (this.applyEventHandler != null) {
				this.applyEventHandler.handle(e);
			}
		});
		ButtonBar.setButtonData(applyButton, ButtonBar.ButtonData.APPLY);

		final Button resetButton = JavaFXDrawFactory.createButton("configuration.reset", e -> Configurartions.resetConfiguration(this.configurable, this.componentValues));
		ButtonBar.setButtonData(resetButton, ButtonBar.ButtonData.BACK_PREVIOUS);

		final ButtonBar burronBar = JavaFXDrawFactory.createButtonBar();
		burronBar.getButtons().addAll(applyButton, resetButton);

		// adding children
		this.getChildren().addAll(propertyScrollPane, JavaFXDrawFactory.createSeparator(Orientation.HORIZONTAL), burronBar);
	}

	public void addApplyEventHandler(final EventHandler<ActionEvent> eventHandler) {
		this.applyEventHandler = eventHandler;
	}

	public void loadConfiguration() {

		if (this.configurable != null) {

			int rowIndex = 0;
			for (final DrawProperty<?> property : this.configurable.getConfiguration().getProperties()) {
				// label
				this.gridPane.add(JavaFXDrawFactory.createLabel(property), 0, rowIndex);
				// editable value
				final AbstractPropertyComponent<?,?> value = JavaFXDrawFactory.createPropertyControl(property);
				this.gridPane.add(value.getControl(), 1, rowIndex);

				this.componentValues.put(property.getId(), value);
				rowIndex++;
			}
		}
	}

	/**
	 * @return the configurable
	 */
	public DrawConfigurable getConfigurable() {
		return this.configurable;
	}

	/**
	 * @param configurable the configurable to set
	 */
	public void setConfigurable(final DrawConfigurable configurable) {
		this.gridPane.getChildren().clear();
		this.configurable = configurable;
		this.loadConfiguration();
	}
}
